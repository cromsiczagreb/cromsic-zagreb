<?php

if(!$session->is_logged_in()){ 
	redirect_to("index.php");
}

class ChangeForm extends Form {
	
		public $errors =array();
		public $id;
		public $password;
		public $password2;


		
		
		// Individual process
		
		public function individual_process() {
				$this->validate();
				$this->show_errors($this->errors);
				
				if(empty($this->errors)) {
					
					 $this->prepare();
					 $this->finalize();
				}
		}
		
		// Validate
		
		private function Validate() {
			// check if passwords match
			if(!($this->password == $this->password2)) { $this->errors['password'] = _("Lozinke se ne podudaraju.") ;}
			if(!User::confirm_password($this->id, $this->password_old)) { $this->errors['wrong'] = _("Stara lozinka nije točna."); }
		}
		
		// Prepare
		
		private function prepare() {
			//$this->password = password_hash($this->password, PASSWORD_DEFAULT);
			
		}
		
		// Finalize
		
		private function finalize() {
			global $session;
		
			if(User::change_password($this->id, $this->password)) {
					$session->message(_("Lozinka je uspješno promjenjena"), "success");
					redirect_to("index.php");
			} else {
					$this->errors['error'] = _("Greška na poslužitelju. Pokušajte kasnije.") ;
					$this->show_errors() ;
			}
	
		}
		
		
		
		
				
		// Form creation
		
		public function create_form() {
			global $session;
			$id = $session->user_id;
			
			$this->start_form("change_password.php");
			$this->add_element("password_old", _("Stara lozinka "), "password", false, "", "");
			$this->add_element("password", _("Nova lozinka"), "password", false, "", "");
			$this->add_element("password2", _("Ponovite lozinku"), "password", false,"", "");
			$this->add_element_hidden("id", $id);
			$this->end_form();
		}
}



?>