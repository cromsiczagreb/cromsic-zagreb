<?php

if(!isset($_GET['id'])  and !isset($_POST['password'])) {
	redirect_to("index.php");
} else {
	if(isset($_GET['id'])) {
		$get_id = $_GET['id'];
		$get_hash = $_GET['hash'];
	} else {
		$get_id = $_POST['id'];
		$get_hash = $_POST['hash'];
	}
}


class ResetForm extends Form {
	
		public $errors =array();
		public $id;
		public $hash;
		public $password;
		public $password2;
		public $made;

		
		
		// Individual process
		
		public function individual_process() {
				$this->validate();
				$this->show_errors($this->errors);
				
				if(empty($this->errors)) {
					
					 $this->prepare();
					 $this->finalize();
				}
		}
		
		// Validate
		
		private function Validate() {
			// check if passwords match
			if(!($this->password == $this->password2)) { $this->errors['password'] = _("Lozinke se ne podudaraju.") ;}
		}
		
		// Prepare
		
		private function prepare() {
			//$this->password = password_hash($this->password, PASSWORD_DEFAULT);
			global $get_id;
			global $get_hash;
			
		}
		
		// Finalize
		
		private function finalize() {
			global $session;
			global $db;
			$this->id	 = $db->escape_value($this->id);
			$this->hash  = $db->escape_value($this->hash);
			$time = strftime("%Y-%m-%d %H:%M:%S", time() - 7200);
			
			$sql = "SELECT * FROM password_change WHERE user_id='{$this->id}' AND hash='{$this->hash}' AND time >='{$time}' LIMIT 1";
		
			$result_array = $db->query($sql);
			$request_exists = $db->fetch_array($result_array);
			if($request_exists) {
				if(User::change_password($this->id, $this->password)) {
					$session->message(_("Lozinka je uspješno promjenjena"), "success");
					redirect_to("index.php");
				} else {
					$this->errors['error'] = _("Greška na poslužitelju. Pokušajte kasnije.") ;
					$this->show_errors() ;
				}
			
			} else {
				$session->message(_("Zahtjev za novom lozinkom ne postoji ili je istjekao.") , "danger");
				redirect_to("index.php");
			}
		}
		
		
		
		
				
		// Form creation
		
		public function create_form() {
			global $get_id;
			global $get_hash;
			
			$this->start_form("reset_password.php");
			$this->add_element("password", _("Nova lozinka"), "password", false);
			$this->add_element("password2", _("Ponovite lozinku"), "password", false);
			$this->add_element_hidden("id", $get_id);
			$this->add_element_hidden("hash", $get_hash);
			$this->end_form();
		}
}



?>