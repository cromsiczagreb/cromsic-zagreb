<?php
ini_set("include_path", "/home/cromsich/php:" . ini_get("include_path")  );
require_once 'Services/Mailman.php';



class RegisterForm extends Form {
	
		public $errors;
		
		public $jmbag;
		public $password;
		public $prezime;
		public $ime;
		public $mail;
		public $mobitel;
		public $datum_rod;
		public $studij;
		public $godina_sad;
		public $godina_upis;
		public $registered;
	
		public $password2;
		
		
		
		// Individual process
		
		public function individual_process() {
				$this->validate();
				$this->show_errors($this->errors);
				if(empty($this->errors)) {
					 $this->prepare();
					 $this->finalize();
				}
		}
		
		// Validate
		
		private function Validate() {

			// check if passwords match
			if(!($this->password == $this->password2)) { $this->errors['password'] = _("Lozinke se ne podudaraju.") ;}
			// check if user already exists
			$user = User::find_by_jmbag($this->jmbag);
			if(!empty($user)) { $this->errors['korisnik'] = _("Korisnik s tim JMBAG-om već postoji") ;}
						
		}
		
		// Prepare
		
		private function prepare() {
			
			//$this->ime = ucwords(strtolower($this->ime));
			//$this->prezime = ucwords(strtolower($this->prezime));
			//$this->studij = ucwords(strtolower($this->studij));
			
			//Hash password
			$this->password = password_hash($this->password, PASSWORD_DEFAULT);
			
			// Fix date for DB
			$this->datum_rod = hr_to_sql($this->datum_rod);
			$this->registered = date("Y-m-d H:i:s");    
		
		}
		
		// Finalize
		
		private function finalize() {
			$user = new User;
			foreach(User::$db_fields as $key) {
				if(!empty($this->$key)) {
					$this->$key = s($this->$key);
					$this->$key = h($this->$key);
					$user->$key = $this->$key;
				} else {
					$user->$key = NULL;
				}
			}
			//$user->save();
			if($user->save()) {
				global $session;	
				// If user saved, check whether to subscribe
				if (isset($_POST["mailcheck"])) {
					$mm_service = new Services_Mailman(MM_URL, MM_USER, MM_PASSWORD);
					try {
    					$mm_service->subscribe($user->mail);
					} catch (Services_Mailman_Exception $e) {
    				$e->getMessage();
					} 
				}
				
				$session->message(_("Uspješno"), "success");
	 		 	redirect_to('index.php');
			} else {
				$session->message(_("Greška na poslužitelju. Pokušajte kasnije."), "danger");
				redirect_to('index.php');
			}

		}
			
		
		
	
		
		// Form creation
		
		public function create_form() {
			global $lang;
			$this->start_form("register.php");
			$this->add_element("jmbag", "JMBAG", "text", true, $this->jmbag, "JMBAG");
			$this->add_element("password", "Password","password", false, $this->password, "Password");
			$this->add_element("password2", _("Ponovite password"),"password", false, $this->password2, _("Ponovite password"));
			echo "<br /><br />";
			$this->add_element("prezime", _("Prezime"), "text" , true, $this->prezime, _("Prezime"));
			echo "<br />";
			$this->add_element("ime", _("Ime"), "text", true, $this->ime, _("Ime") );
			echo "<br />";
			$this->add_element("mail", _("E-mail"), "email" , false, $this->mail, _("E-mail"));
			echo "<br />";
			$this->add_element("mobitel", _("Broj mobitela") , "number" , true, $this->mobitel, "0901234567");
			$this->add_datepicker("datum_rod", _("Datum rođenja"), "text", false, $this->datum_rod , _("Datum rođenja"), "datepicker", "");
			echo "<br /><br />";
			$this->add_element("studij" , _("Studij"), "text", false, $this->studij, _("Medicina"));
			$this->add_element("godina_sad" , _("Godina studija"), "number", true, $this->godina_sad, _("Godina studija"));
			$this->add_element("godina_upis", _("Akademska godina upisa"), "text", true, $this->godina_upis, "2012/2013");
			$this->add_element_hidden("jezik", $lang);
			echo "<br /><br />";
			$this->add_checkbox("mailcheck", _("Želim primati obavijesti o događanjima u CroMSICu"), "checked", false, "mailing_list");
			echo "<br /><br />";
			$this->end_form();
			echo "<br /><br /><br />";
		}
		

	
	
}




?>