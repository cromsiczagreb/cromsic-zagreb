<?php

ini_set("include_path", '/home/cromsich/php:' . ini_get("include_path")  );
require_once 'Services/Mailman.php';

class ProfileSettings extends Form {
	
			
		private $user;
			
	//	Construct
		
		function __construct() {
			global $session;
			
			//User first to compare mails
			$user = User::find_by_id($session->user_id);
			if(!is_object($user)) { 
			$session->message(_("Greška na poslužitelju. Pokušajte kasnije.") , "danger");
			redirect_to("index.php") ;
			}
			
			
			// (Un)Subscribe if button checked
			if(isset($_POST["submit"])) {
				$mm_service = new Services_Mailman(MM_URL, MM_USER, MM_PASSWORD);
    			if (isset($_POST["mailcheck"])) {
					if($user->mail != $_POST['mail']){
						try {
					
						$mm_service->unsubscribe($user->mail);
						} catch (Services_Mailman_Exception $e) {
    					$e->getMessage();
						} 
					}
					try {
    				$mm_service->subscribe($_POST['mail']);
					$session->message(_("Uspješno"), "success");
				} catch (Services_Mailman_Exception $e) {
    				$e->getMessage();
				} 
				}else {			
				try {
    				$mm_service->unsubscribe($user->mail);
					$session->message(_("Uspješno"), "success");
				} catch (Services_Mailman_Exception $e) {
					$e->getMessage();
			    }				
			    }
			}	
			
			
			
			
			
			if (isset($_POST['submit'])) {
				
				$remove_submit = array_pop($_POST);
				$attributes = $_POST;
				$attributes['id'] = $session->user_id;
				$this->errors = array();
				foreach ($attributes as $key => $value) {
					$this->$key 		= $value;
				}
				$this->individual_process();	
			}
			
			
			
			$this->create_form($user);	
			
			
		}
		
	
		// Individual process
		
		public function individual_process() {
					 $this->prepare();
					 $this->finalize();
		}
		
		// Prepare
		
		private function prepare() {

			// Fix date for DB
			$this->datum_rod = hr_to_sql($this->datum_rod);
		
		}
		
		// Finalize
		
		private function finalize() {
			global $session;
			$user = new user;
			foreach(User::$db_fields as $key) {
				if(!empty($this->$key)) {
					$this->$key = s($this->$key);
					$this->$key = h($this->$key);
					$user->$key = $this->$key;
				} else {
					$user->$key = NULL;
				}
			}
			//$user->save();
			if($user->save()) {
				
				$session->message(_("Uspješno"), "success");
	 		 	redirect_to("settings.php");
			} else {
				//$session->message(_("Greška na poslužitelju. Pokušajte kasnije."));
				redirect_to("settings.php");
			}
		}
		
		
		// Create form
		
		public function create_form($user) {

			
			$this->start_form("settings.php");
			echo "<p><strong>JMBAG:</strong>";
			echo "&nbsp &nbsp &nbsp $user->jmbag</p>";
			echo "<p><strong>" ._("Prezime").":</strong>";
			echo "&nbsp &nbsp $user->prezime</p>";
			echo "<p><strong>"._("Ime") . ":</strong>";
			echo "&nbsp &nbsp &nbsp &nbsp &nbsp &nbsp $user->ime</p>";
			/* MAKNUTO JER SE INAČE MOŽE MJENJAT
			$this->add_element("jmbag", "JMBAG ", "text", false, $user->jmbag, "", "", "disabled");
			$this->add_element("prezime", "Prezime ", "text" , false, $user->prezime, "", "", "disabled");
			$this->add_element("ime", "Ime ", "text", false, $user->ime, "", "", "disabled");*/
		
			$this->add_element("mail", _("E-mail"), "email" , false, $user->mail);
			echo "<br />";
			$this->add_element("mobitel", _("Broj mobitela"), "text" , false, $user->mobitel);
			$this->add_datepicker("datum_rod", _("Datum rođenja"), "text", false, sql_to_hr($user->datum_rod), "", "datepicker");
			$this->add_element("studij" , _("Studij"), "text", false, $user->studij);
			$this->add_element("godina_sad" , _("Godina studija"), "number", false, $user->godina_sad);
			$this->add_element("godina_upis", _("Akademska godina upisa"), "text", false, $user->godina_upis);
			echo "<br /><br />";
			//$this->add_element("registriran", "Registriran ", "text", false, sql_to_hr_datetime($user->registered), "", "", "disabled");
			
			//Prepare maling button
			$mm_service = new Services_Mailman(MM_URL, '', MM_PASSWORD);
			try {
    			$listmember = $mm_service->member($user->mail);
				foreach ($listmember as $member) {
        		$mailing_mail = $member['address'];
    			}
				if($mailing_mail == $user->mail){
				$mailcheck = "checked";
				} else {
					$mailcheck = "";
					}
				} catch (Services_Mailman_Exception $e) {
    				$mailcheck = "";
					//echo 'Error: ' . $e->getMessage();
			}
			
			$this->add_checkbox("mailcheck", _("Pretplati me na mailing listu"), $mailcheck, false, "mailing_list");
			
			
			echo "<br /><br /><br />";
			echo "<p><strong>" ._("Registriran") . ":</strong>";
			echo "&nbsp &nbsp &nbsp " . sql_to_hr_datetime($user->registered) ."</p>";
			$this->end_form();
			echo "<br /><br /><br />";
		}
		
		
		
		
}




?>