<?php

if(!($session->is_logged_in())) {
	$session->message("Morate se prijaviti da bi prijavili aktivnosti.", "warning");	
	redirect_to("index.php");
}



class WaitingPerUser extends Controller {
	
	
	// Table options
	
	public $columns						= array('odbor', 'ime','opis', 'bodovi');
	public $data_url 					= "core/apply_old.php";
	public $action						= "apply_old_edit";
	public $create_new_link 			= "#";
	public $modal						= true;
	public $action_icon					= "glyphicon-plus";
	
		
	function __construct() {
		
		global $session;
		
		echo "<h3>". _("Na čekanju") . ": </h3>";
		
		
		
		$this->search = "false";
		$this->data_url = $this->data_url . "?action=waiting";
		$this->create_table();

	}
	
}

class VerifiedPerUser extends Controller {
	
	
	// Table options
	
	public $columns						= array('odbor', 'ime', 'opis','bodovi');
	public $data_url 					= "core/apply_old.php";
	public $action						= "";
	//public $create_new_link 			= "";
	public $modal						= false;
	public $action_icon					= "glyphicon-plus";
	
		
	function __construct() {
		
		echo "<h3> ". _("Odobrene aktivnosti") .  "</h3>";
		$this->search = "false";
		$this->data_url = $this->data_url . "?action=verified";
		$this->create_table();
	
	}
	
}



class RejectedPerUser extends Controller {
	
	
	// Table options
	
	public $columns						= array('odbor', 'ime','opis', 'razlog');
	public $data_url 					= "core/apply_old.php";
	public $action						= "apply_old_edit";
	//public $create_new_link 			= "";
	public $modal						= false;
	public $action_icon					= "glyphicon-plus";
	
		
	function __construct() {
		echo "<h3>".  _("Odbijeno") . ": </h3>";
		$this->search = "false";
		$this->data_url = $this->data_url . "?action=rejected";
		$this->create_table();

	}
	
}


class ApplyOldForm extends Form {

		
		public $ime;
		public $opis;
		// Create form
		
		function __construct($type, $title) {
			$modal_id	= $type . "_modal";
			$button_id	= $type . "_button";
			$form_id	= $type . "_form";
			$form_type  = $type;
			$this->create_modal_form($modal_id, $title, $button_id, $form_id, $form_type);
		}
		
		public function create_modal_form($modal_id, $modal_title, $button_id, $form_id, $form_type) {
			$this->start_modal($modal_id, $modal_title);
			$this->input_form($form_id, $form_type) ;
			$this->end_modal($button_id);
			
		}

		// Input form
		
		public function input_form($form_id, $form_type) {
			$IDs = Project::find_all();
			$values[] = "";
			$values_two = array("Loading...");
			foreach ($IDs as $ID) {
				$values[] = $ID->id;
			}
			$names_origin = "name_from_id";
			echo "<div id=\"modal_form\">";
			$this->start_form("#", $form_id);
			$this->add_element_hidden("type", $form_type);
			$this->add_element_select('project_id', _("Odbor") . "&nbsp; &nbsp; &nbsp;", "", $values, "", $names_origin, "Project");
			echo "<br /><br />";
			$this->add_element_select('activity_id', _("Aktivnost") . " &nbsp;", "", "", "");
			echo "<br /><br />";
			$this->add_element("opis", _("Opis"), "text", $tooltip=false, "", _("Opis"),"opis")	;


			echo "</form></div>";
		}

}

class HelpModal extends Form {
	
			function __construct($type, $title) {
			global $lang;
			$this->start_modal_large("help_modal", _("Upute"));
			require("./locale/{$lang}/HELP/apply_old_help.php");
			$this->end_modal_buttonless();
			}
}

class SideBar extends Form {
	
		function __construct() {
		
		echo "<div id=\"container_two_thirds_right\">
<a href=\"#help\" data-toggle=\"modal\" data-target=\"#help_modal\"><i id=\"tooltip_icon\" class=\"glyphicon glyphicon-question-sign\" data-toggle=\"tooltip\" data-placement=\"right\" style=\"font-size:80px\" title=\"Pomoć\" ></i></a>
<br /><br />
<a href=\"PrijaviAktivnosti\">&nbsp &nbsp &nbsp " . _("PRIJAVI") .  "&nbsp &nbsp &nbsp </a> <br /><br />
<a href=\"Odobrene\">&nbsp" . _("ODOBRENE") .  "&nbsp &nbsp </a>  <br /><br />
<a href=\"Odbijene\">&nbsp &nbsp " ._("ODBIJENE") . " &nbsp &nbsp </a>  <br /><br /><br />";
echo "</div>"		;
		}
		
			
}

?>