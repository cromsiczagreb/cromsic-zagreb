<?php require("../includes/initialize.php");?>
<?php require("../includes/model.php") ; ?>


<?php 

class ActivityListModel extends Model {
	
	private $call_class = "activity";
	public $requested	= array('id', 'ime', 'bodovi', 'opis');
	
	
	public function individual_process() {
	
		if(!isset($_GET)) {
			echo "Invalid request";
		} else {	
			$this->get_activities($_GET['project_id']); 
			
		}
	}
		
	
	
	// Get activities
	
	private function get_activities($project_id) {
		
		$activities = Activity::get_activities($project_id);
		foreach ($activities as $activity) {
			
			
				if(empty($activity->opis)) {
					$activity->opis = " ";
				}
						$activity->opis = _("{$activity->opis}");
						
						
		}
		$this->create_JSON($activities);
	}
	
	

}



$model = new ActivityListModel();



?>
