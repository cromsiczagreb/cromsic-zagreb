<?php require("../includes/initialize.php");?>
<?php require("../includes/model.php") ; ?>


<?php 

class InstanceListModel extends Model {
	
	private $call_class = "instance";
	public $requested	= array('id', 'ime', 'bodovi', 'opis',  'datum');
	
	
	public function individual_process() {
		global $session;
		if(!isset($_GET)) {
			echo "Invalid request";
		} else {	
			switch($_GET['action']) {
				case "participations": $this->get_participations($session->user_id); break;
				case "individual": $this->get_individual($session->user_id); break;
				case "documents": $this->get_documents($session->user_id); break;
				case "entries": $this->get_entries($session->user_id); break;
				default: echo "Invalid request";
			}
		}
		
	}
	
	// Get participations
	
	private function get_participations($user_id) {
		$membership = new Instance;
		$membership->id = 0;
		$membership->ime = "Članstvo u CroMSIC-u";
		$membership->bodovi = 30;
		$membership->opis = "Bodovi za članstvo";
		$membership->datum = "";
		$instances = Instance::for_user($user_id);
		array_unshift($instances , $membership);
		$this->create_JSON($instances);
	}
	
	// Get individual
	
	private function get_individual($user_id) {
		$individual = Individual::for_user($user_id);
		$this->requested = array('id', 'opis', 'datum', 'bodovi');
		$this->create_JSON($individual);
	}
	
	// Get individual
	
	private function get_documents($user_id) {
		$individual = Document::verified_for_user($user_id);
		$this->requested = array('id', 'opis', 'datum', 'bodovi');
		$this->create_JSON($individual);
	}
	
	// Get entries
	
	private function get_entries($user_id) {
		$waiting = OldEntry::get_old_entries_verified_for_user($user_id);
		foreach ($waiting as $wait) {
			$wait->odbor = Project::name_from_id($wait->project_id);
			$wait->ime = Activity::name_from_id($wait->activity_id);
			$wait->bodovi = Activity::points_from_id($wait->activity_id);
			unset ($wait->activity_id);	
			unset ($wait->project_id);	
		}
		echo json_encode($waiting);	
	}
}



$model = new InstanceListModel();



?>
