<?php require("../includes/initialize.php");?>
<?php require("../includes/model.php") ; ?>


<?php 

class DocumentsModel extends Model {
	
	private $call_class = "document";
	public $requested	= array('id', 'kategorija', 'opis', 'bodovi', 'filename');
	
	
	public function individual_process() {
		global $session;
		if(!isset($_GET)) {
			echo "Invalid request";
			redirect_to("../documents.php");
		} else {	
			switch($_GET['action']) {
				case "verified": $this->get_verified($session->user_id); break;
				case "unverified": $this->get_unverified($session->user_id); break;
				case "delete": $this->delete_document($_GET['id'],$session->user_id); break;
				default: echo "Invalid request";
			}
		}
		
	}
	
	// Get participations
	
	private function get_verified($user_id) {
		$verified = Document::verified_for_user($user_id);
		foreach ($verified as $document) {
			$kategorija = Doc_Category::find_by_id($document->kategorija);
			if($document->kategorija == 1) {
				$document->bodovi = $document->opis * $kategorija->bodovi;
			} else {
				$document->bodovi = $kategorija->bodovi;
			}
			$document->kategorija = _("{$kategorija->ime}");
		}
		$this->create_JSON($verified);
	}
	
	// Get individual
	
	private function get_unverified($user_id) {
		$this->requested = array('id', 'kategorija', 'opis');
		$unverified = Document::unverified_for_user($user_id);
		foreach ($unverified as $document) {
			$kategorija_ime = Doc_Category::name_from_id($document->kategorija);
			$document->kategorija = _("{$kategorija_ime}");
		}
		$this->create_JSON($unverified);
	}
	
	// Delete document
	
	private function delete_document($id, $user_id) {
		$document = Document::find_by_id($id);
		$file_name = $document->filename;
		if($document->user_id == $user_id) { 
			$document->delete();
			$file = "../uploads/" . md5($user_id) . "/" . $file_name;
			unlink($file);
			echo "Success.";
		} else {
		echo "Invalid request";
		}
	}
	
}



$model = new DocumentsModel();



?>
