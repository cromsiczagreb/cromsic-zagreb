<?php

class EventView extends Form {
	
			
			private $can_edit = false;
			private $disabled = "disabled";
			public $event;
			
	//	Construct
		
		function __construct() {
			global $session;
			
			if (!isset($_GET['id'])) {
				redirect_to("index.php");	
			}
			
			$event = Signup::find_by_id($_GET['id']);
			if(!is_object($event)) { redirect_to("events.php") ; }

			if (isset($_POST['submit'])) {
				$this->individual_process($event);	
			}
			
			
			$this->create_form($event);	
			
						
		}
		
	
		// Individual process
		
		public function individual_process($event) {
					 $this->finalize($event);
		}
		
		// Prepare
		
		private function prepare() {

		
		}
		
		// Finalize
		
		private function finalize($event) {
			global $session;
			if($_POST['submit'] == _("Prijavi me")) {
				$function = "sign_up";
				$action = _("prijavili na ") . " " . $event->ime;
			} else {
				$function = "sign_off";
				$action = _("odjavili sa ") . " " . $event->ime ;
			}
			if(SignupUsers::$function($event->id, $session->user_id)) {
				$session->message(_("Uspješno ste se ") . " " . $action . ".", "success");
	 		 	redirect_to("event_view.php?id=" . $event->id);
			} else {
				$session->message(_("Dogodila se greška."), "danger");
				redirect_to("event_view.php?id=" . $event->id);
			}
		}
		
		
		// Create form
		
		public function create_form($event) {
			global $session;
			echo "<h3>{$event->ime}</h3>";
			echo "<br /><br />";
			$datum = sql_to_hr($event->datum);
			echo "<h4>"._("Datum") .":</h4> " . $datum;
			echo "<br /><br />";
			echo $event->opis;
			echo "<br /><br />";
			if($event->prijaveod < date("Y-m-d H:i:s")) {
				$this->start_form("event_view.php?id={$event->id}", "toggle_signup");
				if(SignupUsers::is_signed($event->id, $session->user_id)) {
					if($event->odjavedo > date("Y-m-d H:i:s")) {
					$this->end_form(_("Odjavi me"));
					}
					echo "<br />"._("Odjave do").":  &nbsp  &nbsp &nbsp &nbsp" . sql_to_hr_datetime($event->odjavedo);
				} else {
					$this->end_form(_("Prijavi me"));
				}
			} else {
				echo "<h4>". _("Prijave za").":</h4><p id=\"countdown\"></p>";
				echo "<script>
					var t = \"{$event->prijaveod}\".split(/[- :]/);
					var liftoff = new Date(t[0], t[1]-1, t[2], t[3], t[4], t[5] + 1);
					console.log(liftoff);
					$('#countdown').countdown({until: liftoff, format: 'yowdHMS', expiryUrl: 'Dogadanje{$event->id}'});  
					</script>	";
			}
			
		}
		
		
		
		
}


class SignupUsersRequiredList extends Controller {
	
	
	
	
	// Table options
	
	public $columns 					= array	('broj','prezime', 'ime');
	public $data_url 					= "core/events_model.php";
	public $action						= "";
	public $action_icon					= "";
	public $create_new_link 			= "";
	public $modal						= false;
	
		
	function __construct($signup_id) {
		echo "<h3>" ._("Prijavljeni") . "</h3>";
		$this->data_url = $this->data_url . "?action=get_list_required&signup_id={$signup_id}";
		$this->create_new_link = $this->create_new_link . "?signup_id={$signup_id}";
		$this->create_table();
	}
	

	
}

class SignupUsersSubsList extends Controller {
	
	
	
	
	// Table options
	
	public $columns 					= array	('broj','prezime', 'ime');
	public $data_url 					= "core/events_model.php";
	public $action						= "";
	public $action_icon					= "";
	public $create_new_link 			= "";
	public $modal						= false;
	
		
	function __construct($signup_id) {
		echo "<h3>" ._("Zamjene")."</h3>";
		$this->data_url = $this->data_url . "?action=get_list_subs&signup_id={$signup_id}";
		$this->create_new_link = $this->create_new_link . "?signup_id={$signup_id}";
		$this->create_table();
	}
	

	
}






?>