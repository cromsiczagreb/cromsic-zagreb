<?php require("../includes/initialize.php");?>
<?php require("../includes/model.php") ; ?>


<?php 

class OldEntryModel extends Model {
	
	private $call_class = "Project";
	public $requested = array('id', 'project_id', 'activity_id', 'user_id', 'opis');
	
	
	public function individual_process() {
		$this->create_oldentry();
	}
	
	
	private function create_oldentry() {
			global $session;
			
			$this->user_id = $session->user_id;
			//$remove_submit = array_pop($_POST);
			$attributes = $_POST;
			$this->errors = array();
			foreach ($attributes as $key => $value) {
				$this->$key 		= $value;
			}
			
			$oldentry = new OldEntry;
			foreach(OldEntry::$db_fields as $key) {
				if(!empty($this->$key)) {
					$oldentry->$key = $this->$key;
				} else {
					$oldentry->$key = NULL;
				}
			}
			
			if($oldentry->save()) {
				echo "Success.";
			} else {
				echo "Failed.";
			}
			
			
	}
	
}


$model = new OldEntryModel();



?>