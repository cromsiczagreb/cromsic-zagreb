<?php

class LoginForm extends Form {
	
		public $errors =array();
				
		public $username;
		public $password;
		
		
		
		// Individual process
		
		public function individual_process() {
				$this->validate();
				$this->show_errors($this->errors);
				
				if(empty($this->errors)) {
					
					 $this->prepare();
					 $this->finalize();
				}
		}
		
		// Validate
		
		private function Validate() {
		}
		
		// Prepare
		
		private function prepare() {
		}
		
		// Finalize
		
		private function finalize() {
			global $session;
			$found_user = User::authenticate($this->username, $this->password);
			if($found_user) {
				$session->login($found_user);
				redirect_to("index.php");
			} else {
				$this->errors['error'] = _("JMBAG / password netočan.");
				$this->show_errors($this->errors);
			}
		}
				
		// Form creation
		
		public function create_form() {
			$this->start_form("login.php");
			$this->add_element("username", "JMBAG ", "text", false, $this->username, "JMBAG");
			$this->add_element("password", "Password ","password", false, "", "Password");
			$this->end_form();
		}
}

?>