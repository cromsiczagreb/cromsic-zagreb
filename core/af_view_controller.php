<?php

if(!($session->is_logged_in())) {
	$session->message(_("Morate se prijaviti da bi prijavili razmjenu."), "warning");	
	redirect_to("index.php");
}


$application = Application::find_by_user_id($session->user_id);
if(empty($application) or ($application->status < 2)) { 
		redirect_to("af_info.php");
	
}





class ViewApp extends Form {
	
	
	function __construct($application) {
		
		    global $session;
			$user = User::find_by_id($session->user_id);
			
			/*$total = $user->count_points($session->user_id);
			$general = $user->count_from_general($session->user_id);
			$activities = $user->count_from_activities($session->user_id);
			$postotak = round((($general / $total) * 100)) . "%";
			
			if($general > $activities) {
				$finalpoints = $activities * 2;
			} else {
				$finalpoints = $general + $activities;	
			}*/
			
			$checked_f = "";
			$checked_m = "";
			$checked_p = "";
			$checked_r = "";
		
			if($application->spol == 1) { $spol = _("M") ; } else { $spol = _("Ž");}
			if ($application->type == 1) { $checked_p = "checked" ; $checked_r = "disabled";}
			if ($application->type == 2) { $checked_r = "checked" ; $checked_f = "disabled";}
			
			
			echo "<div id=\"appform\">";
			/*echo "<div class=\"square_middle\"> <h4>&nbsp Bodovi na natječaju </h4> <br />";
			echo "<div class=\"square_left\"> ";
			echo "<p><strong>Ukupno:</strong>";
			echo "&nbsp &nbsp ". $total ."</p><p> &nbsp </p>";
			echo "<p style=\"color:#11ABA8\"><strong>Korigirano:</strong>";
			echo "&nbsp &nbsp ". $finalpoints ."</p>";
			echo "</div>";
			echo "<div class=\"square_right\"> ";
			echo "<p><strong>Opći dio:</strong>";
			echo "&nbsp &nbsp ". $general ."</p>";
			echo "<p><strong>CroMSIC:</strong>";
			echo "&nbsp &nbsp ". $activities ."</p>";
			echo "<p><strong>Postotak:</strong>";
			echo "&nbsp &nbsp ". $postotak ."</p>";
			echo " </div></div>";*/
			echo "<div class=\"square_middle\" style=\"text-align: center\"><h4>" . _("Vrsta razmjene") . "</h4><br />";
			echo "<div class=\"cc-selector\">
			<div class=\"square_left\"> 
        <input id=\"scope\" type=\"radio\" name=\"type\" required {$checked_p} value=\"1\" />
        <label class=\"drinkcard-cc scope\" for=\"scope\"></label></div>
        <div class=\"square_right\"><input id=\"score\" type=\"radio\" name=\"type\" {$checked_r} value=\"2\" />
        <label class=\"drinkcard-cc score\" for=\"score\"></label></div>
    </div>";
			echo "</div>";
			echo "<div class=\"square_middle\">";
			echo "<div class=\"square_left\"> <h4>" . _("Opći podaci") . "</h4> ";
			echo "<br />";
			echo "<p><strong>". _("Ime") . ":</strong>";
			echo "&nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp $user->ime</p>";
			echo "<p><strong>". _("Prezime") . ":</strong>";
			echo "&nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp  $user->prezime</p>";
			echo "<p><strong>" . _("Nacionalnost") . ":</strong>";
			echo "&nbsp &nbsp &nbsp  $application->nacionalnost</p>";
			echo "<p><strong>" . _("Materinji jezik") . ":</strong>";
			echo "&nbsp &nbsp $application->jezik</p>";
			echo "<p><strong>". _("Spol") . ":</strong>";
			echo "&nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp $spol";
			echo "</div>";
			echo "<div class=\"square_right\"> <h4>" . _("Kontakt") . "</h4> <br /> ";
			echo "<p><strong>" ._("Ulica i kućni broj") . ":</strong>";
			echo "&nbsp &nbsp &nbsp  $application->adresa</p>";
			echo "<p><strong>" ._("Mjesto i poštanski broj") . ":</strong>";
			echo "&nbsp &nbsp &nbsp  $application->mjesto</p>";
			echo "<p><strong>" . _("Fiksni") . ":</strong>";
			echo "&nbsp &nbsp &nbsp  $application->fiksni</p>";
			echo "<p><strong>". _("Mobitel") . ":</strong>";
			echo "&nbsp &nbsp $user->mobitel</p>";
			echo "<p><strong>" . _("E-mail") . ":</strong>";
			echo "&nbsp &nbsp &nbsp   $user->mail</p>";
			echo "<br />";
			echo "</div></div></div>";
			echo "<div class=\"square_middle\"> <h4>&nbsp" . _("Prošle razmjene") . "</h4> <br />";
			echo "<p><strong>&nbsp &nbsp" . _("Jeste li već ste bili na razmjeni preko CroMSIC-a?") . "</strong>(" . _("navesti državu i datum zadnje razmjene") . ")<br /><br />";
			echo "&nbsp &nbsp &nbsp  $application->razmjena</p>";
			echo "</div>";
			echo "<div class=\"square_middle\"> <h4>&nbsp " . _("Status na fakultetu") . "</h4> <br />";
			echo "<div class=\"square_left\"> ";
			echo "<p><strong>". _("Godina upisa na 1. godinu") . ":</strong>";
			echo "&nbsp &nbsp $user->godina_upis</p>";
			echo "<p><strong>". _("Godina upisa na 4. godinu") . ":</strong>";
			echo "&nbsp &nbsp $application->upisiv</p>";
			echo " </div>";
			echo "<div class=\"square_right\"> ";
			echo "<p><strong>". _("Trenutačna studijska godina") . ":</strong>";
			echo "&nbsp &nbsp $user->godina_sad</p>";
			echo "<p><strong>" . _("Prosjek ocjena") . ":</strong>";
			echo "&nbsp &nbsp ". $user->get_average($user->id) ."</p>";
			echo "</div></div>";
			
		}
		
		
	}




?>