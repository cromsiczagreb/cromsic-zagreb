<?php require("../includes/initialize.php");?>
<?php require("../includes/model.php") ; ?>


<?php 

class EventsModel extends Model {
	
	private $call_class = "signup";
	public $requested	= array('id', 'datum', 'ime', 'opis',  );
	
	
	public function individual_process() {
		
		global $session;
		if(!isset($_GET) or !isset($_POST)) {
			echo "Invalid request";
			redirect_to("../index.php");
		} elseif(isset($_GET['action'])) {
			switch($_GET['action']) {
				case "get_list_future": $this->get_list_future(); break;
				case "get_list_past": $this->get_list_past(); break;
				case "get_list_required": $this->get_list_required($_GET['signup_id']); break;
				case "get_list_subs": $this->get_list_subs($_GET['signup_id']); break;
				default: echo "Invalid request";
			}
		} else {	
			switch($_POST['action']) {
				case "signup": $this->signup_user($_GET['id']); break;
				case "signoff": $this->signoff_user($_GET['id']); break;
				default: echo "Invalid request";
			}
		}
		
	}
	
	// Get list future
	
	private function get_list_future() {
		$class = $this->call_class;
		$events = $class::find_future();
		foreach ($events as $event) {
		$event->ime = "<a class=\"link_colorless\" href=\"Dogadanje{$event->id}\"><div>{$event->ime}</div></a>";
		}
		$this->create_JSON($events);
	}
	
	// Get list past
	
		private function get_list_past() {
		$class = $this->call_class;
		$events = $class::find_past();
		$this->create_JSON($events);
	}
	
	
	// Get list of participants - required
	
	private function get_list_required($signup_id) {
		$this->requested = array('broj', 'id', 'prezime', 'ime', 'jmbag');
		$broj = 1;
		$participants = SignupUsers::for_instance_required($signup_id);
		foreach ($participants as $participant) {
			$participant->broj = $broj;
			$broj ++;
		}
		$this->create_JSON($participants);
	}
	
	// Get list of participants - subbs
	
	private function get_list_subs($signup_id) {
		$this->requested = array('broj', 'id', 'prezime', 'ime', 'jmbag');
		$signup = Signup::find_by_id($signup_id);
		$broj = $signup->potrebno + 1;
		$participants = SignupUsers::for_instance_subs($signup_id);
		foreach ($participants as $participant) {
			$participant->broj = $broj;
			$broj ++;
		}
		$this->create_JSON($participants);
	}
	
	
	
}



$model = new EventsModel();



?>
