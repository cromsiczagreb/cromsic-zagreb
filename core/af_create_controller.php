<?php

if(!($session->is_logged_in())) {
	$session->message(_("Morate se prijaviti da bi prijavili razmjenu."), "warning");	
	redirect_to("index.php");
}

if(!check_date_is_within_range('2016-10-26 00:00:00', '2018-11-05 20:00:00', date("Y-m-d G:i:s"))){
    redirect_to("af_info.php");
}

/*if (!User::has_complaint($session->user_id)){
         redirect_to("index.php");
}
*/

$application = Application::find_by_user_id($session->user_id);


if(!empty($application)) { 
		if ($application->status == 1) {
		redirect_to("af_edit.php");	
	} else {
		redirect_to("af_view.php");
	}	
}

class CreateApp extends Form {
	
		public $errors;
		
	    public $nacionalnost;
		public $jezik;
		public $spol;
		public $adresa;
		public $mjesto;
		public $fiksni;
		public $razmjena;
		public $upisiv;
		
	
		public $password2;
		
		
		
		// Individual process
		
		public function individual_process() {
				//$this->validate();
				$this->show_errors($this->errors);
				if(empty($this->errors)) {
					 $this->prepare();
					 $this->finalize();
				}
		}
		
		// Validate
		
		private function Validate() {

						
		}
		
		// Prepare
		
		private function prepare() {
			
			//$this->ime = ucwords(strtolower($this->ime));
			
   
		
		}
		
		// Finalize
		
		private function finalize() {
			global $session;
			$application = new Application;
			$application->user_id = $session->user_id;
			foreach(Application::$db_fields as $key) {
				if(!empty($this->$key)) {
					$this->$key = s($this->$key);
					$this->$key = h($this->$key);
					$application->$key = $this->$key;
				} else {
					$application->$key = NULL;
				}
			}
			$application->user_id = $session->user_id;
			if($application->save()) {
				global $session;
				$session->message(_("Uspješno ste prijavili razmjenu. Možete unositi promijene ili zatvoriti stranicu."), "success");
	 		 	redirect_to('af_edit.php');
			} else {
				$session->message(_("Greška na poslužitelju. Pokušajte kasnije."), "danger");
				redirect_to('af_info.php');
			}
			
		}
			
		
		
	
		
		// Form creation
		
		public function create_form() {
			
			global $session;
			$user = User::find_by_id($session->user_id);
			
			$checked_f = "";
			$checked_m = "";
			$checked_p = "";
			$checked_r = "";
		
			if($this->spol == 1) { $checked_m = "checked" ;}
			if($this->spol == 2) { $checked_f = "checked" ;}
			if ($this->razmjena == 1) { $checked_p = "checked" ;}
			if ($this->razmjena == 2) { $checked_r = "checked" ;}
			
			
			$this->start_form("af_create.php", "appform");	
			echo "<div id=\"appform\">";
			echo "<div class=\"square_middle\" style=\"text-align: center\"><h4>". _("Vrsta razmjene") . "</h4><br />";
			echo "<div class=\"cc-selector\">
			<div class=\"square_left\"> 
        <input id=\"scope\" type=\"radio\" name=\"type\" required {$checked_p} value=\"1\" />
        <label class=\"drinkcard-cc scope\" for=\"scope\"></label></div>
        <div class=\"square_right\"><input id=\"score\" type=\"radio\" name=\"type\" {$checked_r} value=\"2\" />
        <label class=\"drinkcard-cc score\" for=\"score\"></label></div>
    </div>";
			echo "</div>";
			echo "<div class=\"square_middle\">";
			echo "<div class=\"square_left\"> <h4>". _("Opći podaci") . "</h4> ";
			echo "<br />";
			$this->add_element("nacionalnost", _("Nacionalnost"), "text" , false, $this->nacionalnost, _("Nacionalnost"));
			echo "<br />";
			$this->add_element("jezik", _("Materinji jezik"), "text" , false, $this->jezik, _("Materinji jezik"));
			echo "<br /><br /><strong>". _("Spol") . "</strong> <br /> <br/><input type=\"radio\" required name=\"spol\" {$checked_m} value=\"1\" >". _("M") . " &nbsp &nbsp <input type=\"radio\" name=\"spol\" {$checked_f} value=\"2\">". _("Ž");
			echo "</div>";
			echo "<div class=\"square_right\"> <h4>". _("Kontakt") . "</h4> <br /> ";
			$this->add_element("adresa", _("Ulica i kućni broj"), "text" , false, $this->adresa, _("Ulica i kućni broj"));
			echo "<br />";
			$this->add_element("mjesto", _("Mjesto i poštanski broj"), "text" , false, $this->mjesto, _("Mjesto i poštanski broj"));
			echo "<br />";
			$this->add_element("fiksni", _("Broj fiksnog telefona") , "text" , false, $this->fiksni, "01 101 1010");
			echo "<br /><br />";
			echo "<p><strong>" . _("Mobitel") . ":</strong>";
			echo "&nbsp &nbsp $user->mobitel</p>";
			echo "<p><strong>" . _("E-mail") . ":</strong>";
			echo "&nbsp &nbsp &nbsp   $user->mail</p>";
			echo "<br />";
			echo "</div></div></div>";
			echo "<div class=\"square_middle\"> <h4>". _("Prošle razmjene") . "</h4> <br />";
			$this->add_element("razmjena", _("Jeste li već ste bili na razmjeni preko CroMSIC-a? (navesti državu i datum zadnje razmjene)"). " <br /> ","text" , false, $this->razmjena, "");
			echo "</div>";
			echo "<div class=\"square_middle\"> <h4>". _("Status na fakultetu") ." </h4> <br />";
			echo "<div class=\"square_left\"> ";
			echo "<p><strong>" . _("Godina upisa na 1. godinu") . ":</strong>";
			echo "&nbsp &nbsp $user->godina_upis</p>";
			$this->add_element("upisiv", "<strong>" . _("Godina upisa na 4. godinu"). ":</strong> ", "text", false, $this->upisiv, "2012/13");
			echo " </div>";
			echo "<div class=\"square_right\"> ";
			echo "<p><strong>". _("Trenutačna studijska godina") . ":</strong>";
			echo "&nbsp &nbsp $user->godina_sad</p>";
			echo "<p><strong>". _("Prosjek ocjena"). ":</strong>";
			echo "&nbsp &nbsp ". $user->get_average($user->id) ."</p>";
			echo "</div></div>";
			echo "<div id=\"app_button\">";
			$this->end_form(_("Prijavi razmjenu"));
			echo "</div>";
		}
		

	
	
}




?>