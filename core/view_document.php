<?php require("../includes/initialize.php");?>

<?php
if(!isset($_GET)) {
	redirect_to("../documents.php");
} else {
	if(Document::is_uploader($_GET['id'], $session->user_id)) {
		$link  = "../uploads/";
		$link .= md5($session->user_id) . "/" . Document::filename_from_id($_GET['id']);

		redirect_to($link);
 	/*header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename='.basename($link));
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize($link));
    ob_clean();
    flush();
    readfile($link);
    exit;*/
		
	} else {
		redirect_to("../documents.php");
	}
}
?>