<?php

if(!($session->is_logged_in())) {
	$session->message(_("Morate se prijaviti da bi predali žalbu."), "warning");	
	redirect_to("index.php");
}



$status = _("Nije unesena.");
$complaint = Complaint::find_by_user_id($session->user_id);
if(!empty($complaint)) { 
		$complaint_id = $complaint->id;
		if($complaint->status == 0) {
			$status = _("Na čekanju") . ".";
		} else {
			$status = _("U obradi") . ".";
		}
}


class ComplaintForm extends Form {
	
		public $errors;
		
	    public $content;
		
		
		
		//	Construct
		
		function __construct() {
			global $session;
			
			
			if (isset($_POST['submit'])) {
				
				$remove_submit = array_pop($_POST);
				$attributes = $_POST;
				$attributes['user_id'] = $session->user_id;
				$this->errors = array();
				foreach ($attributes as $key => $value) {
					$this->$key 		= $value;
				}
				$this->individual_process();	
			}
			
			$complaint = Complaint::find_by_user_id($session->user_id);
			if(!is_object($complaint)) { 
			$complaint = new Complaint;
			 }
			
			$this->create_form($complaint);	
			
			
		}
		
		// Individual process
		
		public function individual_process() {
				//$this->validate();
				$this->show_errors($this->errors);
				if(empty($this->errors)) {
					 //$this->prepare();
					 $this->finalize();
				}
		}
		
		// Validate
		
		private function Validate() {

						
		}
		
		// Prepare
		
		private function prepare() {
			
			//$this->ime = ucwords(strtolower($this->ime));
			
   
		
		}
		
		// Finalize
		
		private function finalize() {
			global $session;
			global $complaint_id;
			
			$complaint = new Complaint;
			$complaint->user_id = $session->user_id;
			foreach(Complaint::$db_fields as $key) {
				if(!empty($this->$key)) {
					$this->$key = s($this->$key);
					$this->$key = h($this->$key);
					$complaint->$key = $this->$key;
				} else {
					$complaint->$key = NULL;
				}
			}
			$complaint->user_id = $session->user_id;
			$complaint->id = $complaint_id;
		
			
			if($complaint->save()) {
				global $session;
				$session->message(_("Uspješno"), "success");
	 		 	redirect_to('complaint.php');
			} else {
				$session->message(_("Nije učinjena nikakva promjena."), "warning");
				redirect_to('complaint.php');
			}
			
		}
			
		
		
	
		
		// Form creation
		
		public function create_form($complaint) {
			
			global $session;
			global $status;
		
						
			
			$this->start_form("complaint.php", "appform");	
			$this->add_element_textarea("content", "", "", "", _("Ukratko opišite problem ovdje."), "content","style=\"height:150px\"",  $complaint->content);
			echo "<br />";
			$this->end_form(_("Spremi"));
			echo "<br />";
			echo "<strong>"._("Status"). ":</strong> &nbsp &nbsp &nbsp &nbsp {$status}";
			
			
		}
		

	
	
}

?>