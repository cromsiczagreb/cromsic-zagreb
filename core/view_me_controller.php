<?php

$bodovi = User::count_points($session->user_id);

class DocumentPerUser extends Controller {
	
	
	// Table options
	
	public $columns						= array('kategorija', 'opis','bodovi');
	public $data_url 					= "core/documents_model.php";
	public $action						= "";
	//public $create_new_link 			= "instance_list.php";
	public $modal						= false;
	public $action_icon					= "glyphicon-plus";
	
		
	function __construct() {
		echo "<h3>" . _("Moji uspjesi") .  "</h3>";
		$this->data_url = $this->data_url . "?action=verified";
		$this->create_table();
	}
	
}


class EntriesPerUser extends Controller {
	
	
	// Table options
	
	public $columns						= array('odbor', 'ime', 'opis','bodovi');
	public $data_url 					= "core/view_me_model.php";
	public $action						= "";
	//public $create_new_link 			= "instance_list.php";
	public $modal						= false;
	public $action_icon					= "glyphicon-plus";
	
		
	function __construct() {
		echo "<h3>" . _("Prijavljene aktivnosti") ."</h3>";
		$this->data_url = $this->data_url . "?action=entries";
		$this->create_table();
	}
	
}

class InstancePerUser extends Controller {
	
	
	// Table options
	
	public $columns						= array('ime', 'datum','bodovi');
	public $data_url 					= "core/view_me_model.php";
	public $action						= "";
	//public $create_new_link 			= "instance_list.php";
	public $modal						= false;
	public $action_icon					= "glyphicon-plus";
	
		
	function __construct() {
		echo "<h3>" . _("Moja sudjelovanja") ."</h3>";
		$this->data_url = $this->data_url . "?action=participations";
		$this->create_table();
	}
	
}


class IndividualPerUser extends Controller {
	
	
	
	
	// Table options
	
	public $columns						= array('opis', 'datum','bodovi');
	public $data_url 					= "core/view_me_model.php";
	public $action						= "";
	//public $create_new_link 			= "individual_create.php";
	public $modal						= false;
	public $action_icon					= "glyphicon-plus";
	
		
	function __construct() {
		echo "<h3>" . _("Pojedinačni unosi") .  "</h3>";
		$this->data_url = $this->data_url . "?action=individual";
		$this->create_table();
	}
	

}









?>