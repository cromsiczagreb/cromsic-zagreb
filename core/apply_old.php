<?php require("../includes/initialize.php");?>
<?php require("../includes/model.php") ; ?>


<?php 

class ApplyOld extends Model {
	
	
	public $requested = array  ("id", "ime", "bodovi", "opis");
	
	public function individual_process() {
		global $session;
		if(!isset($_GET)) {
			echo "Invalid request";
			redirect_to("../documents.php");
		} else {	
			switch($_GET['action']) {
				case "get_activities": $this->get_activities($_GET['id']); break;
				case "activity": $this->activity_info($_GET['id']); break;
				case "waiting": $this->get_waiting($session->user_id); break;
				case "verified": $this->get_verified($session->user_id); break;
				case "rejected": $this->get_rejected($session->user_id); break;
				case "delete": $this->delete_entry($_GET['id'],$session->user_id); break;
				default: echo "Invalid request";
			}
				
		}
	
		
	}
	
	
	private function get_activities($id) {
		$activities = Activity::get_activities($id);
		$this->create_JSON($activities);
	}
	
	private function activity_info($id) {
		$activity = Activity::find_by_id($id);
		$activity->opis = _("{$activity->opis}");
		echo json_encode($activity);	
	}
	
	
	// Get waiting
	
	private function get_waiting($user_id) {
		$waiting = OldEntry::get_old_entries_waiting_for_user($user_id);
		foreach ($waiting as $wait) {
			$wait->odbor = Project::name_from_id($wait->project_id);
			$wait->ime = Activity::name_from_id($wait->activity_id);
			$wait->bodovi = Activity::points_from_id($wait->activity_id);
			unset ($wait->activity_id);	
			unset ($wait->project_id);	
		}
		echo json_encode($waiting);	
	}
	
	
	// Get verified
	
	private function get_verified($user_id) {
		$waiting = OldEntry::get_old_entries_verified_for_user($user_id);
		foreach ($waiting as $wait) {
			$wait->odbor = Project::name_from_id($wait->project_id);
			$wait->ime = Activity::name_from_id($wait->activity_id);
			$wait->bodovi = Activity::points_from_id($wait->activity_id);
			unset ($wait->activity_id);	
			unset ($wait->project_id);	
		}
		echo json_encode($waiting);	
	}
	
	// Get rejected
	
	private function get_rejected($user_id) {
		$this->requested = array  ("id", "ime",  "razlog","opis" );
		
		$rejected = OldEntry::get_old_entries_rejected_for_user($user_id);
		foreach ($rejected as $reject) {
			$reject->odbor = Project::name_from_id($reject->project_id);
			$reject->ime = Activity::name_from_id($reject->activity_id);
			$reject->razlog = Explanation::get_explanation($reject->id);
			
			
		}
		echo json_encode($rejected);	
	}
	
	
	// Delete entry
	
	
	private function delete_entry($id, $user_id) {
		$oldentry = OldEntry::find_by_id($id);
		if($oldentry->user_id == $user_id) { 
			$oldentry->delete();
			echo "Success";
		} else {
		echo "Invalid request";
		}
	}
}


		


$model = new ApplyOld();



?>
