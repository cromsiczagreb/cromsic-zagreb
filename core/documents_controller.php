<?php

if(!($session->is_logged_in())) {
	$session->message("Morate se prijaviti da bi provjerili svoje potvrde.", "warning");	
	redirect_to("index.php");
}

class VerifiedPerUser extends Controller {
	
	
	// Table options
	
	public $columns						= array('kategorija', 'opis','bodovi');
	public $data_url 					= "core/documents_model.php";
	public $action						= "documents_verified";
	//public $create_new_link 			= "instance_list.php";
	public $modal						= false;
	public $action_icon					= "glyphicon-plus";
	
		
	function __construct() {
		echo "<div id=\"half_page_div_left\">";
		echo "<h3>" . _("Odobrene potvrde") . ": </h3>";
		$this->search = "false";
		$this->data_url = $this->data_url . "?action=verified";
		$this->create_table();
		echo "</div>";
	}
	
}

class UnVerifiedPerUser extends Controller {
	
	
	// Table options
	
	public $columns						= array('kategorija', 'opis');
	public $data_url 					= "core/documents_model.php";
	public $action						= "documents_verified";
	//public $create_new_link 			= "instance_list.php";
	public $modal						= false;
	public $action_icon					= "glyphicon-plus";
	
		
	function __construct() {
		echo "<div id=\"half_page_div_right\">";
		echo "<h3>" . _("Potvrde na čekanju") . ": </h3>";
		$this->search = "false";
		$this->data_url = $this->data_url . "?action=unverified";
		$this->create_table();
		echo "</div>";
	}
	
}


class HelpModal extends Form {
	
			function __construct($type, $title) {
			global $lang;
			$this->start_modal_large("help_modal", _("Upute"));
			require("./locale/{$lang}/HELP/documents_help.php");
			$this->end_modal_buttonless();
			}
}


class NewDocumentForm extends Form {

		
		public $kategorija;
		public $opis;
		public $user_id;
		private $filename;
		
		private $temp_path;
 		public $errors=array();
  
 		protected $upload_errors = array(
		// http://www.php.net/manual/en/features.file-upload.errors.php
	 		UPLOAD_ERR_OK 				=> "No error",
	   		UPLOAD_ERR_INI_SIZE  	=> "File size above allowed.",
	 		UPLOAD_ERR_FORM_SIZE 	=> "File size above allowed.",
	  		UPLOAD_ERR_PARTIAL 		=> "Upload incomplete.",
	  		UPLOAD_ERR_NO_FILE 		=> "File does not exist.",
	  		UPLOAD_ERR_NO_TMP_DIR => "No temporary directory.",
	  		UPLOAD_ERR_CANT_WRITE => "Can't write to disk.",
	  		UPLOAD_ERR_EXTENSION 	=> "File upload stopped by extension."
		);
		
		// Create form
		
		function __construct($type, $title) {
			if (isset($_POST['submit'])) {    
				$remove_submit = array_pop($_POST);
				$attributes = $_POST;
				$this->errors = array();
				foreach ($attributes as $key => $value) {
					$this->$key 		= $value;
					$this->$key = s($this->$key);
					$this->$key = h($this->$key);
				}
				$this->uploaded = $_FILES['file'];
				$this->individual_process();
				
			}
			
			$modal_id	= $type . "_modal";
			$button_id	= $type . "_button";
			$form_id	= $type . "_form";
			$form_type  = $type;
			$this->create_modal_form($modal_id, $title, $button_id, $form_id, $form_type);
		}
		
		// Individual process
		
		public function individual_process() {
				$this->validate_file($this->uploaded);
				$this->Validate();
				$this->show_errors($this->errors);
				if(empty($this->errors)) {
					 $this->prepare();
				
				}
		}
		
		// Validate file
		
		private function Validate_file($file) {
		$allowed_mime = array ("application/pdf", "image/gif", "image/png", "image/jpeg", "image/bmp");
		if(!$file || empty($file) || !is_array($file)) {
		  // error: nothing uploaded or wrong argument usage
		  $this->errors[] = _("Nije uploadan ni jedna datoteka.");
		  return false;
		} 
		if($file['error'] != 0) {
		  // error: report what PHP says went wrong
		  $this->errors[] = $this->upload_errors[$file['error']];
		  return false;
		} 
		if($file['size'] > 3500000) {
		  // Check size
		  $this->errors[] = _("Datoteka je iznad dopuštene veličine");
		  return false;
		} 
		if(!(in_array($file['type'] , $allowed_mime))) {
		  $this->errors[] = _("Datoteka nije odgovarajućeg tipa.");
		  return false;
		} 
			
	
		$this->temp_path = $file['tmp_name'];
		$this->filename = basename($this->temp_path, ".tmp");
		$path_parts = pathinfo($_FILES["file"]["name"]);
		$extension = $path_parts['extension'];
		$this->filename .= "." . $extension;
		}
		
		// Validate
		
		private function Validate() {
		
	
			
		}
		
		// Prepare
		
		private function prepare() {
			global $session;
			//Check if directory exists
			$this->user_id = $session->user_id;
			$dir_name= md5($session->user_id);
			$dir_path= "uploads/{$dir_name}";
			$target_path = $dir_path . "/" . $this->filename;
			echo $dir_path;
			if(!file_exists($dir_path))	{
				mkdir($dir_path);				
			}
			
			// Attempt to move the file 
			if(move_uploaded_file($this->temp_path, $target_path)) {
		  	// Success
				// Save a corresponding entry to the database
				 $this->finalize();
			} else {
				// File was not moved.
		    $this->errors[] = "The file upload failed, possibly due to incorrect permissions on the upload folder.";
		    return false;
			}
		
		
		}
		
		// Finalize
		
		private function finalize() {
			$document = new Document;
			foreach(Document::$db_fields as $key) {
				if(!empty($this->$key)) {
					$document->$key = $this->$key;
				} else {
					$document->$key = NULL;
				}
			}
			print_r($document);
			// Fix GPA
			if($document->kategorija == 1) {
				$document->opis = str_replace(',', '.', $document->opis);
				$document->opis = preg_replace("/[^0-9,.]/", "", $document->opis);
			}
			
			if($document->save()) {
				global $session;
				$session->message(_("Uspješno ste dodali datoteku, pričekajte njezinu potvrdu."), "success");
	 		 	redirect_to('documents.php');
			} else {
				$session->message(_("Greška na poslužitelju. Pokušajte kasnije."), "danger");
				redirect_to('index.php');
			}
			
		
			
		}
		
		// Create modal
		
		public function create_modal_form($modal_id, $modal_title, $button_id, $form_id, $form_type) {
			$this->start_modal($modal_id, $modal_title);
			$this->input_form($form_id, $form_type) ;
			$this->end_modal_buttonless($button_id);
		}

		// Input form
		
		public function input_form($form_id, $form_type) {
			$IDs = Doc_Category::find_all();
			foreach ($IDs as $ID) {
				$values[] = $ID->id;
			}
			$names_origin = "name_from_id";
			echo "<div id=\"modal_form\">";
			$this->start_form("documents.php", $form_id, "enctype=\"multipart/form-data\"");
			$this->add_element_hidden("type", $form_type);
			$this->add_element_hidden("MAX_FILE_SIZE", "3500000");
			$this->add_element_select('kategorija', _("Kategorija"), "", $values, "", $names_origin, "Doc_Category");
			$this->add_element("opis", _("Opis"), "text", $tooltip=false, "", _("Opis"), "opis")	;
			$this->add_element("file", _("Datoteka"), "file", false, "", "datoteka", "","accept=\".pdf,.gif,.png,.jpg,.bmp\"");
			// add file upload
			$this->end_form();
		}

}




?>