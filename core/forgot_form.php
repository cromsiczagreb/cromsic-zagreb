<?php

class ForgotForm extends Form {
	
		public $errors =array();
				
		public $username;
		public $hash;
		public $to_mail;
		
		
		// Individual process
		
		public function individual_process() {
				
				$this->validate();
				$this->show_errors($this->errors);
				
				if(empty($this->errors)) {
					
					 $this->prepare();
					 $this->finalize();
				}
		}
		
		// Validate
		
		private function Validate() {
		}
		
		// Prepare
		
		private function prepare() {
			
		}
		
		// Finalize
		
		private function finalize() {
			global $session;
			$user = User::find_by_jmbag($this->username);
			if($user) {
				if($this->db_request($user) && ($this->send_mail_password($user))) {
					$session->message(_("Mail za reset lozinke poslan na: ") . $this->to_mail  , "info");
					redirect_to("index.php");
					
				} else {
					$this->errors['error'] = _("Greška na poslužitelju. Pokušajte kasnije.");
					$this->show_errors($this->errors);
				}
			} else {
				$this->errors['error'] = _("Korisnik s ovim JMBAG-om ne postoji");
				$this->show_errors($this->errors);
			}
		}
		
		
		// Create database entry
		
		private function db_request($user) {
			global $db;			
			$id = $user['id'];
			$this->hash = htmlentities(password_hash($id, PASSWORD_DEFAULT));
			
			$sql = "INSERT INTO password_change (";
			$sql .= " user_id, hash ";
			$sql .= ") VALUES (";
			$sql .= " '{$id}', '{$this->hash}'";
			$sql .= ")";
			
			if ($db->query($sql)) { 
				return true ;
			} else {
				return false ;
			}
		}
		
		// Send mail password
		
		private function send_mail_password($user) {
			$this->to_mail = $user['mail'];
			$to_name = $user['ime'] . " " . $user['prezime'];
			$id = $user['id'];
			$create_link = $_SERVER['SERVER_NAME']."/reset_password.php?id=".$id."&hash=".$this->hash;
			
			$mail = new PHPMailer();
		
			// SMTP uključen
/*			$mail->IsSMTP();
			$mail->Host = MAIL_HOST;
			$mail->Port = MAIL_PORT;
			$mail->SMTPAuth = true;
			$mail->Username = MAIL_USERNAME;
			$mail->Password = MAIL_PASSWORD;
*/
			$mail->CharSet="UTF-8";
 			

			$mail->FromName = MAIL_FROM_NAME;
			$mail->From = MAIL_FROM;
			$mail->AddAddress($this->to_mail, $to_name);
			$mail->Subject = "Password reset";
			//$mail->WordWrap = 60;
			$mail->IsHTML(true);
			$mail->Body = "<p>" . _("Poštovani") . ",</p>
			<p>". _("zatražena je promjena vaše lozinke, istu možete promijeniti unutar 2 sata na linku") . ":</p>
			<p><a href=\"{$create_link}\">". _("Promijeni lozinku") ."</a></p><p>". _("Puni link") . ":{$create_link} </p>";


			if(!$mail->Send()) {
			echo 'Message was not sent.';
			echo 'Mailer error: ' . $mail->ErrorInfo;
			} else {
			return true;
			}

		$result = $mail->Send();
		return $result;
		}

		
				
		// Form creation
		
		public function create_form() {
			$this->start_form("forgot_password.php");
			$this->add_element("username", "JMBAG ", "text", false, $this->username, "JMBAG");
			$this->end_form();
		}
}

?>