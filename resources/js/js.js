function dateSorter(a, b) {
var a = moment(a, 'DD. MM. YYYY'); 
a = new Date(a);
var b = moment(b, 'DD. MM. YYYY'); 
b = new Date(b);
if (a < b) return 1;
if (a > b) return -1;
return 0;
}

function documents_verified(value, row, index) {
	   return [
  
		'<a class="view" href="core/view_document.php?id='+row.id+ '" target="_blank" id="'+ row.id + '"  title="Vidi">',
        '<i class="glyphicon glyphicon-file"></i>',
        '</a>',
		'&nbsp ',
		'<a class="delete" href="#document_delete" id="'+row.id+'" title="Obriši">',
        '<i class="glyphicon glyphicon-trash"></i>',
        '</a>',
    ].join('');
}

function news_list(value, row, index) {
	  return [
        '<a class="view" href="news_view.php?id='+row.id+'" title="Vidi">',
        '<i class="glyphicon glyphicon-eye-open"></i>',
        '</a>',
		'&nbsp ',
		'<a class="delete" href="#news_delete" id="'+row.id+'" title="Obriši">',
        '<i class="glyphicon glyphicon-trash"></i>',
        '</a>',
     
    ].join('');
}

function admin_verified(value, row, index) {
	   return [
  
		'<a class="view" href="controller/view_document.php?id='+row.id+ '&user_id='+row.user_id+ '" target="_blank" id="'+ row.id + '"  title="Vidi">',
        '<i class="glyphicon glyphicon-file"></i>',
        '</a>',
		'&nbsp ',
		'<a class="delete" href="#" id="'+row.id+'" data-toggle="modal" data-target="#delete_document_modal"title="Obriši">',
        '<i class="glyphicon glyphicon-trash"></i>',
        '</a>',
    ].join('');
}

function admin_unverified(value, row, index) {
	   return [
  
		'<a class="view" href="controller/view_document.php?id='+row.id+ '&user_id='+row.user_id+ '" target="_blank" id="'+ row.id + '"  title="Vidi">',
        '<i class="glyphicon glyphicon-file"></i>',
        '</a>',
		'&nbsp ',
		'<a class="edit" href="#" data-toggle="modal" id="'+ row.id + '"  data-target="#edit_modal" title="Izmijeni">',
        '<i class="glyphicon glyphicon-edit"></i>',
        '</a>',
		'&nbsp ',
		'&nbsp ',
		'&nbsp ',
		'<a class="delete" href="#admin_document_approve" id="'+row.id+'" title="Odobri">',
        '<i class="glyphicon glyphicon-ok"></i>',
        '</a>',
		'&nbsp ',
		'<a class="delete" href="#" id="'+row.id+'" data-toggle="modal" data-target="#delete_document_modal"title="Obriši">',
        '<i class="glyphicon glyphicon-remove"></i>',
        '</a>',
		
    ].join('');
}


function individual_edit(value, row, index) {
	    return [
        '<a class="edit" href="#swap" id="'+row.id+'" title="Izmijeni">',
        '<i class="glyphicon glyphicon-transfer"></i>',
        '</a>',
     
    ].join('');
}


function member_view(value, row, index) {
    return [
        '<a class="view" href="member_view.php?id='+row.id+'" title="Vidi">',
        '<i class="glyphicon glyphicon-eye-open"></i>',
        '</a>',
     
    ].join('');
}

function individual_view(value, row, index) {
    return [
        '<a class="view" href="individual_view.php?id='+row.id+'" title="Vidi">',
        '<i class="glyphicon glyphicon-eye-open"></i>',
        '</a>',
     
    ].join('');
}

function event_view(value, row, index) {
    return [
        '<a class="view" href="Dogadanje'+row.id+'" title="Vidi">',
        '<i class="glyphicon glyphicon-eye-open"></i>',
        '</a>',
	
     
    ].join('');
}


function instance_view(value, row, index) {
    return [
        '<a class="view" href="instance_view.php?id='+row.id+'" title="Vidi">',
        '<i class="glyphicon glyphicon-eye-open"></i>',
        '</a>',
		'&nbsp ',
		'<a class="view" href="instance_users.php?instance_id='+row.id+'" title="Sudionici">',
        '<i class="glyphicon glyphicon-user"></i>',
        '</a>',
     
    ].join('');
}

function signup_view(value, row, index) {
    return [
        '<a class="view" href="signup_view.php?id='+row.id+'" title="Vidi">',
        '<i class="glyphicon glyphicon-eye-open"></i>',
        '</a>',
		'&nbsp ',
		'<a class="view" href="signup_users.php?signup_id='+row.id+'" title="Prijavljeni">',
        '<i class="glyphicon glyphicon-user"></i>',
        '</a>',
		'&nbsp ',
		'<a class="view" href="signup_instance.php?signup_id='+row.id+'" onclick="return confirm(\'Sigurno želite pretvoriti u događaj?\')" title="Unesi kao događaj">',
        '<i class="glyphicon glyphicon-share"></i>',
        '</a>',
		
     
    ].join('');
}



function doc_category_view(value, row, index) {
    return [
  
		'<a class="edit" href="#" data-toggle="modal" id="'+ row.id + '"  data-target="#edit_modal" title="Izmijeni">',
        '<i class="glyphicon glyphicon-edit"></i>',
        '</a>',
		'&nbsp',
		'<a class="delete" href="model/delete.php?class=doc_category&id='+row.id+'" onclick="return confirm(\'Sigurno želite obrisati ovaj unos?\')" title="Obriši">',
        '<i class="glyphicon glyphicon-trash"></i>',
        '</a>',
     
     
    ].join('');
}

function project_view(value, row, index) {
    return [
  
		'<a class="edit" href="#" data-toggle="modal" id="'+ row.id + '"  data-target="#edit_modal" title="Izmijeni">',
        '<i class="glyphicon glyphicon-edit"></i>',
        '</a>',
		'&nbsp',
		'<a class="delete" href="model/delete.php?class=project&id='+row.id+'" onclick="return confirm(\'Sigurno želite obrisati ovaj unos?\')" title="Obriši">',
        '<i class="glyphicon glyphicon-trash"></i>',
        '</a>',
     
     
    ].join('');
}

function activity_view(value, row, index) {
    return [
	//'<a class="edit" href="project_edit.php?id='+row.id+'" title="Edit">',
  
		'<a class="edit" href="#" data-toggle="modal" id="'+ row.id + '"  data-target="#edit_modal" title="Izmijeni">',
        '<i class="glyphicon glyphicon-edit"></i>',
        '</a>',
		'&nbsp',
		'<a class="delete" href="model/delete.php?class=activity&id='+row.id+'" onclick="return confirm(\'Sigurno želite obrisati ovaj unos?\')" title="Obriši">',
        '<i class="glyphicon glyphicon-trash"></i>',
        '</a>',
     
     
    ].join('');
}
    
$('#editor').on("keyup keypress", function(e) {
  var code = e.keyCode || e.which; 
  if (code  == 13) {               
    e.preventDefault();
    return false;
  }
});




function add_datepicker(id) {
          	$(id).datetimepicker({
			viewDate: moment("1993 01 01", "YYYY MM DD"),
			viewMode:'years',
			locale:"hr",
			format:'L'
			});
}

function add_datepicker_now(id) {
            $(id).datetimepicker({
			locale:"hr",
			format:'L'
			});
}

function add_datetimepicker(id) {
            $(id).datetimepicker({
			locale:"hr",
			});
}



function add_listener(page) {
  
  $( "#create_button" ).click(function() {
	$.post( "model/"+page+".php", $( "#create_form" ).serialize());
	$('#create_modal').modal('hide');
	$('.table').bootstrapTable('removeAll');
    $('.table').bootstrapTable('refresh');
	});  
	
  $( "#edit_button" ).click(function() {
	$.post( "model/"+page+".php", $( "#edit_form" ).serialize() + "&id="+$id);
	$('#edit_modal').modal('hide');
	$('.table').bootstrapTable('removeAll');
	$('.table').bootstrapTable('refresh');
	}); 
	
	if(page=="document") {
	  
	$('#edit_modal').on('show.bs.modal', function (e) {
		var $invoker = e.relatedTarget;
		var $row = $($invoker).parent();
		var $bodovi;
		var $kategorija = ($($row).closest('td').siblings('td ').eq(0).text());
		$("select option").filter(function() {
  			  return $(this).text() == $kategorija; 
				}).prop('selected', true);
		$id = $invoker.id;
		$opis = ($($row).closest('td').siblings('td ').eq(1).text());
	 $('#edit_modal').find('#opis').val($opis);
	})
	
	
		
	$('#delete_document_modal').on('show.bs.modal', function (e) {
		var $invoker = e.relatedTarget;
		var $row = $($invoker).parent();
		$id = $invoker.id;
	 $('#delete_document_modal').find('input[name=id]').val($id);
	})
	
    }
  
  if(page=="doc_category") {
	  
	$('#edit_modal').on('show.bs.modal', function (e) {
		var $invoker = e.relatedTarget;
		var $row = $($invoker).parent();
		$id = $invoker.id;
		$ime = ($($row).closest('td').siblings('td ').eq(0).text());
		$bodovi = ($($row).closest('td').siblings('td ').eq(1).text());
 	 $('#edit_modal').find('#ime').val($ime);
	 $('#edit_modal').find('#bodovi').val($bodovi);
	})
    }
  
  if(page=="project") {
	  
	$('#edit_modal').on('show.bs.modal', function (e) {
		var $invoker = e.relatedTarget;
		var $row = $($invoker).parent();
		$id = $invoker.id;
		$ime = ($($row).closest('td').siblings('td ').eq(0).text());
		$opis = ($($row).closest('td').siblings('td ').eq(1).text());
 	 $('#edit_modal').find('#ime').val($ime);
	 $('#edit_modal').find('#opis').val($opis);
	})
    }
	
	if(page=="activity") {
	  
	$('#edit_modal').on('show.bs.modal', function (e) {
		var $invoker = e.relatedTarget;
		var $row = $($invoker).parent();
		var $bodovi;
		var $aktivnost = ($($row).closest('td').siblings('td ').eq(1).text());
		$("select option").filter(function() {
    
  			  return $(this).text() == $aktivnost; 
				}).prop('selected', true);
		$id = $invoker.id;
		$ime = ($($row).closest('td').siblings('td ').eq(0).text());
		$bodovi = ($($row).closest('td').siblings('td ').eq(3).text());
		$opis = ($($row).closest('td').siblings('td ').eq(2).text());
		
 	 $('#edit_modal').find('#ime').val($ime);
	 $('#edit_modal').find('#opis').val($opis);
	 $('#edit_modal').find('#bodovi').val($bodovi);
	})
    }
	
	if(page=="instance_create") {
	 $( "[name=project_id]" ).change(function() {
		 project_id = $(this).val();
		$('[name=activity_id]').attr('disabled','disabled');
  		$('[name=activity_id]')
    		.find('option')
    		.remove()
    		.end()
			
	$.getJSON("model/instance_create.php",{id: $(this).val(), action: 'get_activities'}, function(j){
      var options = '<option value="0"></option>';
      for (var i = 0; i < j.length; i++) {
        options += '<option value="' + j[i].id + '">' + j[i].ime + '</option>';
      }
	 
     $('[name=activity_id]').html(options);
    })
	$('[name=activity_id]').removeAttr("disabled");
		});  
		
	$( "[name=activity_id]" ).change(function() {
		 activity_id = $(this).val();
		$('[name=activity_id]').attr('disabled','disabled');
	$.getJSON("model/instance_create.php",{id: $(this).val(), action: 'activity'}, function(j){
      $('[name=activity_id]').removeAttr("disabled");
	 $('[name=datum]').removeAttr("disabled");
     $('[name=ime]').removeAttr("disabled").val(j.ime);
	 $('[name=bodovi]').removeAttr("disabled").val(j.bodovi);
	 $('[name=opis]').removeAttr("disabled").val(j.opis);
    })
		});  
		
		
    }
	
	if(page=="signup_create") {
	 $( "[name=project_id]" ).change(function() {
		 project_id = $(this).val();
		$('[name=activity_id]').attr('disabled','disabled');
  		$('[name=activity_id]')
    		.find('option')
    		.remove()
    		.end()
			
	$.getJSON("model/signup_create.php",{id: $(this).val(), action: 'get_activities'}, function(j){
      var options = '<option value="0"></option>';
      for (var i = 0; i < j.length; i++) {
        options += '<option value="' + j[i].id + '">' + j[i].ime + '</option>';
      }
	 
     $('[name=activity_id]').html(options);
    })
	$('[name=activity_id]').removeAttr("disabled");
		});  
		
	$( "[name=activity_id]" ).change(function() {
		 activity_id = $(this).val();
		$('[name=activity_id]').attr('disabled','disabled');
	$.getJSON("model/instance_create.php",{id: $(this).val(), action: 'activity'}, function(j){
      $('[name=activity_id]').removeAttr("disabled");
	 $('[name=datum]').removeAttr("disabled");
     $('[name=ime]').removeAttr("disabled").val(j.ime);
	 $('[name=potrebno]').removeAttr("disabled");
	 $('[name=zamjena]').removeAttr("disabled");
	 $('[name=opis]').removeAttr("disabled").val(j.opis);
    })
		});  
		
		
    }
	
	
// UPDATE ON 19. 10, if it doesn't work it's here


	if(page=="apply_old") {
		console.log("hello from add_listener");
	 $( "#create_button").unbind( "click" );
	 $( "#create_button" ).click(function() {
	$.post( "core/apply_old_create.php", $( "#create_form" ).serialize());
	$('.table').bootstrapTable('removeAll');
	$('#create_modal').modal('hide');
	$('.table').bootstrapTable('refresh');
	});  
	
	
	 $( "[name=project_id]" ).change(function() {
		 console.log("hello from onchange");
		 project_id = $(this).val();
		$('[name=activity_id]').attr('disabled','disabled');
  		$('[name=activity_id]')
    		.find('option')
    		.remove()
    		.end();
			
	$.getJSON("core/apply_old.php",{id: $(this).val(), action: 'get_activities'}, function(j){
      var options = '<option value="0"></option>';
      for (var i = 0; i < j.length; i++) {
        options += '<option value="' + j[i].id + '">' + j[i].ime + '</option>';
      }
	 
     $('[name=activity_id]').html(options);
    })
	$('[name=activity_id]').removeAttr("disabled");
		});  
		
	$( "[name=activity_id]" ).change(function() {
		 activity_id = $(this).val();
		$('[name=activity_id]').attr('disabled','disabled');
	$.getJSON("core/apply_old.php",{id: $(this).val(), action: 'activity'}, function(j){
      $('[name=activity_id]').removeAttr("disabled");
	 $('[name=opis]').removeAttr("disabled").val(j.opis);
    })
	 $('[name=activity_id]').removeAttr("disabled");
		}); 
		
			
			$('.table').bootstrapTable('refresh');
			
			
    }


	if(page=="entries") {
	
	$('#delete_entries_modal').on('show.bs.modal', function (e) {
		var $invoker = e.relatedTarget;
		var $row = $($invoker).parent();
		$id = $invoker.id;
	 $('#delete_entries_modal').find('input[name=id]').val($id);
	})
		
	}
	
	if(page=="entries_admin") {
	
	$('#delete_entries_modal').on('show.bs.modal', function (e) {
		var $invoker = e.relatedTarget;
		var $row = $($invoker).parent();
		$id = $invoker.id;
	 $('#delete_entries_modal').find('input[name=id]').val($id);
	})
		
	}



// END OF UPDATE 19. 10.
	



}

$(document).on( "click", "a[href*=#deselect]", function( event ) {
		var selections = $('.table:eq(0)').bootstrapTable('getSelections');
		var $_GET = get_GET();		
		instance_id = $_GET["instance_id"];
		var IDs = "";	
		selections.forEach(function(entry) {
			IDs += entry.id +", ";
					})
		console.log(IDs);
		$.get( "model/participants.php", { action: "remove", instance_id: instance_id, ids: IDs  } );
		var table = $('.table:eq(0)');
		table.bootstrapTable('refresh');
		});
		
$(document).on( "click", "a[href*=#design]", function( event ) {
		var selections = $('.table:eq(0)').bootstrapTable('getSelections');
		var $_GET = get_GET();		
		signup_id = $_GET["signup_id"];
		var IDs = "";	
		selections.forEach(function(entry) {
			IDs += entry.id +", ";
					})
		console.log(IDs);
		$.get( "model/signup_users.php", { action: "remove", signup_id: signup_id, ids: IDs  } );
		var table = $('.table:eq(0)');
		table.bootstrapTable('refresh');
		});
		
$(document).on( "click", "a[href*=#document_delete]", function( event ) {
		var $invoker = $(this);
		var $row = $($invoker).parent();
		$id = $(this).attr("id");
		console.log($id);
		var r = confirm("Želite li sigurno obrisati ovu potvrdu?");
		if (r == true) {
  		 	$.get( "core/documents_model.php", { action: "delete", id: $id} );
			var table = $('.table:eq(0)');
			table.bootstrapTable('refresh');
			var table_two = $('.table:eq(1)');
			table_two.bootstrapTable('refresh');
		}
	
		});
		
$(document).on( "click", "#delete_document_button", function( event ) {
		
		$razlog = $("#razlog").val();
		console.log($id);
		
  		 	$.get( "model/document.php", { action: "delete", id: $id, razlog: $razlog} );
			var table = $('.table:eq(0)');
			table.bootstrapTable('refresh');
			var table_two = $('.table:eq(1)');
			table_two.bootstrapTable('refresh');
			$('#delete_document_modal').modal('hide')
		});
		
$(document).on( "click", "a[href*=#news_delete]", function( event ) {
		var $invoker = $(this);
		var $row = $($invoker).parent();
		$id = $(this).attr("id");
		console.log($id);
		var r = confirm("Želite li sigurno obrisati ovu vijest?");
		if (r == true) {
  		 	$.get( "model/news.php", { action: "delete", id: $id} );
			var table = $('.table:eq(0)');
			table.bootstrapTable('refresh');
		}
		});
		
$(document).on( "click", "a[href*=#admin_document_approve]", function( event ) {
		var $invoker = $(this);
		var $row = $($invoker).parent();
		$id = $(this).attr("id");
		console.log($id);
		var r = confirm("Želite li sigurno odobriti ovu potvrdu?");
		if (r == true) {
  		 	$.get( "model/document.php", { action: "approve", id: $id} );
			var table = $('.table:eq(0)');
			table.bootstrapTable('refresh');
		}
	
		});

		
		
$(document).on( "click", "a[href*=#swap]", function( event ) {
		console.log("click");
		var $invoker = $(this);
		var $row = $($invoker).parent();
		$id = $(this).attr("id");
		$ime = ($($row).closest('td').siblings('td ').eq(1).text());
		$prezime = ($($row).closest('td').siblings('td ').eq(0).text());
		$full_name = $prezime + " " + $ime;
 	 $('#user_id').val($id);
	 $('#member').val($full_name);
	 $('[name=član]').attr('disabled','disabled');
		});
		
		

function add_participants() {
	
		var selections = $('table:eq(4)').bootstrapTable('getSelections');
		var $_GET = get_GET();		
		instance_id = $_GET["instance_id"];
		var IDs = "";	
		console.log(selections);
		selections.forEach(function(entry) {
			IDs += entry.id +", ";
					})
		$.get( "model/participants.php", { action: "add", instance_id: instance_id, ids: IDs  } );
		var table = $('table:eq(3)');
		table.bootstrapTable('uncheckAll');
		var table = $('.table:eq(0)');
		table.bootstrapTable('refresh');
}

function get_GET() {
	var $_GET = {};

	document.location.search.replace(/\??(?:([^=]+)=([^&]*)&?)/g, function () {
    function decode(s) {
        return decodeURIComponent(s.split("+").join(" "));
    }

    $_GET[decode(arguments[1])] = decode(arguments[2]);
	});	
	return $_GET;
	}
	
// MEMBER EXPORT PAGE

$(document).on( "click", "#refresh_last", function( event ) {
	$('#text_last').text(moment().format('DD. MM. YYYY. HH:mm:ss'));
	$.get( "model/member_export.php", "action=refresh");
		});
		
$(document).on( "focusout", "input[name='datum_export']", function( event ) {
	console.log("Changed");
	$('.table').bootstrapTable('refresh', {query: {action: 'new', date: $("input[name='datum_export']").val()}});
		});


// UPDATE 19. 10.
// copy from unstable

function admin_verified_entries(value, row, index) {
	   return [
  
			'<a class="delete" href="#" id="'+row.id+'" data-toggle="modal" data-target="#delete_entries_modal"title="Obriši">',
        '<i class="glyphicon glyphicon-trash"></i>',
        '</a>',
    ].join('');
}


function admin_waiting_entries(value, row, index) {
	   return [
	   
	   	'<a class="delete" href="#admin_entry_approve" id="'+row.id+'" title="Odobri">',
        '<i class="glyphicon glyphicon-ok"></i>',
        '</a>',
		'&nbsp ',
  
		'<a class="delete" href="#" id="'+row.id+'" data-toggle="modal" data-target="#delete_entries_modal"title="Obriši">',
        '<i class="glyphicon glyphicon-trash"></i>',
        '</a>',
    ].join('');
}


function admin_rejected_entries(value, row, index) {
	   return [
  
		'<a class="delete" href="#admin_entry_delete" id="'+row.id+'" title="Obriši">',
        '<i class="glyphicon glyphicon-trash"></i>',
        '</a>',
		
    ].join('');
}

function apply_old_edit(value, row, index) {
	    return [
        '<a class="delete" href="#entry_delete" id="'+row.id+'" title="Obriši">',
        '<i class="glyphicon glyphicon-trash"></i>',
        '</a>',
     
    ].join('');
}


 $(document).on( "click", "a[href*=#entry_delete]", function( event ) {
		var $invoker = $(this);
		var $row = $($invoker).parent();
		$id = $(this).attr("id");
		console.log($id);
		var r = confirm("Želite li sigurno obrisati ovu stavku?");
		if (r == true) {
  		 	$.get( "core/apply_old.php", { action: "delete", id: $id} );
			var table = $('.table:eq(0)');
			$('.table:eq(0)').bootstrapTable('refresh');
			var table_two = $('.table:eq(1)');
			$('.table:eq(1)').bootstrapTable('refresh');
		}
	
		});
		
 $(document).on( "click", "a[href*=#admin_entry_delete]", function( event ) {
		var $invoker = $(this);
		var $row = $($invoker).parent();
		$id = $(this).attr("id");
		console.log($id);
		var r = confirm("Želite li sigurno obrisati ovu stavku?");
		if (r == true) {
  		 	$.get( "model/entries.php", { action: "delete", id: $id} );
			var table = $('.table:eq(0)');
			table.bootstrapTable('refresh');

		}
	
	   });
	   
	
$(document).on( "click", "a[href*=#admin_entry_approve]", function( event ) {
		var $invoker = $(this);
		var $row = $($invoker).parent();
		$id = $(this).attr("id");
		console.log($id);
		var r = confirm("Želite li sigurno odobriti ovu aktivnost?");
		if (r == true) {
  		 	$.get( "model/entries.php", { action: "approve", id: $id} );
			var table = $('.table:eq(0)');
			table.bootstrapTable('refresh');
		}
	
	
	   });


$(document).on( "click", "#delete_entries_button", function( event ) {
		
		$razlog = $("#razlog").val();
		console.log('test');
		
  		 	$.get( "model/entries.php", { action: "reject", id: $id, razlog: $razlog} );
			var table = $('.table:eq(0)');
			table.bootstrapTable('refresh');
			var table_two = $('.table:eq(1)');
			table_two.bootstrapTable('refresh');
			$('#delete_entries_modal').modal('hide')
		});
		
// END UPDATE 19. 10.

// UPDATE 24. 10. 

function admin_afs(value, row, index) {
    return [
        '<a class="view" href="af_view.php?id='+row.id+'" title="Vidi">',
        '<i class="glyphicon glyphicon-eye-open"></i>',
        '</a>',
     
    ].join('');
}

// END LAST UPDATE



// UPDATE 14. 11.

function admin_complaints(value, row, index) {
    return [
        '<a class="view" href="complaint_view.php?id='+row.id+'" title="Vidi">',
        '<i class="glyphicon glyphicon-eye-open"></i>',
        '</a>',
     
    ].join('');
}



// END LAST UPDATE