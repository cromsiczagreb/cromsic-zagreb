<?php require("../../includes/initialize.php");?>
<?php require("../../includes/model.php") ; ?>


<?php 

class AfListModel extends Model {
	
	private $call_class = "applications";
	public $requested	= array('id', 'target_ime', 'bodovi_opci', 'bodovi_cromsic', 'postotak', 'potroseno','bodovi');
	
	
	public function individual_process() {
		global $session;
		if(!isset($_GET) or !$session->is_admin()) {
			echo "Invalid request";
			redirect_to("../af_list.php");
		} elseif(isset($_POST['type'])) {
			//$this->save_document();
		} else {	
			switch($_GET['action']) {
				case "get_scope": $this->get_list(1); break;
				case "get_score": $this->get_list(2); break;
				case "get_scope_archive": $this->get_archive(1); break;
				case "get_score_archive": $this->get_archive(2); break;
				default: echo "Invalid request";
			}
		}
		
	}
	
	
	
	// Get list
	
	private function get_list($type) {
		
		$applications = Application::find_by_type_current($type);
		
		
		foreach ($applications as $application) {
			
			$application->target_ime = User::name_from_id($application->user_id);
			$application->bodovi_opci = User::count_from_general($application->user_id);
			$application->bodovi_cromsic = User::count_from_activities($application->user_id);
			$application->bodovi = $application->bodovi_opci + $application->bodovi_cromsic;
			$application->postotak = round((($application->bodovi_opci / $application->bodovi) * 100));
			if ($application->postotak > 50) {
				$application->bodovi = 2 * $application->bodovi_cromsic;
			}
			$application->potroseno = "-";
			if( $application_check =  Application::get_last_exchange($application->user_id)) {
				 $application->potroseno =  User::deduct_points($application->user_id, $application_check->datum);
			}
			$application->bodovi = $application->bodovi - $application->potroseno;
			$application->target_ime = "<a style=\"color:black\" href=\"member_view.php?id={$application->user_id}\">{$application->target_ime}</a>";
			
					
			
		}
		$this->create_JSON($applications);
	}
	
	
	// Get archive
	
	private function get_archive($type) {
		$this->requested	= array('id', 'target_ime', 'datum');
		$applications = Application::find_by_type($type);
		
		
		foreach ($applications as $application) {
			
			$application->target_ime = User::name_from_id($application->user_id);
			$application->target_ime = "<a style=\"color:black\" href=\"member_view.php?id={$application->user_id}\">{$application->target_ime}</a>";
			$application->datum = sql_to_hr_datetime($application->datum);
			
					
			
		}
		$this->create_JSON($applications);
	}
	

	
}



$model = new AfListModel();



?>
