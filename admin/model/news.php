<?php require("../../includes/initialize.php");?>
<?php require("../../includes/model.php") ; ?>


<?php 

class NewsModel extends Model {
	
	private $call_class = "news";
	public $requested	= array('id', 'naslov', 'podnaslov');
	
	
	public function individual_process() {
		global $session;
		if(!isset($_GET) or !$session->is_admin()) {
			echo "Invalid request";
			redirect_to("../news_list.php");
		} else {	
			switch($_GET['action']) {
				case "get_news_list": $this->get_news_list(); break;
				case "delete": $this->delete_news($_GET['id']); break;
				default: echo "Invalid request";
			}
		}
		
	}
	
	
	// News list
	
	private function get_news_list() {
		$class = $this->call_class;
		$news = $class::find_all();
		$this->create_JSON($news);
	}
		
	// Delete news
	
	private function delete_news($id) {
		global $session;
		
		$news = News::find_by_id($id);
		$file_name = $news->filename;
		if($session->is_admin()) { 
			$news->delete();
			$file = "../../uploads/carousel/" . $file_name;
			unlink($file);
		} else {
		echo "Invalid request";
		}
	}
	
	
}



$model = new NewsModel();



?>
