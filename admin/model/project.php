<?php require("../../includes/initialize.php");?>
<?php require("../../includes/model.php") ; ?>


<?php 

class ProjectModel extends Model {
	
	private $call_class = "Project";
	public $requested = array('ime', 'opis');
	
	
	public function individual_process() {
		$this->create_project();
	}
	
	
	private function create_project() {
		
			//$remove_submit = array_pop($_POST);
			$attributes = $_POST;
			$this->errors = array();
			foreach ($attributes as $key => $value) {
				$this->$key 		= $value;
			}
			
			$project = new Project;
			foreach(Project::$db_fields as $key) {
				if(!empty($this->$key)) {
					$project->$key = $this->$key;
				} else {
					$project->$key = NULL;
				}
			}
			
			if($project->save()) {
				echo "Success"; 
			} else {
				echo "Failed";
			}
			
			
	}
	
}


$model = new ProjectModel();



?>
