<?php require("../../includes/initialize.php");?>
<?php require("../../includes/model.php") ; ?>


<?php 

class MemberPointsModel extends Model {
	
	private $call_class = "User";
	public $requested = array('id', 'jmbag', 'prezime', 'ime', 'mail', 'mobitel', 'godina_sad', 'bodovi');
	
	public function individual_process() {
		$class = $this->call_class;
		$users = $class::find_all_refreshed();
		$this->add_points($users);
	
	}
	
	
	
	
	// Add points
	
	public function add_points($users) {
		foreach ($users as $user) {
			$user->bodovi = User::count_points($user->id);
		}
		$this->create_JSON($users);
	}
	
	
	
}



$model = new MemberPointsModel();



?>
