<?php require("../../includes/initialize.php");?>
<?php require("../../includes/model.php") ; ?>


<?php 

class InstanceCreate extends Model {
	
	
	public $requested = array  ("id", "ime", "bodovi", "opis");
	
	public function individual_process() {
		if(!isset($_GET)) { echo "Invalid request." ; }
		if($_GET['action'] == "get_activities") { $this->get_activities($_GET['id']); }
		if($_GET['action'] == "activity") { $this->activity_info($_GET['id']); }
	}
	
	
	private function get_activities($id) {
		$activities = Activity::get_activities($id);
		$this->create_JSON($activities);
	}
	
	private function activity_info($id) {
		$activity = Activity::find_by_id($id);
		echo json_encode($activity);	}
	
}


		


$model = new InstanceCreate();



?>
