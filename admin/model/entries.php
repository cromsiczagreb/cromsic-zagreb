<?php require("../../includes/initialize.php");?>
<?php require("../../includes/model.php") ; ?>


<?php 

class EntriesModel extends Model {
	
	private $call_class = "old_entries";
	public $requested	= array('id', '#', 'user_id', 'project_id', 'opis', 'activity_id',  'status');
	
	
	public function individual_process() {
		global $session;
		if(!isset($_GET) or !$session->is_admin()) {
			echo "Invalid request";
			redirect_to("../documents.php");
		} elseif(isset($_POST['type'])) {
			//$this->save_document();
		} else {	
			switch($_GET['action']) {
				case "get_verified": $this->get_verified(); break;
				case "get_waiting_for_project": $this->get_waiting_for_project($_GET['project_id']); break;
				case "entries_user": $this->get_approved_for_user($_GET['user_id']); break;
				case "get_rejected": $this->get_rejected(); break;
				case "user": $this->verified_for_user($_GET['user_id']) ; break;
				case "approve": $this->approve_entry($_GET['id']); break;
				case "delete": $this->delete_entry($_GET['id']); break;
				case "reject": $this->reject_entry($_GET['id'],$_GET['razlog']); break;
				default: echo "Invalid request";
			}
		}
		
	}
	
	
	
	// Get all approved
	
	private function get_verified() {
		$this->requested = array('id', 'broj','odbor', 'opis', 'bodovi', 'ime', 'target_ime', 'approver');
		$verified = OldEntry::verified_for_all();
		foreach ($verified as $entry) {
			$entry->broj = $entry->id;
			$entry->odbor = Project::name_from_id($entry->project_id);
			$entry->ime = Activity::name_from_id($entry->activity_id);
			$entry->bodovi = Activity::points_from_id($entry->activity_id);
			$entry->target_ime = User::name_from_id($entry->user_id);
			$entry->approver = User::name_from_id($entry->officer);
		}
		$this->create_JSON($verified);
	}
	
	// Get all waiting
	
	
	// Get waiting for project
	
	private function get_waiting_for_project($project_id) {
		$this->requested = array('id', 'broj', 'odbor', 'opis', 'bodovi', 'ime', 'target_ime');
		$waiting = OldEntry::waiting_for_project($project_id);
		foreach ($waiting as $entry) {
			$entry->broj = $entry->id;
			$entry->odbor = Project::name_from_id($entry->project_id);
			$entry->ime = Activity::name_from_id($entry->activity_id);
			$entry->bodovi = Activity::points_from_id($entry->activity_id);
			$entry->target_ime = User::name_from_id($entry->user_id);
		}
		$this->create_JSON($waiting);
	}
	
	// Get approved for user
	
	private function get_approved_for_user($user_id) {
		$this->requested = array('id', 'odbor', 'opis', 'bodovi', 'ime', 'target_ime');
		$waiting = OldEntry::get_old_entries_verified_for_user($user_id);
		foreach ($waiting as $entry) {
			$entry->odbor = Project::name_from_id($entry->project_id);
			$entry->ime = Activity::name_from_id($entry->activity_id);
			$entry->bodovi = Activity::points_from_id($entry->activity_id);
			$entry->target_ime = User::name_from_id($entry->user_id);
		}
		$this->create_JSON($waiting);
	}
	
	// Get all rejected
	
	private function get_rejected() {
		$this->requested = array  ("id", 'broj',"ime",  'target_ime', "razlog","opis", 'rejector' );
		$rejected = OldEntry::rejected_for_all();
		foreach ($rejected as $reject) {
			$reject->broj = $reject->id;
			$reject->odbor = Project::name_from_id($reject->project_id);
			$reject->ime = Activity::name_from_id($reject->activity_id);
			$reject->target_ime = User::name_from_id($reject->user_id);
			$reject->razlog = Explanation::get_explanation($reject->id);
			$reject->rejector = User::name_from_id($reject->officer);
			
			
		}
		echo json_encode($rejected);	
	}
	
	// Approve entry
	
	private function approve_entry($id) {
		
			global $session;
			$entry = OldEntry::approve($id);
			
		
	
	}
	
	// Reject entry
	
	private function reject_entry($id, $razlog) {
		global $session;
		
		$entry = OldEntry::find_by_id($id);
		$entry->status = 2;
		$entry->officer = $session->user_id;
		$entry->save();
		
		$explanation = new Explanation;
		$explanation->razlog = $razlog;
		$explanation->entry_id = $entry->id;
		$explanation->save();
		
	}
	
	// Delete entry
	
	private function delete_entry($id) {
		global $session;
		
		$entry = OldEntry::find_by_id($id);
		$explanation = Explanation::find_by_entry_id($entry->id);
		$explanation->delete();
		$entry->delete();
		
		

		
		
	}
	
	
}



$model = new EntriesModel();



?>
