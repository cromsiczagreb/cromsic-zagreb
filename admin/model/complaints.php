<?php require("../../includes/initialize.php");?>
<?php require("../../includes/model.php") ; ?>


<?php 

class ComplaintsModel extends Model {
	
	private $call_class = "complaints";
	public $requested	= array('id', 'ime' );
	
	
	public function individual_process() {
		global $session;
		if(!isset($_GET) or !$session->is_admin()) {
			echo "Invalid request";
			redirect_to("../documents.php");
		} elseif(isset($_POST['type'])) {
			//$this->save_document();
		} else {	
			switch($_GET['action']) {
				case "get_working": $this->get_working(); break;
				case "get_waiting": $this->get_waiting(); break;
				case "approve": $this->approve_complaint($_GET['id']); break;
				
				default: echo "Invalid request";
			}
		}
		
	}
	
	
	
	// Get working
	
	private function get_working() {
		$this->requested = array('id', 'ime' );
		$working = Complaint::get_working();
		foreach ($working as $complaint) {
			$complaint->ime = User::name_from_id($complaint->user_id);
			
		}
		$this->create_JSON($working);
	}
	
	// Get working
	
	private function get_waiting() {
		$this->requested = array('id', 'ime' );
		$working = Complaint::get_waiting();
		foreach ($working as $complaint) {
			$complaint->ime = User::name_from_id($complaint->user_id);
		}
		$this->create_JSON($working);
	}
	
	
	
	
	
	// Approve entry
	
	private function approve_entry($id) {
		
			global $session;
			Complaint::approve($id);
			
		
	
	}
	
	
	
	// Delete entry
	
	private function delete_entry($id) {
		global $session;
		
		$entry = Complaint::find_by_id($id);
		$entry->delete();
		
		

		
		
	}
	
	
}



$model = new ComplaintsModel();



?>
