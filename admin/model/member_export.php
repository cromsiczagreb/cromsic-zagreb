<?php require("../../includes/initialize.php");?>
<?php require("../../includes/model.php") ; ?>
<?php require("../../includes/last_member_export.php") ; ?>


<?php 

class MemberExportModel extends Model {
	
	private $call_class = "User";
	public $requested = array('id', 'registered','prezime', 'ime', 'mail','mobitel', 'jmbag', 'mjesto', 'datum_rod', 'studij', 'godina_sad', 'godina_upis',
							 'trajanje', 'vrsta');
	
	public function individual_process() {
		global $session;
		if(!$session->is_admin()) {
			echo "Invalid request";
		} elseif(!isset($_GET['action'])) {
			$this->first_call();
		} else {	
			switch($_GET['action']) {
				case "refresh": LastMemberExport::refresh_export(); break;
				case "new": $this->new_call($_GET['date']); break;
				default: echo "Invalid request";
			}
		}		
	}
	
	private function first_call() {
		$datum = LastMemberExport::get_last_export();
		$users = User::from_date($datum);
		foreach ($users as $user) {
			$user->registered = hr_to_export_datetime($user->registered);
			$user->mjesto = "Zagreb";
			$user->trajanje = "6";
			$user->vrsta = "Redovno";
			$user->datum_rod = hr_to_export($user->datum_rod);
		}
		$this->create_JSON($users);
	}
	
	
	
	
	
	private function new_call() {
		$datum = hr_to_sql($_GET['date']);
		$users = User::from_date($datum);
		foreach ($users as $user) {
			$user->registered = hr_to_export_datetime($user->registered);
			$user->mjesto = "Zagreb";
			$user->trajanje = "6";
			$user->vrsta = "Redovno";
			$user->datum_rod = hr_to_export($user->datum_rod);
		}
		$this->create_JSON($users);
	}
	
}


$model = new MemberExportModel();

?>
