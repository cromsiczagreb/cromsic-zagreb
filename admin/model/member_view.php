<?php require("../../includes/initialize.php");?>
<?php require("../../includes/model.php") ; ?>


<?php 

class InstanceListModel extends Model {
	
	private $call_class = "instance";
	public $requested	= array('id', 'ime', 'bodovi', 'opis',  'datum');
	
	
	public function individual_process() {
		if(!isset($_GET)) {
			echo "Invalid request";
		} else {	
			switch($_GET['action']) {
				case "participations": $this->get_participations($_GET['user_id']); break;
				case "individual": $this->get_individual($_GET['user_id']); break;
				default: echo "Invalid request";
			}
		}
		
	}
	
	// Get participations
	
	private function get_participations($user_id) {
		$membership = new Instance;
		$membership->id = 0;
		$membership->ime = "Članstvo u CroMSIC-u";
		$membership->bodovi = 30;
		$membership->opis = "Bodovi za članstvo";
		$membership->datum = "";
		$instances = Instance::for_user($user_id);
		array_unshift($instances , $membership);
		$this->create_JSON($instances);
	}
	
	// Get individual
	
	private function get_individual($user_id) {
		$individual = Individual::for_user($user_id);
		$this->requested = array('id', 'opis', 'datum', 'bodovi');
		$this->create_JSON($individual);
	}
	
}



$model = new InstanceListModel();



?>
