<?php require("../../includes/initialize.php");?>
<?php require("../../includes/model.php") ; ?>


<?php 

class DocumentsModel extends Model {
	
	private $call_class = "document";
	public $requested	= array('id', 'kategorija', 'opis', 'bodovi',  'user_id');
	
	
	public function individual_process() {
		global $session;
		if(!isset($_GET) or !$session->is_admin()) {
			echo "Invalid request";
			redirect_to("../documents.php");
		} elseif(isset($_POST['type'])) {
			$this->save_document();
		} else {	
			switch($_GET['action']) {
				case "get_verified": $this->get_verified($session->user_id); break;
				case "get_unverified": $this->get_unverified($session->user_id); break;
				case "user": $this->verified_for_user($_GET['user_id']) ; break;
				case "approve": $this->approve_document($_GET['id']); break;
				case "delete": $this->delete_document($_GET['id'],$_GET['razlog']); break;
				default: echo "Invalid request";
			}
		}
		
	}
	
	// Verified for user
	
	private function verified_for_user($user_id) {
		$verified = Document::verified_for_user($user_id);
		foreach ($verified as $document) {
			$kategorija = Doc_Category::find_by_id($document->kategorija);
			if($document->kategorija == 1) {
				$document->bodovi = $document->opis * $kategorija->bodovi;
			} else {
				$document->bodovi = $kategorija->bodovi;
			}
			$document->kategorija = $kategorija->ime;
		}
		$this->create_JSON($verified);
	}
	
	
	// Get participations
	
	private function get_verified($user_id) {
		$verified = Document::verified_for_all();
		$this->requested = array('id', 'kategorija', 'opis', 'bodovi', 'filename', 'target_ime', 'user_id', 'approver');
		foreach ($verified as $document) {
			$kategorija = Doc_Category::find_by_id($document->kategorija);
			if($document->kategorija == 1) {
				$document->bodovi = $document->opis * $kategorija->bodovi;
			} else {
				$document->bodovi = $kategorija->bodovi;
			}
			$document->kategorija = $kategorija->ime;
			$document->target_ime = User::name_from_id($document->user_id);
			$document->approver = User::name_from_id($document->officer);
		}
		$this->create_JSON($verified);
	}
	
	// Get individual
	
	private function get_unverified() {
		$this->requested = array('id', 'kategorija', 'opis', 'filename', 'target_ime', 'user_id');
		$unverified = Document::unverified_for_all();
		foreach ($unverified as $document) {
			$document->kategorija = Doc_Category::name_from_id($document->kategorija);
			$document->target_ime = User::name_from_id($document->user_id);
		}
		$this->create_JSON($unverified);
	}
	
	// Approve document
	
	private function approve_document($id) {
		global $session;
		
		if($session->is_admin()) { 
			$document = Document::approve($id);
			
		} else {
		echo "Invalid request";
		}
	}
	
	// Delete document
	
	private function delete_document($id, $razlog) {
		global $session;
		
		$document = Document::find_by_id($id);
		$kategorija = Doc_Category::name_from_id($document->kategorija);
		$opis = $document->opis;
		$file_name = $document->filename;
		$user_id = $document->user_id;
		if($session->is_admin()) { 
			$document->delete();
			$file = "../../uploads/" . md5($user_id) . "/" . $file_name;
			unlink($file);
			$this->send_delete_reason($user_id, $razlog, $kategorija, $opis);
		} else {
		echo "Invalid request";
		}
		
	}
	
	// Send delete reason
	
	private function send_delete_reason($user_id, $razlog, $kategorija, $opis) {
		$user = User::find_by_id($user_id);
		$this->to_mail = $user->mail;
		$to_name = $user->ime . " " . $user->prezime;
			
			
			$mail = new PHPMailer();
		
			// SMTP 
			/*$mail->IsSMTP();
			$mail->Host = MAIL_HOST;
			$mail->Port = MAIL_PORT;
			$mail->SMTPAuth = true;
			$mail->Username = MAIL_USERNAME;
			$mail->Password = MAIL_PASSWORD;*/

			$mail->CharSet="UTF-8";
 			

			$mail->FromName = MAIL_FROM_NAME;
			$mail->From = MAIL_FROM;
			$mail->AddAddress($this->to_mail, $to_name);
			$mail->Subject = "Potvrda odbijena";
			$mail->WordWrap = 70;
			$mail->IsHTML(true);
			$mail->Body = "<p>Poštovani,</p>
			<p> </p>
			<p>Vaša potvrda</p>
			<p>Kategorija: {$kategorija} </p>
			<p>Opis: {$opis} </p>
			<p> je odbijena zbog: </p>
			<p>{$razlog}</p><p> </p>
			<p> Za dodatna objašnjenja slobodno se obratite na <br />leo.zagreb@cromsic.hr <br /> ili <br />lore.zagreb@cromsic.hr"
			;


		$result = $mail->Send();
		}
	
	// Save document
	
	private function save_document() {
		$attributes = $_POST;
		$this->errors = array();
		foreach ($attributes as $key => $value) {
			$this->$key 		= $value;
		}
			
		$document = new Document;
		foreach(Document::$db_fields as $key) {
			if(!empty($this->$key)) {
				$document->$key = $this->$key;
			} else {
				$document->$key = NULL;
			}
		}
			
			$document->save();
	}
}



$model = new DocumentsModel();



?>
