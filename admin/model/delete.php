<?php require("../../includes/initialize.php");?>
<?php 

if(!$session->is_admin() or (!isset($_GET['id']))) { 
redirect_to("../../index.php") ; 
}



$class = $_GET['class'];

$object = $class::find_by_id($_GET['id']);



function has_children($object, $class) {
	switch($class) {
		case "project": return project_has_children($object); break;
		case "activity": return activity_has_children($object); break;
		case "instance": return instance_has_children($object); break;
		case "signup": return signup_has_children($object); break;
		case "doc_category": return doc_category_has_children($object); break;
		case "user": return user_has_children($object); break;
		case "af": return delete_af($object); break;
		default: return false;
	}
}

function project_has_children($project) {
  	$children =	Activity::get_activities($project->id) ;
	if(empty($children)) { return false; } else { return true; }
}

function activity_has_children($activity) {
	$sql = "SELECT * FROM instances WHERE activity_id = {$activity->id} "; 
	$children = Instance::find_by_sql($sql);	
	if(empty($children)) { return false; } else { return true; }
}

function instance_has_children($instance) {
	$children = Participants::for_instance($instance->id);

	
	if(empty($children)) { return false; } else { return true; }
}

function signup_has_children($signup) {
	//$children = SignupUsers::for_instance($signup->id);

	
	if(empty($children)) { return false; } else { return true; }
}

function doc_category_has_children($object) {
	$sql = "SELECT * FROM documents WHERE kategorija = {$object->id} "; 
	$children = Document::find_by_sql($sql);	
	if(empty($children)) { return false; } else { return true; }
}

function delete_af($object) {
	if($object->delete()) {
		$session->message("Stavak obrisan", "success");
		redirect_to($_SERVER['HTTP_REFERER']);
	} else {
		$session->message("Dogodila se greška, pokušajte ponovno", "danger");
		redirect_to($_SERVER['HTTP_REFERER']);
	}
}

function user_has_children($user) {
	$children = Document::for_user($user->id);
	$sql = "SELECT * FROM individual WHERE user_id = {$user->id} OR creator_id = {$user->id} "; 
	$check = Individual::find_by_sql($sql);
	array_push($children, $check);	
	$sql = "SELECT * FROM instances WHERE user_id = {$user->id} "; 
	$check = Instance::find_by_sql($sql);	
	array_push($children, $check);	
	$sql = "SELECT * FROM participants WHERE user_id = {$user->id} "; 
	$check = Participants::find_by_sql($sql);	
	array_push($children, $check);	
	
	foreach( $children as $key => $value ){
    	if(empty($value)){
      	  unset( $children[$key] );
    	}
	}
	print_r($children);
	if(empty($children)) { return false; } else { return true; }
}

if(has_children($object, $class)) {
	$session->message("Ne možete obrisati stavku u upotrebi.", "warning");
	redirect_to($_SERVER['HTTP_REFERER']);
} else {
	if($object->delete()) {
		$session->message("Stavak obrisan", "success");
		redirect_to($_SERVER['HTTP_REFERER']);
	} else {
		$session->message("Dogodila se greška, pokušajte ponovno", "danger");
		redirect_to($_SERVER['HTTP_REFERER']);
	}
}



?>
