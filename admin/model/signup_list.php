<?php require("../../includes/initialize.php");?>
<?php require("../../includes/model.php") ; ?>


<?php 

class SignupListModel extends Model {
	
	private $call_class = "signup";
	public $requested	= array('id', 'activity_ime', 'ime', 'potrebno', 'zamjena','opis', 'user_ime', 'datum', 'special', 'export');
	
	
	public function individual_process() {
		$class = $this->call_class;
		$instances = $class::find_all();
		foreach ($instances as $instance) {
			$instance->activity_ime = Activity::name_from_id($instance->activity_id);
			$instance->user_ime = User::name_from_id($instance->creator_id);
			unset ($instance->activity_id);
			unset ($instance->user_id);	
			if($instance->export==1){
				$instance->export='<i style="color:#11A8AB" class="glyphicon glyphicon-ok"></i>';
			}else{
				$instance->export='<i style="color:#A52A2A" class="glyphicon glyphicon-remove"></i>';	
			}
		}
		$this->create_JSON($instances);
	}
	
	
	
}



$model = new SignupListModel();



?>
