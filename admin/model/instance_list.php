<?php require("../../includes/initialize.php");?>
<?php require("../../includes/model.php") ; ?>


<?php 

class InstanceListModel extends Model {
	
	private $call_class = "instance";
	public $requested	= array('id', 'activity_ime', 'ime', 'bodovi', 'opis', 'user_ime', 'datum');
	
	
	public function individual_process() {
		$class = $this->call_class;
		$instances = $class::find_all();
		foreach ($instances as $instance) {
			$instance->activity_ime = Activity::name_from_id($instance->activity_id);
			$instance->user_ime = User::name_from_id($instance->user_id);
			unset ($instance->activity_id);
			unset ($instance->user_id);	
		}
		$this->create_JSON($instances);
	}
	
	
	
}



$model = new InstanceListModel();



?>
