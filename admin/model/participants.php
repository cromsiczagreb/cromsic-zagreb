<?php require("../../includes/initialize.php");?>
<?php require("../../includes/model.php") ; ?>


<?php 

class ParticipantsModel extends Model {
	
	private $call_class = "Participants";
	public $requested = array('id', 'prezime', 'ime', 'jmbag' );
	
	
	public function individual_process() {
		if(!isset($_GET)) {
			echo "Invalid request";
		} else {	
			switch($_GET['action']) {
				case "get_list": $this->get_participations($_GET['instance_id']); break;
				default: echo "Invalid request";
				case "remove": $this->remove_participants($_GET['instance_id'], $_GET['ids']); break;
				case "add": $this->add_participants($_GET['instance_id'], $_GET['ids']); break;
			}
		}
	}
	
	// Get list of participants
	
	private function get_participations($instance_id) {
		$participants = Participants::for_instance($instance_id);
		$this->create_JSON($participants);
	}
	
	// Remove participants
	
	private function remove_participants($instance_id, $user_ids) {
		$user_ids = explode(", ", $user_ids);
		array_pop($user_ids);
		Participants::remove_participants($instance_id, $user_ids);
		
	}
	
	// Remove participants
	
	private function add_participants($instance_id, $user_ids) {
		$user_ids = explode(", ", $user_ids);
		array_pop($user_ids);
		Participants::add_participants($instance_id, $user_ids);
		
	}
	
}


$model = new ParticipantsModel();



?>
