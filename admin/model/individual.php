<?php require("../../includes/initialize.php");?>
<?php require("../../includes/model.php") ; ?>


<?php 

class IndividualModel extends Model {
	
	
	public $requested = array  ("id", "opis", "datum", "bodovi", "user_ime", "target_ime");
	
	public function individual_process() {
		if(!isset($_GET)) { echo "Invalid request." ; }
		if($_GET['action'] == "get_list") { $this->get_list(); }
		
	}
	
	
	// Get list 
	
	private function get_list() {
		$individuals = Individual::find_all();
		foreach ($individuals as $individual) {
			$individual->user_ime = User::name_from_id($individual->creator_id);
			$individual->target_ime = User::name_from_id($individual->user_id);
			unset ($individual->creator_id);
			unset ($individual->user_id);	
		}
		$this->create_JSON($individuals);
	}
	
	
	
}


		


$model = new IndividualModel();



?>
