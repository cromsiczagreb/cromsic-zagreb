<?php require("../../includes/initialize.php");?>
<?php require("../../includes/model.php") ; ?>


<?php 

class ActivityListModel extends Model {
	
	private $call_class 	= "Activity";
	public $requested		= array('id', 'project_ime', 'ime', 'bodovi', 'opis');
	
	
	public function individual_process() {
		$class = $this->call_class;
		$activities = $class::find_all();
		foreach ($activities as $activity) {
			$activity->project_ime = Project::name_from_id($activity->project_id);
			unset ($activity->project_id);	
		}
		$this->create_JSON($activities);
	}
	
	
	
}



$model = new ActivityListModel();



?>
