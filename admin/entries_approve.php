<?php require("../includes/initialize.php");?>
<?php if(!$session->is_admin()) { redirect_to("../index.php"); } ?>
<?php require ("controller/" . ME_MPHP."_controller.php"); ?>

<?php include('../layout/header.php') ?>

<div id="main">
<div id="navigation">
<?php require("layout/admin_nav.php");?>
</div>
<?php echo output_message($message); ?>
<div id="page">

<?php $list = new EntriesApprove()?>
<?php $delete_modal = new EntriesDeleteModal("delete_entries", "Obriši prijavu"); ?>

</div>
</div>
<?php include('../layout/footer.php') ?>
<script> add_listener("entries_admin") </script>
