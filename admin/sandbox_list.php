<?php require("../includes/initialize.php");?>
<?php if(!$session->is_admin()) { redirect_to("../index.php"); } ?>
<?php require ("controller/" . ME_MPHP."_controller.php"); ?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>CroMSIC Zagreb</title>

<link href="http://zagreb.cromsic.hr/resources/css/general.css" media="all" rel="stylesheet" type="text/css" />
<link href="http://zagreb.cromsic.hr/external/bootstrap/datepicker/css/bootstrap-datepicker.standalone.css" media="all" rel="stylesheet" type="text/css" />
<link href="http://zagreb.cromsic.hr/external/bootstrap/css/bootstrap.min.css" media="all" rel="stylesheet" type="text/css" />
<link href="http://zagreb.cromsic.hr/external/bootstrap/css/bootstrap-theme.min.css" media="all" rel="stylesheet" type="text/css" />
<link href="http://zagreb.cromsic.hr/external/bootstrap/bootstrap-table-master/bootstrap-table.min.css" media="all" rel="stylesheet" type="text/css" />


<script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<script type="text/javascript" src="http://zagreb.cromsic.hr/external/bootstrap/datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="http://zagreb.cromsic.hr/external/bootstrap/bootstrap-table-master/bootstrap-table.min.js"></script>
<script type="text/javascript" src="http://zagreb.cromsic.hr/external/bootstrap/bootstrap-table-master/locale/bootstrap-table-hr-HR.min.js"></script>
<script type="text/javascript" src="http://zagreb.cromsic.hr/resources/js/js.js"></script>
<script type="text/javascript" src="../external/bootstrap/bootstrap-table-master/extensions/export/bootstrap-table-export.min.js"></script>
<script type="text/javascript" src="../external/tableExport/tableExport.min.js"></script>
<script type="text/javascript" src="../external/tableExport/libs/FileSaver/FileSaver.min.js"></script>
<script type="text/javascript" src="../external/tableExport/libs/html2canvas/html2canvas.min.js"></script>
<script type="text/javascript" src="../external/tableExport/libs/jsPDF/jspdf.min.js"></script>
<
</head>

<body>
<div id="header">
</div>

<div id="main">
<div id="navigation">
<?php require("layout/admin_nav.php");?>
</div>
<div id="page">
<?php echo output_message($message); ?>

<?php $list = new MemberList()?>
<a href="#" onClick="something()">Sand</a>
</div>
</div>
<?php include('../layout/footer.php') ?>


