<?php require("../includes/initialize.php");?>
<?php if(!$session->is_admin()) { redirect_to("../index.php"); } ?>
<?php require ("controller/" . ME_MPHP."_controller.php"); ?>

<?php include('../layout/header.php') ?>

<div id="main">
<div id="navigation">
<?php require("layout/admin_nav.php");?>
</div>
<div id="page">
<?php echo output_message($message); ?>
<div id="float_container">
<div id="half_page_div_left">
<?php $instance_view = new SignupView()?>
</div>
<div id="half_page_div_right">
<?php $participants = new SignupUsersRequiredList($_GET['id']);?>
</div>

<div id="half_page_div_right" style="padding-top:40px">
<?php $participants = new SignupUsersSubsList($_GET['id']);?>
</div>
</div>
</div>
</div>
<?php include('../layout/footer.php') ?>

<script>add_datepicker_now('#datepicker')</script>
<script>add_datetimepicker('#datetimepicker')</script>
<script>add_datetimepicker('#datetimepickertwo')</script>
<script>
tinymce.init({
    selector: "textarea",
    plugins: [
        "advlist autolink lists link image charmap print preview anchor",
        "searchreplace visualblocks code fullscreen",
       
    ],
    
});

</script>