<?php require("../includes/initialize.php");?>
<?php if(!$session->is_admin()) { redirect_to("../index.php"); } ?>
<?php require ("controller/" . ME_MPHP."_controller.php"); ?>

<?php include('../layout/header.php') ?>

<div id="main">
<div id="navigation">
<?php require("layout/admin_nav.php");?>
</div>
<div id="page">
<?php echo output_message($message); ?>
<div id="login_form">
<?php $news_create = new NewsCreate()?>
</div>
</div>
</div>
<?php include('../layout/footer.php') ?>


<script>
tinymce.init({
    selector: "textarea.editme",
    plugins: [
        "advlist autolink lists link image charmap print preview anchor",
        "searchreplace visualblocks code fullscreen",
       
    ],
    
});

</script>