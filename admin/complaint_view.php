<?php require("../includes/initialize.php");?>
<?php if(!$session->is_admin()) { redirect_to("../index.php"); } ?>
<?php require ("controller/" . ME_MPHP."_controller.php"); ?>

<?php include('../layout/header.php') ?>

<div id="main">
<div id="navigation">
<?php require("layout/admin_nav.php");?>
</div>
<?php echo output_message($message); ?>
<div id="page">

<div id="half_page_div_left">
<?php $comp = new ViewComp($complaint) ?>
</div>
<div id="half_page_div_right">
<?php $comp = new ReplyForm($complaint) ?>
</div>



</div>
</div>
<?php include('../layout/footer.php') ?>

<script>
tinymce.init({
    selector: "textarea",
    plugins: [
        "advlist autolink lists link image charmap print preview anchor",
        "searchreplace visualblocks code fullscreen",
       
    ],
    
});

</script>