<?php require("../includes/initialize.php");?>
<?php if(!$session->is_admin()) { redirect_to("../index.php"); } ?>

<?php include('../layout/header.php') ?>

<div id="main">
<div id="navigation">
<?php require("layout/admin_nav.php");?>
</div>
<div id="page">
<div class="alert alert-danger" role="alert">Prođite kroz aktivnosti koje ste odobrili i pripazite na one nakon 24. 10. 2015. (vidi dalje)</div>
<p>Odite pod Razmjene - Aktivnosti u tražilicu upišite svoje ime i pogledajte šta se odobrili nakon 24. 10. 2015.; ako ima opravdan razlog, samo mi javite  brojeve, ako nema, onda dodajte te ljude pod događaju koje ste ionako morali napravit u jednom trenutku! Sve šta je nakon 24. 10. a niste mi rekli broj bit će obrisano do nedelje navečer! </p><br  />
<h3>Obavijest </h3><br  />
<p>U tijeku je prvi krug natječaja za razmjene pa odobravajte aktivnosti </p>
<br />
<h3>Guide</h3>
<br /><br />
<h4>Otvaranje prijava </h4>
<div class="alert alert-info" role="alert">Sve prijave od sada preko stranice (osim ako nije posebna prijava zbog nekih preduvjeta)! NE PREKO MAILA! </div>
<p>Za otvaranje prijava idi pod <strong>Unesi - Prijave</strong> </p>
 <p>Kliknut na plus, i popunit podatke, u Prijave od odabrat vrijeme kad se prijave otvaraju i naglasit u mailu da znaju. </p>
 <p>Nakon što događaj završi potrebno ga je Pretvorit u događaj s trećom ikonicom da bi se pravilno bodovao; maknut sa liste one koji se nisu pojavili. </p>
 <br /><br />
 <h4>Unošenje bodova</h4>
 <p> Glavni način za provest bodove kroz sustav je unošenjem događaja <strong>Unesi - Događanja</strong> ako on prethodno nije napravljen iz Prijave.</p>
<p> Odabrat aktivnost - većina podataka će se sama ispisat tako da treba samo odabrat datum, te ako su stvari po satu dat odgovarajuć broj bodova, te dodat sudionike koji su bili.</p>
 <br /><br />
<h4>Pojedinačne korekcije</h4>
 <p>Za slučaj da trebate napraviti korekcije nekih bodova; primjerice neko vam je na nekom događaju bio extra dugo, ili je otišo brže ili tako to radite pod <strong>Unesi - Pojedinačno</strong></p>
<p> Opet stisnut plusić, s desne strane odabrat člana s liste, unjeg bodove (može i negativno te objašnjenje.</p>
 <br /><br />
<h4>Odobravanje aktivnosti</h4>
 <p>Aktivosti odobravate tako da odete na <strong>Odobri - Aktivnosti</strong> i odaberete svoj odbor, opet ponavljam, odobravate sve aktivnosti prije 24. 10. 2015.</strong></p>
<p>Ako je netko prijavio aktivnosti nakon tog datuma, odbijte mu, i upišite u odgovarajući događaj</p>
</div>   
</div>
</div>
<?php include('../layout/footer.php') ?>