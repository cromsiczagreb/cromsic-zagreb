<?php require("../includes/initialize.php");?>
<?php if(!$session->is_admin()) { redirect_to("../index.php"); } ?>

<?php


if(!isset($_GET['signup_id'])) {
	redirect_to("index.php");
}

$signup = Signup::find_by_id($_GET['signup_id']);
if(!is_object($signup)) { redirect_to("signup_list.php") ; }

if(($session->user_type != 4) and ($session->user_id != $signup->creator_id)) {
	$session->message("Samo autor prijava može unijeti ovaj događaj", "info");
	redirect_to("signup_list.php");
}	

$signup->old_id = $signup->id;
unset ($signup->id);	
$instance = new Instance;

		// Assign values to instance

		foreach(Instance::$db_fields as $key) {
			if(!empty($signup->$key)) {
				$instance->$key = $signup->$key;
			} else {
				$instance->$key = NULL;
			}
		}
		
		$instance->user_id = $signup->creator_id;
		$instance->opis = s($instance->opis);
		
		// Find points in activity template
		$activity = Activity::find_by_id($signup->activity_id);
		$instance->bodovi = $activity->bodovi;
		

		// Create instance
		
			if($instance->save()) {
				
				// If successful add participants
				$users = SignupUsers::for_instance_required($signup->old_id);
				$user_ids = array();
				foreach($users as $user) {
					$user_ids[] = $user->id;
				}
				Participants::add_participants($instance->id, $user_ids);
				Signup::set_exported($signup->old_id);
				$session->message("Uspješno", "success");
				redirect_to("instance_view.php?id={$instance->id}");
				
			
			} else {
				
				// IF instance creaton not successful:
				
				$session->message("Problem u unosu događaja", "danger");
				redirect_to("signup_list.php?");
			}



?>