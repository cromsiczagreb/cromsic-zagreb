<?php require("../includes/last_member_export.php") ; ?>
<?php

class MemberExport extends Controller {
	

	// Table options

	public $columns = array('registered','prezime', 'ime', 'mail','mobitel', 'jmbag', 'mjesto', 'datum_rod', 'studij', 'godina_sad', 'godina_upis',
							 'trajanje', 'vrsta');
	public $data_url 					= "model/member_export.php";
	public $action						= "member_view";

	
		
	function __construct() {
		$last_export = sql_to_hr_datetime(LastMemberExport::get_last_export());
		echo "<div id=\"float_container\"><div id=\"half_page_div_left\">";
		echo "<h4>Prikaži od: </h4>";
		echo "<div style=\"position:relative\"><input type=\"text\" name=\"datum_export\" id=\"datepicker\" value=\"{$last_export}\"></div>";
		echo "</div><div id=\"half_page_div_right\">";
		echo "<h4>Zadnji izvoz:</h4>";
		echo "<span id=\"text_last\">{$last_export}</span>  <i class=\"glyphicon glyphicon-refresh\" id=\"refresh_last\" style=\"cursor: pointer;\"></i>";
		echo "</div> <br style=\"clear:both;\" /></div>";
		$this->create_table();
	}
	
	
}

class ExportForm extends Form {
	
		public $errors = array();
		public $datum;

		
		
		// Individual process
		
		public function individual_process() {
					 $this->finalize();
		}
		
				
		// Finalize
		
		private function finalize() {
			global $session;
		
			if(LastMemberExport::refresh_export($datum)) {
					redirect_to("member_export.php");
			} else {
					$this->errors['error'] = "Greška sa poslužiteljem. Pokušajte ponovno kasnije." ;
					$this->show_errors() ;
			}
	
		}
		
		
		
		
				
		// Form creation
		
		public function create_form() {
			$this->start_form("member_export.php");	
			$this->end_form("Jesam");
		}
}
?>
    