<?php

class DocumentVerifyList extends Controller {
	
	
	
	// Table options
	
	public $columns						= array('kategorija', 'opis', 'target_ime' );
	public $data_url 					= "model/document.php";
	public $action						= "admin_unverified";
	//public $create_new_link 			= "individual_create.php";
	public $modal						= false;
	public $action_icon					= "glyphicon-plus";
	
		
	function __construct() {
		echo "<h3>Potvrde na čekanju</h3>";
		$this->data_url = $this->data_url . "?action=get_unverified";
		$this->create_table();
	}
	

}


class DocumentEditModal extends Form {

		
		// Create form
		
		function __construct($type, $title) {
			$modal_id	= $type . "_modal";
			$button_id	= $type . "_button";
			$form_id	= $type . "_form";
			$form_type  = $type;
			$this->create_modal_form($modal_id, $title, $button_id, $form_id, $form_type);
		}
		
		public function create_modal_form($modal_id, $modal_title, $button_id, $form_id, $form_type) {
			$this->start_modal($modal_id, $modal_title);
			$this->input_form($form_id, $form_type) ;
			$this->end_modal($button_id);
		}

		// Input form
		
		public function input_form($form_id, $form_type) {
			$IDs = Doc_Category::find_all();
			foreach ($IDs as $ID) {
				$values[] = $ID->id;
			}
			$names_origin = "name_from_id";
			echo "<div id=\"modal_form\">";
			$this->start_form("#", $form_id);
			$this->add_element_hidden("type", $form_type);
			$this->add_element_select('kategorija', "Kategorija", "", $values, "", $names_origin, "Doc_Category");
			echo "<br /><br />";
			$this->add_element("opis", "Opis ", "text", $tooltip=false, "", "Opis", "opis")	;
			echo "</form></div>";
		}

}

class DocumentDeleteModal extends Form {

		
		// Create form
		
		function __construct($type, $title) {
			$modal_id	= $type . "_modal";
			$button_id	= $type . "_button";
			$form_id	= $type . "_form";
			$form_type  = $type;
			$this->create_modal_form($modal_id, $title, $button_id, $form_id, $form_type);
		}
		
		public function create_modal_form($modal_id, $modal_title, $button_id, $form_id, $form_type) {
			$this->start_modal($modal_id, $modal_title);
			$this->input_form($form_id, $form_type) ;
			$this->end_modal($button_id);
		}

		// Input form
		
		public function input_form($form_id, $form_type) {
			echo "<div id=\"modal_form\">";
			$this->start_form("#", $form_id);
			$this->add_element_hidden("id", 0);
			$this->add_element("razlog", "Razlog ", "text", $tooltip=false, "", "Razlog", "razlog")	;
			echo "</form></div>";
		}

}


?>
    