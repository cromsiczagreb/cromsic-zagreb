<?php

if (!isset($_GET['id'])) {
	redirect_to("../index.php");	
}

$application = Application::find_by_id($_GET['id']);


if(empty($application)) {
	redirect_to("af_list.php");
}



				


class ViewApp extends Form {
	
	
	function __construct($application) {
		    global $session;
		    
			$user = User::find_by_id($application->user_id);
			
			$checked_f = "";
			$checked_m = "";
			$checked_p = "";
			$checked_r = "";
		
			if($application->spol == 1) { $spol = "M" ; } else { $spol = "Ž";}
			if ($application->type == 1) { $checked_p = "checked" ; $checked_r = "disabled";}
			if ($application->type == 2) { $checked_r = "checked" ; $checked_f = "disabled";}
			
			
			echo "<div id=\"appform\">";
			echo "<div class=\"square_middle\" style=\"text-align: center\"><h4> Vrsta razmjene </h4><br />";
			echo "<div class=\"cc-selector\">
			<div class=\"square_left\"> 
        <input id=\"scope\" type=\"radio\" name=\"type\" required {$checked_p} value=\"1\" />
        <label class=\"drinkcard-cc scope\" for=\"scope\"></label></div>
        <div class=\"square_right\"><input id=\"score\" type=\"radio\" name=\"type\" {$checked_r} value=\"2\" />
        <label class=\"drinkcard-cc score\" for=\"score\"></label></div>
    </div>";
			echo "</div>";
			echo "<div class=\"square_middle\">";
			echo "<div class=\"square_left\"> <h4>Opći podaci</h4> ";
			echo "<br />";
			echo "<p><strong>Ime:</strong>";
			echo "&nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp $user->ime</p>";
			echo "<p><strong>Prezime:</strong>";
			echo "&nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp  $user->prezime</p>";
			echo "<p><strong>Nacionalnost:</strong>";
			echo "&nbsp &nbsp &nbsp  $application->nacionalnost</p>";
			echo "<p><strong>Materinji jezik:</strong>";
			echo "&nbsp &nbsp $application->jezik</p>";
			echo "<p><strong>Spol:</strong>";
			echo "&nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp $spol";
			echo "</div>";
			echo "<div class=\"square_right\"> <h4>Kontakt </h4> <br /> ";
			echo "<p><strong>Ulica i kućni broj:</strong>";
			echo "&nbsp &nbsp &nbsp  $application->adresa</p>";
			echo "<p><strong>Mjesto i poštanski broj:</strong>";
			echo "&nbsp &nbsp &nbsp  $application->mjesto</p>";
			echo "<p><strong>Fiksni:</strong>";
			echo "&nbsp &nbsp &nbsp  $application->fiksni</p>";
			echo "<p><strong>Mobitel:</strong>";
			echo "&nbsp &nbsp $user->mobitel</p>";
			echo "<p><strong>E-mail:</strong>";
			echo "&nbsp &nbsp &nbsp   $user->mail</p>";
			echo "<br />";
			echo "</div></div></div>";
			echo "<div class=\"square_middle\"> <h4>&nbsp Prošle razmjene</h4> <br />";
			echo "<p><strong>&nbsp &nbsp Jeste li već ste bili na razmjeni preko CroMSIC-a?</strong> (navesti državu i datum zadnje razmjene) <br /><br />";
			echo "&nbsp &nbsp &nbsp  $application->razmjena</p>";
			echo "</div>";
			echo "<div class=\"square_middle\"> <h4>&nbsp Status na fakultetu </h4> <br />";
			echo "<div class=\"square_left\"> ";
			echo "<p><strong>Godina upisa na 1. godinu:</strong>";
			echo "&nbsp &nbsp $user->godina_upis</p>";
			echo "<p><strong>Godina upisa na 4. godinu:</strong>";
			echo "&nbsp &nbsp $application->upisiv</p>";
			echo " </div>";
			echo "<div class=\"square_right\"> ";
			echo "<p><strong>Trenutačna studijska godina:</strong>";
			echo "&nbsp &nbsp $user->godina_sad</p>";
			echo "<p><strong>Prosjek ocjena:</strong>";
			echo "&nbsp &nbsp ". $user->get_average($user->id) ."</p>";
			echo "</div></div>";
			if($session->user_type == "4") {
			echo "<a href=\"model\delete.php?class=application&id={$application->id}\" onclick=\"return confirm('Sigurno želite obrisati ovaj unos?')\" >";
			echo "<i class=\"glyphicon glyphicon-trash\"></i></a>";
			}
			
		}
		
		
	}




?>
