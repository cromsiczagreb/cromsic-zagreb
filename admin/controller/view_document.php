<?php require("../../includes/initialize.php");?>

<?php
if(!isset($_GET)) {
	redirect_to("../admin.php");
} else {
	if($session->is_admin()) {
		$link  = "../../uploads/";
		$link .= md5($_GET['user_id']) . "/" . Document::filename_from_id($_GET['id']);
		redirect_to($link);

	
	} else {
		redirect_to("../admin.php");
	}
}
?>