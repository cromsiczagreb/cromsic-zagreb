<?php

class ComplaintsWorking extends Controller {
	
	
	
	// Table options
	
	public $columns						= array('ime' );
	public $data_url 					= "model/complaints.php";
	public $action						= "admin_complaints";
	//public $create_new_link 			= "individual_create.php";
	public $modal						= false;
	public $action_icon					= "glyphicon-plus";
	
		
	function __construct() {
		echo "<h3>Žalbe u obradi</h3>";
		$this->data_url = $this->data_url . "?action=get_working";
		$this->create_table();
	}
	
}

class ComplaintsWaiting extends Controller {
	
	
	
	// Table options
	
	public $columns						= array('ime' );
	public $data_url 					= "model/complaints.php";
	public $action						= "admin_complaints";
	//public $create_new_link 			= "individual_create.php";
	public $modal						= false;
	public $action_icon					= "glyphicon-plus";
	
		
	function __construct() {
		echo "<h3>Žalbe na čekanju</h3>";
		$this->data_url = $this->data_url . "?action=get_waiting";
		$this->create_table();
	}

}


?>
    