<?php



class ParticipantsList extends Controller {
	
	
	
	
	// Table options
	
	public $columns 					= array	('prezime', 'ime', 'jmbag');
	public $data_url 					= "model/participants.php";
	public $action						= "";
	public $action_icon					= "glyphicon-trash";
	public $create_new_link             = "#deselect";
	public $modal						= false;
	public $checkboxes                  = "yes";

	
		
	function __construct() {
		global $session;
		echo "<h3>Sudionici</h3>";
		if(!isset($_GET['instance_id'])) { redirect_to ("../..index.php") ; }
		$instance_id = $_GET['instance_id'];
		$instance = Instance::find_by_id($_GET['instance_id']);
		if(($session->user_type != 4) && ($session->user_id != $instance->user_id)) {
				$session->message("Nemate pravo izmjenjivati ovaj događaj", "info");
				 redirect_to("instance_view.php?id={$instance_id}");
			}	
		
		$this->data_url = $this->data_url . "?action=get_list&instance_id={$instance_id}";
		$this->create_table();
	}
	

	
}


class MemberList extends Controller {
	
	
	
	
	// Table options
	
	public $columns = array('prezime', 'ime', 'jmbag','mail', 'mobitel', 'datum_rod', 'godina_sad');
	public $data_url 					= "model/member_list.php";
	public $action						= "";
	public $action_icon					= "glyphicon-plus";
	public $create_new_link             = "#add_new\" onClick=\"add_participants()\" ";
	public $modal						= false;
	public $checkboxes                  = "yes";


	
		
	function __construct() {
		echo "<h3>Članovi</h3>";
		$this->create_table();
	}
	
}

?>