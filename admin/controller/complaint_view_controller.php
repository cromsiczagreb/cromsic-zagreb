<?php



if (!isset($_GET['id'])) {
	redirect_to("complaints.php");	
}

$complaint = Complaint::find_by_id($_GET['id']);

if(empty($complaint)) {
	redirect_to("complaints.php");
}

$user = User::find_by_id($complaint->user_id);




class ReplyForm extends Form {
	
		public $errors;
		
	    public $reply;
		
		
		
		//	Construct
		
		function __construct($complaint) {
			global $session;
			
			if(isset($_POST['submit'])) {
				switch($_POST['action']) {
					case "1": $this->accept($complaint); break;
					case "2": $this->delete($complaint); break;
				default: echo "Invalid request";
				}
			}
			
			
			$this->create_form($complaint);	
			
			
		}
		
		// Individual process
		
		public function individual_process() {
				//$this->validate();
				$this->show_errors($this->errors);
				if(empty($this->errors)) {
					 //$this->prepare();
					 $this->finalize();
				}
		}
		
		// Validate
		
		private function Validate() {

						
		}
		
		// Prepare
		
		private function prepare() {
			
			//$this->ime = ucwords(strtolower($this->ime));
			
   
		
		}
		
		// Finalize
		
		private function finalize() {
			global $session;
		
			
		
		
			
			if($mail==0) {
				global $session;
				$session->message("Uspješno.", "success");
	 		 	//redirect_to('complaint.php');
			} else {
				$session->message("Došlo je greške. Pokušajte ponovno.", "warning");
				//redirect_to('complaint.php');
			}
			
		}
			
		// Accept complaint
			
		public function accept($complaint) {
			global $session;
			
			if(Complaint::approve($complaint->id)) {
				if($this->mail_response()) {
					$session->message("Uspješno.", "success");
					redirect_to("complaint_view.php?id={$complaint->id}") ;
				} else {
					$session->message("Problem sa slanjem maila.", "danger");
					redirect_to("complaint_view.php?id={$complaint->id}") ;
				}
			} else {
				$session->message("Došlo je greške. Pokušajte ponovno.", "warning");
				redirect_to("complaint_view.php?id={$complaint->id}") ;
				}
		}
			
		// Delete complaint
			
		public function delete($complaint) {
			global $session;
			
			$complaint->delete();
			if($this->mail_response()) {
					$session->message("Uspješno.", "success");
					redirect_to("complaint_view.php?id={$complaint->id}") ;
				} else {
					$session->message("Problem sa slanjem maila.", "danger");
					redirect_to("complaint_view.php?id={$complaint->id}") ;
				}
		}
			
		// Mail complaint
			
		public function mail_response() {
				
			global $user;
				
			$this->to_mail = $user->mail;
			$to_name = $user->ime . " " . $user->prezime;
			$text = $_POST['opis'];
			
			
			$mail = new PHPMailer();
		
			// SMTP 
			/*$mail->IsSMTP();
			$mail->Host = MAIL_HOST;
			$mail->Port = MAIL_PORT;
			$mail->SMTPAuth = true;
			$mail->Username = MAIL_USERNAME;
			$mail->Password = MAIL_PASSWORD;*/

			$mail->CharSet="UTF-8";
 			

			$mail->FromName = MAIL_FROM_NAME;
			$mail->From = MAIL_FROM;
			$mail->AddAddress($this->to_mail, $to_name);
			$mail->Subject = "Odgovor na žalbu";
			$mail->WordWrap = 70;
			$mail->IsHTML(true);
			$mail->Body =  $text . "<br /><p> Za dodatna objašnjenja slobodno se obratite na <br />leo.zagreb@cromsic.hr <br /> ili <br />		   lore.zagreb@cromsic.hr";

				$result = $mail->Send();
				return $result;
			}
				
			
		
		
	
		
		// Form creation
		
		public function create_form($complaint) {
			
			global $session;
			global $user;
						
			echo "<h4>Odgovor </h4><br />";
			echo "<br />";
			
			$this->start_form("complaint_view.php?id={$complaint->id}", "appform","novalidate");
			if($complaint->status == 0) {
				echo "<input type=\"radio\" checked name=\"action\" value=\"1\" > &nbsp <i class=\"glyphicon glyphicon-ok\"></i>&nbsp <strong>Prihvati</strong> ";
				echo "<br />";
 				echo "<input type=\"radio\" name=\"action\" value=\"2\"> &nbsp <i class=\"glyphicon glyphicon-remove\"></i>&nbsp <strong>Odbij</strong>	";
			} else {
				echo "<input type=\"radio\" checked name=\"action\" value=\"2\" > &nbsp <i class=\"glyphicon glyphicon-trash\"></i>&nbsp<strong>Obriši</strong> ";
			}
			echo "<br /><br />";	
			$this->add_element_textarea("opis", "Opis", "", "", "Opis", "", "");
			echo "<br />";
			
			$this->end_form("Pošalji");
			echo "<br />";
					
			
		}
		
}
				


class ViewComp extends Form {
	
	
	function __construct($complaint) {
		
		global $user;
		
		echo "<h4>Žalba</h4>";
		echo "<br />";
		echo $complaint->content;
		echo "<br /><br /><br />";
		echo "<strong>Član:</strong> &nbsp &nbsp <a href=\"member_view.php?id={$complaint->user_id}\" target=\"_blank\"> {$user->ime} {$user->prezime} </a><br />";
		echo "<strong>Mail:</strong> &nbsp &nbsp &nbsp {$user->mail}";
		echo "<br />";
		echo "<strong>Broj:</strong> &nbsp  &nbsp &nbsp {$user->mobitel}";
		}
		
		
	}




?>
