<?php

class SignupView extends Form {
	
			
			private $can_edit = false;
			private $disabled = "disabled";
			private $instance;
			
	//	Construct
		
		function __construct() {
			global $session;
			
			if (!isset($_GET['id'])) {
				redirect_to("../..index.php");	
			}

			if (isset($_POST['submit'])) {
				
				$remove_submit = array_pop($_POST);
				$attributes = $_POST;
				$this->errors = array();
				foreach ($attributes as $key => $value) {
					$this->$key 		= $value;
				}
				$this->individual_process();	
			}
			
			$signup = Signup::find_by_id($_GET['id']);
			if(!is_object($signup)) { redirect_to("signup_list.php") ; }
			if($session->user_type == 4 or $session->user_id == $signup->creator_id) {
				 $this->disabled = ""; 
				 $this->can_edit = true;
			}	
			$this->create_form($signup);	
			
						
		}
		
	
		// Individual process
		
		public function individual_process() {
					 $this->prepare();
					 $this->finalize();
		}
		
		// Prepare
		
		private function prepare() {

			// Fix date for DB
			$this->datum = hr_to_sql($this->datum);
			$this->prijaveod = hr_to_sql_datetime($this->prijaveod);
			$this->odjavedo = hr_to_sql_datetime($this->odjavedo);
		
		}
		
		// Finalize
		
		private function finalize() {
			$signup = new Signup;
			foreach(Signup::$db_fields as $key) {
				if(!empty($this->$key)) {
					$signup->$key = $this->$key;
				} else {
					$signup->$key = NULL;
				}
			}
			//$user->save();
			if($signup->save()) {
				global $session;
				$session->message("Uspješno", "success");
	 		 	redirect_to("signup_view.php?id=" . $signup->id);
			} else {
				global $session;
				redirect_to("signup_view.php?id=" . $signup->id);
			}
		}
		
		
		// Create form
		
		public function create_form($signup) {
			echo "<h3>Događaj</h3>";
			echo "<br />";
			echo "<i class=\"glyphicon glyphicon-link\"></i>: <a href=../Dogadanje{$signup->id}>zagreb.cromsic.hr/Dogadanje{$signup->id}</a>";
			$IDs = Activity::find_all();
			foreach ($IDs as $ID) {
				$values[] = $ID->id;
			}
			$names_origin = "name_from_id";
			$this->start_form("signup_view.php?id=" . $signup->id, "novalidate");
			$this->add_element_select('activity_id', "", $signup->activity_id, $values, $this->disabled, $names_origin, "Activity");
			echo "<br /><br />";
			$this->add_element_hidden("id",  $signup->id);
			$this->add_element("ime", "Ime ", "text", "", $signup->ime, "Ime","", $this->disabled);
			$this->add_element("potrebno", "Potrebno ", "number", "", $signup->potrebno, "Potrebno", "", $this->disabled)	;
			$this->add_element("zamjena", "Zamjena ", "number", "", $signup->zamjena, "Zamjena", "", $this->disabled)	;
			echo "<br /><br />";
			$this->add_datepicker("datum", "Datum ", "text", "", sql_to_hr($signup->datum), "Datum","datepicker", $this->disabled);
			$this->add_datepicker("prijaveod", "Prijave od ", "text", "", sql_to_hr_datetime($signup->prijaveod), "Prijave od","datetimepicker", $this->disabled);
			$this->add_datepicker("odjavedo", "Odjave do ", "text", "", sql_to_hr_datetime($signup->odjavedo), "Odjave do","datetimepickertwo", $this->disabled);
			echo "<br /><br />";
			$this->add_element("napravio", "Napravio ", "text", "", User::name_from_id($signup->creator_id), "", "", "disabled");
			$this->add_element_textarea("opis", "Opis ", "", "", "","", $this->disabled, $signup->opis );
			echo "<br /><br />";
			$this->end_form_save($signup->id, "signup", $this->can_edit);
		}
		
		
		
		
}


class SignupUsersRequiredList extends Controller {
	
	
	
	
	// Table options
	
	public $columns 					= array	('broj','prezime', 'ime', 'mail');
	public $data_url 					= "model/signup_users.php";
	public $action						= "";
	public $action_icon					= "glyphicon-user";
	public $create_new_link 			= "signup_users.php";
	public $modal						= false;
	
		
	function __construct($signup_id) {
		echo "<h3>Prijavljeni - potrebno</h3>";
		$this->data_url = $this->data_url . "?action=get_list_required&signup_id={$signup_id}";
		$this->create_new_link = $this->create_new_link . "?signup_id={$signup_id}";
		$this->create_table();
	}
	

	
}

class SignupUsersSubsList extends Controller {
	
	
	
	
	// Table options
	
	public $columns 					= array	('broj','prezime', 'ime', 'mail');
	public $data_url 					= "model/signup_users.php";
	public $action						= "";
	public $action_icon					= "glyphicon-user";
	public $create_new_link 			= "signup_users.php";
	public $modal						= false;
	
		
	function __construct($signup_id) {
		echo "<h3>Prijavljeni - zamjene</h3>";
		$this->data_url = $this->data_url . "?action=get_list_subs&signup_id={$signup_id}";
		$this->create_new_link = $this->create_new_link . "?signup_id={$signup_id}";
		$this->create_table();
	}
	

	
}






?>