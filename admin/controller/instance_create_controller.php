<?php

class InstanceCreate extends Form {
	
			
			
			private $instance;
			
	//	Construct
		
		function __construct() {
			global $session;
			
			if (isset($_POST['submit'])) {
				
				$remove_submit = array_pop($_POST);
				$attributes = $_POST;
				$this->errors = array();
				foreach ($attributes as $key => $value) {
					$this->$key 		= $value;
				}
				$this->individual_process();	
			}
			
			$this->create_form();	
			
				
		}
		
	
		// Individual process
		
		public function individual_process() {
					 $this->prepare();
					 $this->finalize();
		}
		
		// Prepare
		
		private function prepare() {

			// Fix date for DB
			$this->datum = hr_to_sql($this->datum);
		
		}
		
		// Finalize
		
		private function finalize() {
			$instance = new Instance;
			foreach(Instance::$db_fields as $key) {
				if(!empty($this->$key)) {
					$instance->$key = $this->$key;
				} else {
					$instance->$key = NULL;
				}
			}
			//$user->save();
			if($instance->save()) {
				global $session;
				$session->message("Uspješno", "success");
	 		 	redirect_to("instance_view.php?id=" . $instance->id);
			} else {
				global $session;
				redirect_to("instance_view.php?id=" . $instance->id);
			}
		}
		
		
		// Create form
		
		public function create_form() {
			global $session;
			echo "<h3>Novi događaj</h3>";
			$IDs = Project::find_all();
			$values[] = "";
			foreach ($IDs as $ID) {
				$values[] = $ID->id;
			}
			$names_origin = "name_from_id";
			$this->start_form("instance_create.php");
			$this->add_element_select('project_id', "Projekt &nbsp; &nbsp; &nbsp;", "", $values, "", $names_origin, "Project");
			echo "<br /><br />";
			$this->add_element_select('activity_id', "Aktivnost &nbsp;", "", "", "");
			echo "<br /><br />";
			$this->add_element("ime", "Ime ", "text", "", "", "Ime","", "disabled");
			$this->add_datepicker("datum", "Datum ", "text", "", "", "Datum","datepicker", "disabled");
			$this->add_element("bodovi", "Bodovi ", "number", "", "", "Bodovi", "","disabled");
			$this->add_element_hidden("user_id", $session->user_id);
			$this->add_element_textarea("opis", "Opis", "", "", "Opis", "", "disabled");
			echo "<br /><br />";
			$this->end_form();
		}
		
		
		
		
}





?>