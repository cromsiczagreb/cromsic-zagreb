<?php

class IndividualCreate extends Form {
	
			
			
			private $individual;
			
	//	Construct
		
		function __construct() {
			global $session;
			
			if (isset($_POST['submit'])) {
				
				$remove_submit = array_pop($_POST);
				$attributes = $_POST;
				$this->errors = array();
				foreach ($attributes as $key => $value) {
					$this->$key 		= $value;
				}
				$this->individual_process();	
			}
			
			$this->create_form();	
			
				
		}
		
	
		// Individual process
		
		public function individual_process() {
					 $this->prepare();
					 $this->finalize();
		}
		
		// Prepare
		
		private function prepare() {

		
		}
		
		// Finalize
		
		private function finalize() {
			$individual = new Individual;
			foreach(individual::$db_fields as $key) {
				if(!empty($this->$key)) {
					$individual->$key = $this->$key;
				} else {
					$individual->$key = NULL;
				}
			}
			//$user->save();
			if($individual->save()) {
				global $session;
				$session->message("Uspješno", "success");
	 		 	redirect_to("individual_list.php");
			} else {
				global $session;
				redirect_to("individual_view.php");
			}
		}
		
		
		// Create form
		
		public function create_form() {
			global $session;
			echo "<h3>Pojedinačan unos</h3>";
			$this->start_form("individual_create.php");
			$this->add_element_hidden("user_id",  " \" id=\"user_id\"");
			$this->add_element("član", "Član ", "text", "", "", "Dodaj s liste", "member", "disabled");
			echo "<br/><br/>"; 
			$this->add_element("opis", "Opis ", "text", "", "", "Opis");
			$this->add_element("bodovi", "Bodovi ", "number", "", "", "Bodovi")	;
			echo "<br/><br/>"; 
			$this->add_element_hidden("creator_id",  $session->user_id);
			$this->add_element("napravio", "Napravio ", "text", "", User::name_from_id($session->user_id), "", "disabled");
			echo "<br/><br/>";
			$this->end_form();
		}
		

}


class MemberList extends Controller {
	
	
	
	
	// Table options
	
	public $columns = array('prezime', 'ime', 'jmbag');
	public $data_url 					= "model/member_list.php";
	public $action						= "individual_edit";
	public $action_icon					= "glyphicon-plus";
/*	public $create_new_link             = "";
	public $modal						= false;
	public $checkboxes                  = "no";*/


	
		
	function __construct() {
		echo "<h3>Članovi</h3>";
		$this->create_table();
	}
	
}


?>