<?php

class InstanceView extends Form {
	
			
			private $can_edit = false;
			private $disabled = "disabled";
			private $instance;
			
	//	Construct
		
		function __construct() {
			global $session;
			
			if (!isset($_GET['id'])) {
				redirect_to("../../index.php");	
			}

			if (isset($_POST['submit'])) {
				
				$remove_submit = array_pop($_POST);
				$attributes = $_POST;
				$this->errors = array();
				foreach ($attributes as $key => $value) {
					$this->$key 		= $value;
				}
				$this->individual_process();	
			}
			
			$instance = Instance::find_by_id($_GET['id']);
			if(!is_object($instance)) { redirect_to("instance_list.php") ; }
			if($session->user_type == 4 or $session->user_id == $instance->user_id) {
				 $this->disabled = ""; 
				 $this->can_edit = true;
			}	
			$this->create_form($instance);	
			
						
		}
		
	
		// Individual process
		
		public function individual_process() {
					 $this->prepare();
					 $this->finalize();
		}
		
		// Prepare
		
		private function prepare() {

			// Fix date for DB
			$this->datum = hr_to_sql($this->datum);
		
		}
		
		// Finalize
		
		private function finalize() {
			$instance = new Instance;
			foreach(Instance::$db_fields as $key) {
				if(!empty($this->$key)) {
					$instance->$key = $this->$key;
				} else {
					$instance->$key = NULL;
				}
			}
			//$user->save();
			if($instance->save()) {
				global $session;
				$session->message("Uspješno", "success");
	 		 	redirect_to("instance_view.php?id=" . $instance->id);
			} else {
				global $session;
				redirect_to("instance_view.php?id=" . $instance->id);
			}
		}
		
		
		// Create form
		
		public function create_form($instance) {
			echo "<h3>Događaj</h3>";
			$IDs = Activity::find_all();
			foreach ($IDs as $ID) {
				$values[] = $ID->id;
			}
			$names_origin = "name_from_id";
			$this->start_form("instance_view.php?id=" . $instance->id);
			$this->add_element_select('activity_id', "", $instance->activity_id, $values, $this->disabled, $names_origin, "Activity");
			echo "<br /><br />";
			$this->add_element_hidden("id",  $instance->id);
			$this->add_element("ime", "Ime ", "text", "", $instance->ime, "Ime","", $this->disabled);
			$this->add_element("bodovi", "Bodovi ", "number", "", $instance->bodovi, "Bodovi", "", $this->disabled)	;
			$this->add_datepicker("datum", "Datum ", "text", "", sql_to_hr($instance->datum), "Datum","datepicker", $this->disabled);
			$this->add_element("napravio", "Napravio ", "text", "", User::name_from_id($instance->user_id), "", "", "disabled");
			$this->add_element_textarea("opis", "Opis ", "", "", "","", $this->disabled, $instance->opis );
			echo "<br /><br />";
			$this->end_form_save($instance->id, "instance", $this->can_edit);
		}
		
		
		
		
}


class ParticipantsList extends Controller {
	
	
	
	
	// Table options
	
	public $columns 					= array	('prezime', 'ime', 'jmbag');
	public $data_url 					= "model/participants.php";
	public $action						= "";
	public $action_icon					= "glyphicon-user";
	public $create_new_link 			= "instance_users.php";
	public $modal						= false;
	
		
	function __construct($instance_id) {
		echo "<h3>Sudionici</h3>";
		$this->data_url = $this->data_url . "?action=get_list&instance_id={$instance_id}";
		$this->create_new_link = $this->create_new_link . "?instance_id={$instance_id}";
		$this->create_table();
	}
	

	
}






?>