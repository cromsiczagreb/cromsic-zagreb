<?php



class SignupUsersList extends Controller {
	
	
	
	
	// Table options
	
	public $columns 					= array	('broj', 'prezime', 'ime', 'jmbag','mail', 'mobitel');
	public $data_url 					= "model/signup_users.php";
	public $action						= "";
	public $action_icon					= "glyphicon-trash";
	public $create_new_link             = "#design";
	public $modal						= false;
	public $checkboxes                  = "yes";

	
		
	function __construct() {
		global $session;
		echo "<h3>Prijavljeni</h3>";
		if(!isset($_GET['signup_id'])) { redirect_to ("../../index.php") ; }
		$signup_id = $_GET['signup_id'];
		$signup = Signup::find_by_id($_GET['signup_id']);
		if(($session->user_type != 4) && ($session->user_id != $signup->creator_id)) {
				$session->message("Nemate pravo izmjenjivati ovaj događaj", "info");
				 redirect_to("signup_view.php?id={$signup_id}");
			}	
		
		$this->data_url = $this->data_url . "?action=get_list&signup_id={$signup_id}";
		$this->create_table();
	}
	

	
}



?>