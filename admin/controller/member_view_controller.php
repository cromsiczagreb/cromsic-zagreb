<?php

class MemberView extends Controller {
	
			private $can_edit = false;
			private $disabled = "disabled";
			private $user;
			
	//	Construct
		
		function __construct() {
			global $session;
			
			if (!isset($_GET['id'])) {
				redirect_to("../index.php");
			}
			
			if (isset($_POST['submit'])) {
				$remove_submit = array_pop($_POST);
				$attributes = $_POST;
				$this->errors = array();
				foreach ($attributes as $key => $value) {
					$this->$key 		= $value;
				}
				
				$this->individual_process();
				
			}
			$user = User::find_by_id($_GET['id']);
			if(!is_object($user)) { redirect_to("member_list.php") ; }
			if($session->user_type == 4) {
				 $this->disabled = ""; 
				 $this->can_edit = true;
			}
			
			$this->create_form($user);	
			
		}
		
	
		// Individual process
		
		public function individual_process() {
					 $this->prepare();
					 $this->finalize();
		}
		
		// Prepare
		
		private function prepare() {

			// Fix date for DB
			$this->datum_rod = hr_to_sql($this->datum_rod);
		
		}
		
		// Finalize
		
		private function finalize() {
			$user = new User;
			foreach(User::$db_fields as $key) {
				if(!empty($this->$key)) {
					$user->$key = $this->$key;
				} else {
					$user->$key = NULL;
				}
			}
			//$user->save();
			if($user->save()) {
				global $session;
				$session->message("Uspješno", "success");
	 		 	redirect_to("member_view.php?id=" . $user->id);
			} else {
				global $session;
				redirect_to("member_view.php?id=" . $user->id);
			}
			
		}
	
	
		//	Create form
	
		public function create_form($user) {
			echo "<h3> Član </h3>";
			$this->start_form("member_view.php?id=" . $user->id);
			$values = array (1, 2, 3, 4);
			$this->add_element_hidden("id",  $user->id);
			$this->add_element_select("type", "Grupa ", $user->type, $values, $this->disabled, "translate_type", "User");
			echo "<br /><br />";
			$this->add_element("jmbag", "JMBAG ", "text", true, $user->jmbag, "", $this->disabled);
			$this->add_element("prezime", "Prezime ", "text" , true, $user->prezime, "", $this->disabled);
			echo "<br />";
			$this->add_element("ime", "Ime ", "text", true, $user->ime, "", $this->disabled);
			echo "<br />";
			$this->add_element("mail", "E-mail ", "email" , false, $user->mail, "", $this->disabled);
			echo "<br />";
			$this->add_element("mobitel", "Broj mobitela " , "text" , true, $user->mobitel, "", $this->disabled);
			$this->add_datepicker("datum_rod", "Datum rođenja ", "text", "", sql_to_hr($user->datum_rod), "Datum","datepicker", "");
			echo "<br /><br />";
			$this->add_element("studij" , "Studij", "text", false, $user->studij, "", $this->disabled);
			$this->add_element("godina_sad" , "Godina studija ", "number", true, $user->godina_sad, "", $this->disabled);
			$this->add_element("godina_upis", "Akademska godina upisa ", "text", true, $user->godina_upis, "", $this->disabled);
			echo "<br /><br />";
			$this->add_element("registriran", "Registriran ", "text", true, sql_to_hr_datetime($user->registered), "", "disabled");
			$bodovi = User::count_points($user->id);
			$this->add_element("bodovi", "Bodovi ", "text", true, $bodovi, "", "disabled");
			if( $application =  Application::get_last_exchange($user->id)) {
			$potroseno =  User::deduct_points($user->id, $application->datum);
			$this->add_element("potroseno", "Potrošeni ", "text", true, $potroseno, "", "disabled");
			}
			
		
			echo "<br /><br />";
			$this->end_form($user->id);
			echo "<br /><br /><br />";
		}
		
		// Start form
		
		public function start_form($target){
			echo "<form action=\"{$target}\" id=\"editor\" method=\"post\">";
		}
		
		// End form
	
		public function end_form($id){
			if($this->can_edit) {
				 echo "<input type=\"submit\" name=\"submit\" value=\"Spremi\" />";
				 echo " &nbsp &nbsp &nbsp &nbsp &nbsp";
				 echo "<a href=\"model\delete.php?class=user&id={$id}\" onclick=\"return confirm('Sigurno želite obrisati ovaj unos?')\" >";
				 echo "<i class=\"glyphicon glyphicon-trash\"></i></a>";
			}
			echo "</form>";
		}

		
		// Add element
		
		public function add_element($name, $label, $type, $tooltip=false, $value="", $id="", $disabled="" ) {
		echo $label . "<input type=\"" .$type. "\" name=\"" . $name . "\" required {$disabled} id=\"".$id."\" value=\"". $value."\">";
		}
		
		// Add element hidden
		
		public function add_element_hidden($name, $value) {
		echo "<input type=\"hidden\" name=\"{$name}\" value=\"{$value}\">" ;
		}
		
		// Add select
		
		public function add_element_select($name, $label, $value, $values, $disabled, $names_origin, $class) {
		echo $label . "<select {$disabled} name=\"{$name}\">";
		foreach ($values as $smth) {
			echo "<option value=\"{$smth}\"" ;
			if($smth == $value) { echo "selected " ; } 
			echo "\">" . $class::$names_origin($smth) ."</option>";
		}
		echo "</select>";
		}
		
		public function add_datepicker($name, $label, $type, $tooltip=false, $value="", $placeholder="", $id="", $disabled="" ) {
		echo "<div style=\"position: relative\">";
		echo $label . "<input type=\"" .$type. "\" name=\"" . $name . "\" placeholder=\"{$placeholder}\" required {$disabled} id=\"".$id."\" value=\"".	        $value."\">";
		echo "</div>";
		}
	
	
}

class DocumentPerUser extends Controller {
	
	
	// Table options
	
	public $columns						= array('kategorija', 'opis','bodovi');
	public $data_url 					= "model/document.php";
	public $action						= "";
	//public $create_new_link 			= "instance_list.php";
	public $modal						= false;
	public $action_icon					= "glyphicon-plus";
	
		
	function __construct() {
		echo "<h3> Upjesi </h3>";
		$this->data_url = $this->data_url . "?action=user&user_id={$_GET['id']}";
		$this->create_table();
	}
	
}

class EntriesPerUser extends Controller {
	
	
	// Table options
	
	public $columns						= array('odbor', 'ime','opis','bodovi');
	public $data_url 					= "model/entries.php";
	public $action						= "";
	//public $create_new_link 			= "instance_list.php";
	public $modal						= false;
	public $action_icon					= "glyphicon-plus";
	
		
	function __construct() {
		echo "<h3> Prijavljene aktivnosti </h3>";
		$this->data_url = $this->data_url . "?action=entries_user&user_id={$_GET['id']}";
		$this->create_table();
	}
	
}



class InstancePerUser extends Controller {
	
	
	// Table options
	
	public $columns						= array('ime', 'datum','bodovi');
	public $data_url 					= "model/member_view.php";
	public $action						= "instance_view";
	public $create_new_link 			= "instance_list.php";
	public $modal						= false;
	public $action_icon					= "glyphicon-plus";
	
		
	function __construct() {
		echo "<h3> Sudjelovanja </h3>";
		$this->data_url = $this->data_url . "?action=participations&user_id={$_GET['id']}";
		$this->create_table();
	}
	
}

class IndividualPerUser extends Controller {
	
	
	
	
	// Table options
	
	public $columns						= array('opis', 'datum','bodovi');
	public $data_url 					= "model/member_view.php";
	public $action						= "individual_view";
	public $create_new_link 			= "individual_create.php";
	public $modal						= false;
	public $action_icon					= "glyphicon-plus";
	
		
	function __construct() {
		echo "<h3> Pojedinačni unosi </h3>";
		$this->data_url = $this->data_url . "?action=individual&user_id={$_GET['id']}";
		$this->create_table();
	}
	

}

?>