<?php

class NewsCreate extends Form {

	private $temp_path;
 	public $errors=array();
  
 	protected $upload_errors = array(
	// http://www.php.net/manual/en/features.file-upload.errors.php
	 		UPLOAD_ERR_OK 				=> "Bez grešaka",
	   		UPLOAD_ERR_INI_SIZE  	=> "Veličina datoteke iznad dopuštene.",
	 		UPLOAD_ERR_FORM_SIZE 	=> "Veličina datoteke iznad dopuštene",
	  		UPLOAD_ERR_PARTIAL 		=> "Nepotpuno učitvanje.",
	  		UPLOAD_ERR_NO_FILE 		=> "Nepostojeća datoteka.",
	  		UPLOAD_ERR_NO_TMP_DIR => "No temporary directory.",
	  		UPLOAD_ERR_CANT_WRITE => "Can't write to disk.",
	  		UPLOAD_ERR_EXTENSION 	=> "File upload stopped by extension."
		);
		
		// Create form
		
		function __construct() {
			if (isset($_POST['submit'])) {    
				$remove_submit = array_pop($_POST);
				$attributes = $_POST;
				$this->errors = array();
				foreach ($attributes as $key => $value) {
					$this->$key 		= $value;
				}
				$this->uploaded = $_FILES['file'];
				$this->individual_process();
				
			}
			
	
			$this->create_form();
		}
		
		// Individual process
		
		public function individual_process() {
				$this->validate_file($this->uploaded);
				$this->Validate();
				$this->show_errors($this->errors);
				if(empty($this->errors)) {
					 $this->prepare();
				
				}
		}
		
		// Validate file
		
		private function Validate_file($file) {
		$allowed_mime = array ("image/gif", "image/png", "image/jpeg", "image/bmp");
		if(!$file || empty($file) || !is_array($file)) {
		  // error: nothing uploaded or wrong argument usage
		  $this->errors[] = "Nije uploadana ni jedna datoteka.";
		  return false;
		} 
		if($file['error'] != 0) {
		  // error: report what PHP says went wrong
		  $this->errors[] = $this->upload_errors[$file['error']];
		  return false;
		} 
		if($file['size'] > 3500000) {
		  // Check size
		  $this->errors[] = "Datoteka je iznad dopuštene veličine";
		  return false;
		} 
		if(!(in_array($file['type'] , $allowed_mime))) {
		  $this->errors[] = "Datoteka nije odgovarajućeg tipa.";
		  return false;
		} 
			
	
		$this->temp_path = $file['tmp_name'];
		$this->filename = basename($this->temp_path, ".tmp");
		$path_parts = pathinfo($_FILES["file"]["name"]);
		$extension = $path_parts['extension'];
		$this->filename .= "." . $extension;
		}
		
		// Validate
		
		private function Validate() {
		
	
			
		}
		
		// Prepare
		
		private function prepare() {
			global $session;
			//Check if directory exists
			$this->user_id = $session->user_id;
			$dir_name= md5($session->user_id);
			$dir_path= "../uploads/carousel/";
			$target_path = $dir_path . "/" . $this->filename;
			echo $dir_path;
			if(!file_exists($dir_path))	{
				mkdir($dir_path);				
			}
			
			// Attempt to move the file 
			if(move_uploaded_file($this->temp_path, $target_path)) {
		  	// Success
				// Save a corresponding entry to the database
				 $this->finalize();
			} else {
				// File was not moved.
		    $this->errors[] = "The file upload failed, possibly due to incorrect permissions on the upload folder.";
		    return false;
			}
		
		
		}
		
		// Finalize
		
		private function finalize() {
			$news = new News;
			foreach(News::$db_fields as $key) {
				if(!empty($this->$key)) {
					$news->$key = $this->$key;
				} else {
					$news->$key = NULL;
				}
			}
			//$user->save();
			if($news->save()) {
				global $session;
				$session->message("Uspješno", "success");
	 		 	redirect_to("news_view.php?id=" . $news->id);
			} else {
				global $session;
				$session->message("Neuspješno, pokušajte ponovno.", "warning");
				redirect_to("news_list.php");
			}
		}
		
		
		// Create form
		
		public function create_form() {
			global $session;
			echo "<h3>Nova vijest</h3>";
			$this->start_form("news_create.php", "news_create_form", "enctype=\"multipart/form-data\" novalidate");
			$this->add_element_hidden("MAX_FILE_SIZE", "3500000");
			$this->add_element("naslov", "", "text", "", "", "Naslov","", "");
			echo "<br /><br />";
			$this->add_element_textarea("podnaslov", "", "", "", "Podnaslov", "", "");
			$this->add_element_hidden("creator_id", $session->user_id);
			echo "<br /><br />";
			$this->add_element_textarea("tekst", "Tekst", "", "", "Tekst", "", "class=\"editme\"");
			echo "<br />";
			$this->add_element("file", "Slika", "file", false, "", "datoteka", "", "accept=\".gif,.png,.jpg,.bmp\"");
			echo "<br /><br />";
			$this->end_form();
		}
		
		
		
		
}





?>