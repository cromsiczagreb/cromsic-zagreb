<?php

class SignupCreate extends Form {
	
			
			
			private $signup;
			
	//	Construct
		
		function __construct() {
			global $session;
			
			if (isset($_POST['submit'])) {
				
				$remove_submit = array_pop($_POST);
				$attributes = $_POST;
				$this->errors = array();
				foreach ($attributes as $key => $value) {
					$this->$key 		= $value;
				}
				$this->individual_process();	
			}
			
			$this->create_form();	
			
				
		}
		
	
		// Individual process
		
		public function individual_process() {
					 $this->prepare();
					 $this->finalize();
		}
		
		// Prepare
		
		private function prepare() {

			// Fix date for DB
			$this->datum = hr_to_sql($this->datum);
			$this->prijaveod = hr_to_sql_datetime($this->prijaveod);
			$this->odjavedo = hr_to_sql_datetime($this->odjavedo);
		}
		
		// Finalize
		
		private function finalize() {
			$signup = new Signup;
			foreach(Signup::$db_fields as $key) {
				if(!empty($this->$key)) {
					$signup->$key = $this->$key;
				} else {
					$signup->$key = NULL;
				}
			}
			//$user->save();
			if($signup->save()) {
				global $session;
				$session->message("Uspješno", "success");
	 		 	redirect_to("signup_view.php?id=" . $signup->id);
			} else {
				global $session;
				redirect_to("signup_view.php?id=" . $signup->id);
			}
		}
		
		
		// Create form
		
		public function create_form() {
			global $session;
			echo "<h3>Nove prijave</h3>";
			$IDs = Project::find_all();
			$values[] = "";
			foreach ($IDs as $ID) {
				$values[] = $ID->id;
			}
			$names_origin = "name_from_id";
			$this->start_form("signup_create.php", "", "novalidate");
			$this->add_element_select('project_id', "Projekt &nbsp; &nbsp; &nbsp;", "", $values, "", $names_origin, "Project");
			echo "<br /><br />";
			$this->add_element_select('activity_id', "Aktivnost &nbsp;", "", "", "");
			echo "<br /><br />";
			$this->add_element("ime", "Ime ", "text", "", "", "Ime","", "disabled");
			echo "<br /><br />";
			$this->add_datepicker("datum", "Datum događaja", "text", "", "", "Datum","datepicker", "");
			$this->add_datepicker("prijaveod", "Prijave od", "text", "", "", "Prijave od","datetimepicker", "");
			$this->add_datepicker("odjavedo", "Odjave do", "text", "", "", "Odjave do","datetimepickertwo", "");
			echo "<br /><br />";
			$this->add_element("potrebno", "Potrebno ", "number", "", "", "Potrebno", "","disabled");
			$this->add_element("zamjena", "Zamjena ", "number", "", "", "Zamjena", "","disabled");
			$this->add_element_hidden("creator_id", $session->user_id);
			$this->add_element_textarea("opis", "Opis", "", "", "Opis", "", "");
			echo "<br /><br />";
			$this->end_form();
		}
		
		
		
		
}





?>