<?php

class Doc_CategoriesList extends Controller {
	
	
	
	
	// Table options
	
	public $columns 					= array	('ime', 'bodovi');
	public $data_url 					= "model/doc_category_list.php";
	public $action						= "doc_category_view";
	public $create_new_link 			= "#";
	public $modal						= true;
	public $action_icon					= "glyphicon-plus";
	
		
	function __construct() {
		echo "<h3>Potvrde</h3>";
		$this->create_table();
	}
	

	
}



class Doc_CategoriesForm extends Form {

		
		public $ime;
		public $opis;
		// Create form
		
		function __construct($type, $title) {
			$modal_id	= $type . "_modal";
			$button_id	= $type . "_button";
			$form_id	= $type . "_form";
			$form_type  = $type;
			$this->create_modal_form($modal_id, $title, $button_id, $form_id, $form_type);
		}
		
		public function create_modal_form($modal_id, $modal_title, $button_id, $form_id, $form_type) {
			$this->start_modal($modal_id, $modal_title);
			$this->input_form($form_id, $form_type) ;
			$this->end_modal($button_id);
		}

		// Input form
		
		public function input_form($form_id, $form_type) {
			echo "<div id=\"modal_form\">";
			$this->start_form("#", $form_id);
			$this->add_element_hidden("type", $form_type);
			$this->add_element("ime", "Ime ", "text", $tooltip=false, "", "Ime", "ime")	;
			$this->add_element("bodovi", "Bodovi ", "number", $tooltip=false, "", "Bodovi", "bodovi")	;
			echo "</form></div>";
		}

}




?>
    