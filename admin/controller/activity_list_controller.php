<?php

class ActivityList extends Controller {
	
	
	
	
	// Table options
	
	public $columns 					= array	('ime', 'project_ime', 'opis', 'bodovi');
	public $data_url 					= "model/activity_list.php";
	public $action						= "activity_view";
	public $create_new_link 			= "#";
	public $modal						= true;
	public $action_icon					= "glyphicon-plus";
	
		
	function __construct() {
		echo "<h3>Aktivnosti</h3>";
		$this->create_table();
	}
	

	
}



class ActivityForm extends Form {

		
		public $ime;
		public $opis;
		// Create form
		
		function __construct($type, $title) {
			$modal_id	= $type . "_modal";
			$button_id	= $type . "_button";
			$form_id	= $type . "_form";
			$form_type  = $type;
			$this->create_modal_form($modal_id, $title, $button_id, $form_id, $form_type);
		}
		
		public function create_modal_form($modal_id, $modal_title, $button_id, $form_id, $form_type) {
			$this->start_modal($modal_id, $modal_title);
			$this->input_form($form_id, $form_type) ;
			$this->end_modal($button_id);
		}

		// Input form
		
		public function input_form($form_id, $form_type) {
			$IDs = Project::find_all();
			foreach ($IDs as $ID) {
				$values[] = $ID->id;
			}
			$names_origin = "name_from_id";
			echo "<div id=\"modal_form\">";
			$this->start_form("#", $form_id);
			$this->add_element_hidden("type", $form_type);
			$this->add_element_select('project_id', "", "", $values, "", $names_origin, "Project");
			echo "<br /><br />";
			$this->add_element("ime", "Ime ", "text", $tooltip=false, "", "Ime","ime")	;
			$this->add_element("bodovi", "Bodovi ", "number", $tooltip=false, "", "Bodovi", "bodovi")	;
			$this->add_element_textarea("opis", "Opis ", $tooltip=false, $value="", "Opis", $id="opis", $disabled="" );
			echo "</form></div>";
		}

}




?>
    