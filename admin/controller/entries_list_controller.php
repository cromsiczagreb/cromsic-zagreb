<?php

class EntriesList extends Controller {
	
	
	
	// Table options
	
	public $columns						= array('broj','odbor', 'ime', 'target_ime','opis', 'bodovi', 'approver' );
	public $data_url 					= "model/entries.php";
	public $action						= "admin_verified_entries";
	//public $create_new_link 			= "individual_create.php";
	public $modal						= false;
	public $action_icon					= "glyphicon-plus";
	
		
	function __construct() {
		echo "<h3>Odobrene aktivnosti</h3>";
		$this->data_url = $this->data_url . "?action=get_verified";
		$this->create_table();
	}
	


}

class EntriesDeleteModal extends Form {

		
		// Create form
		
		function __construct($type, $title) {
			$modal_id	= $type . "_modal";
			$button_id	= $type . "_button";
			$form_id	= $type . "_form";
			$form_type  = $type;
			$this->create_modal_form($modal_id, $title, $button_id, $form_id, $form_type);
		}
		
		public function create_modal_form($modal_id, $modal_title, $button_id, $form_id, $form_type) {
			$this->start_modal($modal_id, $modal_title);
			$this->input_form($form_id, $form_type) ;
			$this->end_modal($button_id);
		}

		// Input form
		
		public function input_form($form_id, $form_type) {
			echo "<div id=\"modal_form\">";
			$this->start_form("#", $form_id);
			$this->add_element_hidden("id", 0);
			$this->add_element("razlog", "Razlog ", "text", $tooltip=false, "", "Razlog", "razlog")	;
			echo "</form></div>";
		}

}

?>
    