<?php

class IndividualView extends Form {
	
			
			private $can_edit = false;
			private $disabled = "disabled";
			private $individual;
			
	//	Construct
		
		function __construct() {
			global $session;
			
			if (!isset($_GET['id'])) {
				redirect_to("../index.php");	
			}

			if (isset($_POST['submit'])) {
				
				$remove_submit = array_pop($_POST);
				$attributes = $_POST;
				$this->errors = array();
				foreach ($attributes as $key => $value) {
					$this->$key 		= $value;
				}
				$this->individual_process();	
			}
			
			$individual = Individual::find_by_id($_GET['id']);
			if(!is_object($individual)) { redirect_to("individual_list.php") ; }
			if($session->user_type == 4 or $session->user_id == $individual->creator_id) {
				 $this->disabled = ""; 
				 $this->can_edit = true;
			}	
			$this->create_form($individual);	
			
			
			
		}
		
	
		// Individual process
		
		public function individual_process() {
					 $this->prepare();
					 $this->finalize();
		}
		
		// Prepare
		
		private function prepare() {

				
		}
		
		// Finalize
		
		private function finalize() {
			$individual = new Individual;
			foreach(Individual::$db_fields as $key) {
				if(!empty($this->$key)) {
					$individual->$key = $this->$key;
				} else {
					$individual->$key = NULL;
				}
			}
			//$user->save();
			if($individual->save()) {
				global $session;
				$session->message("Uspješno", "success");
	 		 	redirect_to("individual_view.php?id=" . $individual->id);
			} else {
				global $session;
				redirect_to("individual_view.php?id=" . $individual->id);
			}
		}
		
		
		// Create form
		
		public function create_form($individual) {
			echo "<h3>Pojedinačan unos</h3>";
			$this->start_form("individual_view.php?id=" . $individual->id);
			$this->add_element_hidden("id",  $individual->id);
			$this->add_element_hidden("user_id  \"id=\"user_id\"",  $individual->user_id);
			$this->add_element("član", "Član ", "text", "",  User::name_from_id($individual->user_id), "","member", "disabled");
			echo "<br/><br/>"; 
			$this->add_element("opis", "Opis ", "text", "", $individual->opis, "","", $this->disabled);
			$this->add_element("bodovi", "Bodovi ", "number", "", $individual->bodovi, "","", $this->disabled)	;
			echo "<br/><br/>"; 
			$this->add_datepicker("datum", "Datum ", "text", "", sql_to_hr_datetime($individual->datum), "","datepicker", "disabled");
			$this->add_element("napravio", "Napravio ", "text", "", User::name_from_id($individual->creator_id), "","", "disabled");
			echo "<br/><br/>";
			$this->end_form_save($individual->id, "individual", $this->can_edit);
		}
				
}


class MemberList extends Controller {
	
	
	
	
	// Table options
	
	public $columns = array('prezime', 'ime', 'jmbag');
	public $data_url 					= "model/member_list.php";
	public $action						= "individual_edit";
	public $action_icon					= "glyphicon-plus";
/*	public $create_new_link             = "";
	public $modal						= false;
	public $checkboxes                  = "no";*/


	
		
	function __construct() {
		echo "<h3>Članovi</h3>";
		$this->create_table();
	}
	
}







?>