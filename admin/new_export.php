<?php error_reporting(E_ALL);
ini_set("display_errors", 1); ?>
<?php require("../includes/initialize.php");?>
<?php require("../includes/model.php") ; ?>
<?php require("google_client.php") ; ?>
<?php require("../includes/last_member_export.php") ; ?>
<?php 



// Get users

function get_new_members() {
	
		$odjucer = date("Y-m-d H:i:s",strtotime("-1 day"));
		$users = User::from_date($odjucer);
		foreach ($users as $user) {
			$user->registered = hr_to_export_datetime($user->registered);
			$user->mjesto = "Zagreb";
			$user->trajanje = "6";
			$user->vrsta = "Redovno";
			$user->novi = "Novi";
			$user->datum_rod = hr_to_export($user->datum_rod);
		}
		return $users;
}

$users = get_new_members();


// Create the value range Object
// OFFICAL ID = 134rRAy3Asr51creVMkG0feNhM2oV4HJ4wSpMdH2ylSs
// MY ID = 1W-zYjsHhkSeFzVd4fNcc3LzGL3IpmnBGmtriV3d1Sh4

$spreadsheetId = '134rRAy3Asr51creVMkG0feNhM2oV4HJ4wSpMdH2ylSs';
$valueRange= new Google_Service_Sheets_ValueRange();
$range = "A1:M";

foreach ($users as $user) {

// You need to specify the values you insert
$valueRange->setValues(["values" => [$user->registered, $user->prezime, $user->ime, $user->mail, $user->mobitel, $user->jmbag,$user->datum_rod, $user->mjesto, $user->studij, $user->godina_sad, $user->godina_upis, $user->trajanje, $user->novi]]); // Add two values

// Then you need to add some configuration
$conf = ["valueInputOption" => "USER_ENTERED"];

// Update the spreadsheet
$service->spreadsheets_values->append($spreadsheetId, $range, $valueRange, $conf);

}


?>