<?php require("../includes/initialize.php");?>
<?php if(!$session->is_admin() && $session->user_type == "4" ) { redirect_to("../index.php"); } ?>
<?php


// Resetiraj članstvo

if(isset($_POST['reset'])) {
	if(User::reset_refresh()) {
		$session->message(_("Članstva resetirana."), "success");
	 	redirect_to("maintance.php");
	} else {
		$session->message(_("Greška na poslužitelju. Pokušajte kasnije."));
		redirect_to("settings.php");
	}
	
}


// Obriši dokumente

if(isset($_POST['delete'])) {
		$session->message(_("Obrisano: " . Document::delete_old() ." dokumenata." ), "success");
	 	redirect_to("maintance.php"); ;
}






?>


<?php include('../layout/header.php') ?>

<div id="main">
<div id="navigation">
<?php require("layout/admin_nav.php");?>
</div>
<div id="page">

<div id="new_doc_button">
<?php echo output_message($message); ?>
<form action="maintance.php"  method="post" id="delete">
	<p><strong>Očisti višak dokumenata </strong></p>
	<input type="submit" name="delete" value="Očisti" onclick="return confirm('Sigurno želite obrisati dokumente?')" >
</form>

<form action="maintance.php"  method="post" id="reset">
	<p><strong>Resetiraj članstvo za sve članove</strong></p>
	<input type="submit" name="reset" value="Resetiraj" onclick="return confirm('Sigurno želite resetirati članstvo?')" >
</form>
	

		
	
</div>


</div>   
</div>
</div>
<?php include('../layout/footer.php') ?>