<nav>
<div href="#" class="hamburger">
        <div class="line"></div>
        <div class="line"></div>
        <div class="line"></div>
</div>
<div class="nav">
<ul>
<li><a href="#">Članovi</a>
<ul>
<a href="member_list.php"><li>Svi</li></a>
<a href="member_refreshed.php"><li>Aktivni</li></a>
<?php if($session->user_type > 2) { echo "<a href=\"member_points.php\"><li>Bodovna lista</li></a>";} ?>
<?php if($session->user_type == "4") { echo "<a href=\"member_export.php\"><li>Novi</li></a>" ;} ?>
<?php if($session->user_type == "4") { echo "<a href=\"maintance.php\"><li>Održavanje</li></a>" ;} ?>
</ul>
</li>
<li><a href="#">Unesi</a>
<ul>
<a href="individual_list.php"><li>Pojedinačno</li></a>
<a href="instance_list.php"><li>Događanja</li></a>
<a href="signup_list.php"><li>Prijave</li></a>
<?php if($session->user_type == "4") { echo "<a href=\"news_list.php\"><li>Vijesti</li></a>" ;} ?>
</ul>
</li>
<?php if($session->user_type > 2) { 
echo "
<li><a href=\"#\">Razmjene</a>
<ul>
<a href=\"af_list.php\"><li>Prijavnice</li></a>
<a href=\"document_list.php\"><li>Potvrde</li></a>
<a href=\"entries_list.php\"><li>Aktivnosti</li></a> ";
if($session->user_type == "4") { echo "<a href=\"complaints.php\"><li>Žalbe</li></a>" ;}
echo "</ul>
</li>";
} 

if($session->user_type > 2) { 
echo "
<li><a href=\"#\">Odobri</a>
<ul>
<a href=\"document_verify.php\"><li>Potvrde</li></a>
<a href=\"entries_choose.php\"><li>Aktivnosti</li></a>
<a href=\"entries_rejected.php\"><li>Neodobrene aktivnosti</li></a>
</ul>
</li>";
}

if($session->user_type > 2) { 
echo "
<li><a href=\"#\">Predlošci</a>
<ul>
<a href=\"activity_list.php\"><li>Aktivnosti</li></a>
<a href=\"project_list.php\"><li>Projekti</li></a>
<a href=\"doc_category_list.php\"><li>Potvrde</li></a>
</ul>
</li>";

}?>
<li><a href="../logout.php"><i class="glyphicon glyphicon-off" aria-hidden="true"></i></a>
</ul>
</div>
</nav>