<?php require("../includes/initialize.php");?>
<?php if(!$session->is_admin()) { redirect_to("../index.php"); } ?>
<?php require ("controller/" . ME_MPHP."_controller.php"); ?>

<?php include('../layout/header.php') ?>

<div id="main">
<div id="navigation">
<?php require("layout/admin_nav.php");?>
</div>
<div id="page">
<?php echo output_message($message); ?>

<div id="float_container">
<div id="half_page_div_left">
<?php $list = new MemberView()?>
</div>
<div id="half_page_div_right">
<?php $document = new DocumentPerUser() ?>
</div>
<br /><br /><br /><br />
<div id="half_page_div_right">
<?php $instances = new InstancePerUser() ?>
</div>
<br /><br /><br /><br />
<div id="half_page_div_right">
<?php $individual = new IndividualPerUser() ?>
</div>
<br /><br /><br /><br />
<div id="half_page_div_right">
<?php $entries = new EntriesPerUser() ?>
</div>
</div>
</div>
</div>
<?php include('../layout/footer.php') ?>

<script>add_datepicker('#datepicker')</script>
