<?php require("../includes/initialize.php");?>
<?php if(!$session->is_admin()) { redirect_to("../index.php"); } ?>
<?php require ("controller/" . ME_MPHP."_controller.php"); ?>

<?php include('../layout/header.php') ?>

<div id="main">
<div id="navigation">
<?php require("layout/admin_nav.php");?>
</div>
<div id="page">
<?php echo output_message($message); ?>
<div id="login_form">
<?php $instance_create = new InstanceCreate()?>
</div>
</div>
</div>
<?php include('../layout/footer.php') ?>

<script>add_datepicker_now('#datepicker')</script>
<script>add_listener("instance_create")</script>
