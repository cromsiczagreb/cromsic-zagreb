<?php require("../includes/initialize.php");?>
<?php if(!$session->is_admin()) { redirect_to("../index.php"); } ?>
<?php require ("controller/" . ME_MPHP."_controller.php"); ?>

<?php include('../layout/header.php') ?>

<div id="main">
<div id="navigation">
<?php require("layout/admin_nav.php");?>
</div>
<div id="page">
<?php echo output_message($message); ?>
<div id="float_container">
<div id="half_page_div_left">
<?php $individual_create = new IndividualCreate()?>
</div>
<div id="half_page_div_right">
<?php $members = new MemberList()?>
</div>
</div>
</div>
</div>
<?php include('../layout/footer.php') ?>



