<p>For everything that you upload here, there has to be a valid certificate or any official document. Those activities which do not have a proper document will not be taken into account.
<br>
<p><strong>How to:</strong></p>
<br>
<p>Press "New", select a category for which you will upload the document and in Comment write:</p>
<p><strong>Average</strong> - type in your grade average up to 3 points (e.g. 4.444). You can upload certificate from studomat or screenshot of online studomat which has visible: GPA, name, surname and date.</p>
<p><strong>Foreign Language</strong> 
- except Croatian, English, Serbian, Bosnian and extinct languages. Type the language in the comment.</p>

<p><strong>Standardized Language Test</strong>
 -e.g. toefl, cea, ielts... Comment the language and type of certificate</p>
 <p><strong>Rector's Award</strong> - specify year</p>
 <p><strong>Dean's Award</strong> - specify year</p>
 <p><strong>Scientific Paper</strong> - upload the paper PDF or full citation</p>
 <p><strong>Student Demonstrator</strong> - upload for each subject and each year separately</p>
 <p><strong>Membership in a student association/club/organization</strong> - specify which <br />
 Note! SportMef membership only once, not for each sport differently.</p>
 <p><strong>President of association/club/organization</strong> - specify which</p>
 <p><strong>Vice-president of association/club/organization</strong> - specify which </p>
 <p><strong>University or international competitions</strong> - specify which competition, upload document </p>
 <p>In case of any technical difficulties, contact us at: <strong>ntsd.zagreb@cromsic.hr</strong></p>