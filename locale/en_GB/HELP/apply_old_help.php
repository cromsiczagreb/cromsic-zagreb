<p>Links under the question mark:<p>
<p><strong>Submit</strong> - these are the activities that you have submitted but are waiting for approval by local officers.
<br><strong>Approved</strong> - activities which you had submitted and which have been approved.
<br><strong>Declined</strong> - activities which you had submitted but have been declined and on which grounds. </p>
<p><strong>How to:</strong></p>
<p>Press <i class="glyphicon glyphicon-plus" style="color:#11A8AB"></i> on the top of the first table, select the Committee and then choose Activity which you are submitting, complete the comment and press Save.</p>
<p>For help, you can always check points table <a href="Bodovanje">HERE</a></p>
<p><strong>Care!</strong> Activities which state "Hourly", "Daily", or similar, you have to submit each hour you have done separately! </p>
<p> If you can not find an activity you have done, as Committee choose Ostalo, as Activity Ostalo and type in your activity.</p>
<br />
 <p>In case of any technical difficulties, contact us at: <strong>ntsd.zagreb@cromsic.hr</strong></p>