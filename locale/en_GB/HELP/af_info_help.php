<h2> Read carefully! </h2>
<br  />
<p>Before you start the application process, please check whether all the information on the link <a href="Profil" target="_blank"><strong>Profile</strong></a> (or menu <strong> Settings - Profile</strong>) are correct.  This information will be used during the application process.</p>
<p>Read more about points system here <a href="Bodovanje" target="_blank"><strong>Points</strong></a> (or menu <strong>Exchanges - Points</strong>). Note, due to the way the database works, not everything is translatable, however, the most important parts are.</p>
<br />
<h4>Application process consists of several parts:</h4>
<br />
<h5><strong>Submit activities</strong></h5><p> which you have done in CroMSIC <strong>before 24. 10. 2015.</strong> (after  date, activities will be registered automatically). Additional information on how to submit these activities can be found by clicking on the huge Question Mark at respective page: <a href="PrijaviAktivnosti" target="_blank"><strong>Submit Activities</strong></a> (or menu <strong>Name Surname - Submit Activities</strong>).  </p><br />
<h5><strong>Documents for other activities</strong></h5><p> For other non-CroMSIC activities follow the scoring rules at <a href="Bodovanje" target="_blank">Points</a>. It is required to upload documents which prove these accomplishments at  <a href="Potvrde" target="_blank"><strong>Documents</strong></a> (or menu <strong>Name Surname - Documents</strong>).  </p><br />
<h5><strong>Application form</strong></h5><p>Filling in the application form is a mandatory part of the process and appeals without an application form that has been filled correctly and on time will not be considered! You can fill in this application form by clicking on the button <strong>Application</strong> under these instructions. Application form can be filled before other parts of the application process are complete and every information in it can be edited until the end of the deadline.</p>
<br />
In case of any technical difficulties, contact us at <strong>ntsd.zagreb@cromsic.hr</strong>