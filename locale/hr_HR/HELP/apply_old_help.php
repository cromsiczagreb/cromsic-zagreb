<p>Ispod upitnika nalaze se poveznice:<p>
<p><strong>Prijavi</strong> - ovdje su aktivnosti koje ste prijavili, a čekaju odobrenje lokalnih dužnosnika.
<br><strong>Odobrene</strong> - aktivnosti koje ste prijavili, a koje su odobrene.
<br><strong>Odbijene</strong> - aktivnosti koje ste prijavili, a koje nisu odobrene uz naveden razlog zašto.</p>
<p><strong>Kako prijaviti:</strong></p>
<p>Pritisnite <i class="glyphicon glyphicon-plus" style="color:#11A8AB"></i> na vrhu prve tablice, odaberite odbor zatim aktivnost na kojoj ste sudjelovali, popunite opis (datum/tema/sat...) te pritisnite Spremi </p>
<p>Za lakše snalaženje možete pogledati tablicu <a href="Bodovanje">OVDJE</a></p>
<p><strong>Pripazite!</strong> stvari koje u svojoj napomeni pišu "Po satu" ili slično, prijavljujete svaki odrađeni sat 				  zasebno<p>Ako ne možete pronaći na listi nešto što ste radili, kao odbor odaberite Ostalo, kao aktivnost Ostalo te u opis upišite                  što ste radili i kada. </p>
<br />
<p>U slučaju bilo kakvih poteškoća, javite se na mail <strong>ntsd.zagreb@cromsic.hr</strong></p>