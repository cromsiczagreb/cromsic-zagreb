<p>Pripazite da za SVE što navodite u ovoj cjelini kao dokaz priložite potvrdu, svjedodžbu, certifikat, izvadak i sl.
Izvannastavne aktivnosti i uspjesi kod kojih nedostaje odgovarajući dokaz neće se uzimati u razmatranje pri bodovanju! </p>
<br>
<p><strong>Kako popuniti:</strong></p>
<br>
<p>Pritisnite "Unesi novu", odaberite kategoriju dokumenta kojeg prilažete, u opis pišete sljedeće:</p>
<p><strong>Prosjek</strong> - upišite svoj prosjek na 3 decimale i ništa više (npr: 4.444). Valjanom potvrdom smatra se potvrda sa studomata ili screenshot studomata koji sadržava sljedeće: prosjek, ime, prezime i datum.</p>
<p><strong>Strani jezik</strong> 
- osim hrvatskog, engleskog, srpskog, bosanskog i mrtvih jezika, upišite jezik za koji prilažete diplomu</p>

<p><strong>Standardizirana diploma</strong>
 - toefl, cea, ielts... upišite jezik i vrstu diplome</p>
 <p><strong>Rektorova</strong> - upišite godinu</p>
 <p><strong>Dekanova</strong> - upišite godinu</p>
 <p><strong>Znanestveni rad</strong> - priložite pdf rada i / ili upište puni citat</p>
 <p><strong>Demonstratura</strong> - predaje se po kolegiju i po godini zasebno, npr. ako ste demos iz Anatomije 2 godine, dva puta prijavite Demonstratura - opis: Anatomija 'godina', potvrda slikovno može biti identična ako je za više godina </p>
 <p><strong>Članstvo u studentskoj udruzi/sekciji/organizaciji</strong> - upišite kojoj <br />
 Napomena! Sportske sekcije unositi jednom; potvrdom SportMefa ili voditelja sekcije, svaka dodatna potvrda ne će se bodovati. </p>
 <p><strong>Predsjednik udruge/sekcije/organizacije</strong> - upišite kojoj<br><br><strong>Predsjednik udruge/sekcije/organizacije</strong> - upišite kojoj </p>
 <p><strong>Sveučilišna i međunarodna natjecanja</strong> - upišite natjecanje, priložite diplomu </p>
 <p>U slučaju bilo kakvih poteškoća, javite se na mail <strong>ntsd.zagreb@cromsic.hr</strong></p>