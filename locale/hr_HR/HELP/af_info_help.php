<h2> Pročitati prije popunjavanja! </h2>
<br  />
<p>Prije nego što krenete s popunjavanjem prijavnice, provjerite da su sve informacije na poveznici <a href="Profil" target="_blank"><strong>Profil</strong></a> (ili pod <strong>Postavke - Profil</strong>) točne. Ove informacije koristit će se kasnije u procesu prijave.</p>
<p>Pročitajte sve što se boduje i kako se boduje na poveznici <a href="Bodovanje" target="_blank"><strong>Bodovanje</strong></a> (ili pod <strong>Razmjene - Bodovanje</strong>).</p>
<br />
<h4>Proces prijave sastoji se od nekoliko dijelova:</h4>
<br />
<h5><strong>Prijave aktivnosti</strong></h5><p> koje ste radili u CroMSIC-u <strong>prije 24. 10. 2015.</strong> (nakon ovoga datuma aktivnosti su već unesene u sustav). Dodatne informacije o prijavi aktivnosti možete vidjeti klikom na veliki upitnik na stranici za prijavu aktivnosti:  <a href="PrijaviAktivnosti" target="_blank"><strong>Prijavi aktivnosti</strong></a> (ili pod <strong>Ime Prezime - Prijavi aktivnosti</strong>).  </p><br />
<h5><strong>Potvrde o izvannastavnim aktivnostima</strong></h5><p> Za izvannastavne aktivnosti koje se boduju u skladu s tablicom za <a href="Bodovanje" target="_blank">Bodovanje</a> potrebno je priložiti valjane potvrde inače neće biti priznate. Ovo možete učiniti na  <a href="Potvrde" target="_blank"><strong>Potvrde</strong></a> (ili pod <strong>Ime Prezime - Potvrde</strong>).  </p><br />
<h5><strong>Ispunjavanje prijavnice</strong></h5><p>Ispunjavanje prijavnice neizostavni je dio natječaja te se žalbe bez pravovremeno i točno popunjene prijavnice ne će uzimati u obzir! Istu možete ispuniti klikom na <strong>Prijavi razmjenu</strong> ispod uputa. Prijavnicu možete ispuniti i prije nego što se prijavili sve aktivnosti i dodali sve potvrde, a sve podatke možete mijenjati do isteka natječaja. </p>
<br />
U slučaju bilo kakvih poteškoća, javite se na mail <strong>ntsd.zagreb@cromsic.hr</strong>