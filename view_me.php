<?php require("includes/initialize.php");?>
<?php require ("core/" . ME_MPHP."_controller.php"); ?>
<?php if(!$session->is_logged_in()) { redirect_to("login.php"); } ?>
<?php include('layout/header.php') ?>

<div id="main">
<div id="navigation">
<?php require("layout/navigation.php");?>
</div>
<div id="page">
<?php echo output_message($message); ?>

<div id="bodovi_box">
<span id="moji_bodovi_tekst" ><?php echo _("moji bodovi:"); ?>
    </span>
 <br />
  <span id="bodovi_tekst"><?php echo $bodovi ?></span>
</div>
<?php 
if( $application =  Application::get_last_exchange($session->user_id)) {
		$bodovi =  User::deduct_points($session->user_id, $application->datum);
		echo _("Potrošeno na razmjene: ") .  " " . $bodovi;
		}
$user = User::find_by_id($session->user_id);
echo "<br />";
echo _("Godina studija") . ": " . ($user->godina_sad * 20);
?>


<div class="tablice_bodovi">
<?php $documents = new DocumentPerUser() ?>
</div>

<div class="tablice_bodovi">
<?php $instances = new InstancePerUser() ?>
</div>

<div class="tablice_bodovi">
<?php $individual = new IndividualPerUser() ?>
</div>

<div class="tablice_bodovi">
<?php $entries = new EntriesPerUser() ?>
</div>

</div>
</div>
<?php include('layout/footer.php') ?>

<script>
$( document ).ready(function() {
    console.log( "ready!" );
	$(".fixed-table-toolbar").empty();
	$('.table').bootstrapTable('hideColumn', 'action');
});
</script>
