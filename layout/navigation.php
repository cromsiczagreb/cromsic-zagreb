<nav>
<div href="#" class="hamburger">
        <div class="line"></div>
        <div class="line"></div>
        <div class="line"></div>
</div>
<ul>
<?php 
		
		
		if($session->is_logged_in()){ 
			
			
			if($session->is_refreshed()){
			echo "<a href=\"Dogadanja\"><li>". _("Događanja") . "</li></a>" ;
			echo "<li>". _("Postavke"). "<ul>";
			
			echo "<a href=\"Profil\"><li>". _("Profil") . "</li></a>";
			echo "<a href=\"NovaLozinka\"><li>" . _("Promijeni lozinku") . "</li></a>";
			echo "</ul></li>";

            echo "<li>" . _("Razmjene") . "<ul>";
            echo "<a href=\"Bodovanje\"><li>". _("Bodovanje") . "</li></a>";
            echo "<a href=\"PopisZemalja\"><li>". _("Popis zemalja") . "</li></a>"; 
            echo "<a href=\"PrijaviRazmjenu\"><li>" ._("Prijavi razmjenu") . "</li></a>";
            echo "<a href=\"Zalba\"><li>" . _("Žalba") ."</li></a>";
			
			
			//echo "<li><a href=\"list.php\">Zemlje</a></li>";	
			//echo "<li><a href=\"apply.php\">Prijavi</a></li>";		
			echo "</ul></li>";

			if($session->is_admin()) { echo "<a href=\"admin/index.php\"><li>Admin</li></a>" ; }
			echo "<li>{$session->user_name} {$session->user_surname}<ul>";
			echo "<a href=\"MojiBodovi\"><li>". _("Moji bodovi") ."</li></a>";
			echo "<a href=\"Potvrde\"><li>". _("Potvrde") . "</li></a>";
            echo "<a href=\"PrijaviAktivnosti\"><li>". _("Prijavi aktivnosti") . "</li></a>";
			echo "<a href=\"logout.php\"><li>" . _("Odjava") . "</li></a></ul>";
			} else {
			if(!($_SERVER['PHP_SELF'] == "/refresh.php")) {
			$message['message'] = _("Morate obnoviti članstvo za ovo godinu da bi nastavili koristiti stranicu. To možete učiniti ovdje:")." <a href=\"refresh.php\"> ". _("Obnovi") . "</a>";
			$message['type']= "danger";
			output_message($message);
			}
			}
	
		} else {
		
			echo "<a href=\"Prijava\"><li>". _("Prijava") . "</li></a>" ;
			echo "<a href=\"Registracija\"><li>" . _("Registracija") . "</li></a>";
		}
		 
		
		 if((!isset($_SESSION['language'])) or ($_SESSION['language'] == 'hr_HR')) { 
			 echo "<a href=\"".ME_MPHP.".php?language=en_GB\"><li id=\"navigation_menu\" >English</li></a>";
		 } else {
			 echo "<a href=\"".ME_MPHP.".php?language=hr_HR\"><li id=\"navigation_menu\" >Hrvatski</li></a>";
		 }
		 
	
	?>
</ul></nav>


			