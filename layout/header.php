<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width" />
<title>CroMSIC Zagreb</title>


<link href='https://fonts.googleapis.com/css?family=Open+Sans:600,700|Roboto:400,500&subset=latin,latin-ext' rel='stylesheet' type='text/css'/>

<link href="<?php echo PATH ; ?>/external/bootstrap/bootstrap-datepicker/css/bootstrap-datetimepicker.min.css" media="all" rel="stylesheet" type="text/css" />
<link href="<?php echo PATH ; ?>/external/bootstrap/bootstrap-table-master/bootstrap-table.css" media="all" rel="stylesheet" type="text/css" />
<link href="<?php echo PATH ; ?>/external/bootstrap/css/bootstrap.css" media="all" rel="stylesheet" type="text/css" />
<link href="<?php echo PATH ; ?>/external/bootstrap/css/bootstrap-theme.css" media="all" rel="stylesheet" type="text/css" />
<link href="<?php echo PATH ; ?>/external/jquery_countdown/jquery.countdown.css" media="all" rel="stylesheet" type="text/css" />
<link href="<?php echo PATH ; ?>/resources/css/general.css" media="all" rel="stylesheet" type="text/css" />

<!-- mobile -->
<link href="<?php echo PATH ; ?>/resources/css/mobile.css" rel="stylesheet" type="text/css" media="only screen and (max-width:900px)" />

<!-- IKONE -->

<link rel="apple-touch-icon-precomposed" sizes="57x57" href="<?php echo PATH ; ?>/resources/img/favicomatic/apple-touch-icon-57x57.png" />
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo PATH ; ?>/resources/img/favicomatic/apple-touch-icon-114x114.png" />
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo PATH ; ?>/resources/img/favicomatic/apple-touch-icon-72x72.png" />
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo PATH ; ?>/resources/img/favicomatic/apple-touch-icon-144x144.png" />
<link rel="apple-touch-icon-precomposed" sizes="60x60" href="<?php echo PATH ; ?>/resources/img/favicomatic/apple-touch-icon-60x60.png" />
<link rel="apple-touch-icon-precomposed" sizes="120x120" href="<?php echo PATH ; ?>/resources/img/favicomatic/apple-touch-icon-120x120.png" />
<link rel="apple-touch-icon-precomposed" sizes="76x76" href="<?php echo PATH ; ?>/resources/img/favicomatic/apple-touch-icon-76x76.png" />
<link rel="apple-touch-icon-precomposed" sizes="152x152" href="<?php echo PATH ; ?>/resources/img/favicomatic/apple-touch-icon-152x152.png" />
<link rel="icon" type="image/png" href="<?php echo PATH ; ?>/resources/img/favicomatic/favicon-196x196.png" sizes="196x196" />
<link rel="icon" type="image/png" href="<?php echo PATH ; ?>/resources/img/favicomatic/favicon-96x96.png" sizes="96x96" />
<link rel="icon" type="image/png" href="<?php echo PATH ; ?>/resources/img/favicomatic/favicon-32x32.png" sizes="32x32" />
<link rel="icon" type="image/png" href="<?php echo PATH ; ?>/resources/img/favicomatic/favicon-16x16.png" sizes="16x16" />
<link rel="icon" type="image/png" href="<?php echo PATH ; ?>/resources/img/favicomatic/favicon-128.png" sizes="128x128" />
<meta name="application-name" content="&nbsp;"/>
<meta name="msapplication-TileColor" content="#FFFFFF" />
<meta name="msapplication-TileImage" content="<?php echo PATH ; ?>/resources/img/favicomatic/mstile-144x144.png" />
<meta name="msapplication-square70x70logo" content="<?php echo PATH ; ?>/resources/img/favicomatic/mstile-70x70.png" />
<meta name="msapplication-square150x150logo" content="<?php echo PATH ; ?>/resources/img/favicomatic/mstile-150x150.png" />
<meta name="msapplication-wide310x150logo" content="<?php echo PATH ; ?>/resources/img/favicomatic/mstile-310x150.png" />
<meta name="msapplication-square310x310logo" content="<?php echo PATH ; ?>/resources/img/favicomatic/mstile-310x310.png" />

<!--END IKONE-->

<script type="text/javascript" src="https://code.jquery.com/jquery-latest.min.js"></script>
<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo PATH ; ?>/external/moment/moment.js"></script>
<script type="text/javascript" src="<?php echo PATH ; ?>/external/moment/hr.js"></script>
<script type="text/javascript" src="<?php echo PATH ; ?>/external/bootstrap/bootstrap-datepicker/js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript" src="<?php echo PATH ; ?>/external/bootstrap/bootstrap-table-master/bootstrap-table.min.js"></script>
<script type="text/javascript" src="<?php echo PATH ; ?>/external/bootstrap/bootstrap-table-master/locale/bootstrap-table-hr-HR.min.js"></script>
<script type="text/javascript" src="<?php echo PATH ; ?>/external/bootstrap/bootstrap-table-master/extensions/export/bootstrap-table-export.min.js"></script>
<script type="text/javascript" src="<?php echo PATH ; ?>/external/tableExport/tableExport.min.js"></script>
<script type="text/javascript" src="<?php echo PATH ; ?>/external/tableExport/libs/FileSaver/FileSaver.min.js"></script>
<script type="text/javascript" src="<?php echo PATH ; ?>/external/tableExport/libs/html2canvas/html2canvas.min.js"></script>
<script type="text/javascript" src="<?php echo PATH ; ?>/external/tableExport/libs/jsPDF/jspdf.min.js"></script>
<script type="text/javascript" src="<?php echo PATH ; ?>/external/jquery_countdown/jquery.plugin.min.js"></script>
<script type="text/javascript" src="<?php echo PATH ; ?>/external/jquery_countdown/jquery.countdown.min.js"></script>
<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>

<script type="text/javascript" src="<?php echo PATH ; ?>/resources/js/js.js"></script>
<script type="text/javascript" src="<?php echo PATH ; ?>/resources/js/hamburger.js" defer="defer"></script>


</head>
<body>
<div id="wrapper">
<div id="header">
<a href="<?php echo PATH ; ?>/">
<img alt="CroMSIC" src="<?php echo PATH ; ?>/resources/img/logosivoplavo.png"/>
</a>
<div id="beta_tag">
<span style="font-size:40px; color:#11A0AB">BETA</span><br />
<span style="font-size:14px; color:#11A0AB">in development</span>

</div>
</div>
