<?php require("includes/initialize.php");?>
<?php if(!$session->is_logged_in()) { redirect_to("login.php"); } ?>
<?php require ("core/" . ME_MPHP."_controller.php"); ?>
<?php include('layout/header.php') ?>

<div id="main">
<div id="navigation">
<?php require("layout/navigation.php");?>
</div>
<div id="page">
<?php echo output_message($message); ?>

<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
<?php 	


$projects = Project::find_all() ;
$number = 1;
foreach ($projects as $project) {
	
echo "<div class=\"panel panel-default\">
    <div class=\"panel-heading\" role=\"tab\" id=\"heading{$number}\">
      <h4 class=\"panel-title\">
        <a class=\"collapsed\" role=\"button\" data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapse{$number}\" aria-expanded=\"false\" aria-controls=\"collapse{$number}\">{$project->ime} </a>    
      </h4>
    </div><div id=\"collapse{$number}\" class=\"panel-collapse collapse\" role=\"tabpanel\" aria-labelledby=\"heading{$number}\"> <div class=\"panel-body\">";

$table = new ActivitiesPerProject("{$project->id}");

echo "</div></div></div>";
	++$number;
}	
echo "<br /><br />";
echo "<div class=\"panel panel-default\">
    <div class=\"panel-heading\" role=\"tab\" id=\"heading{$number}\">
      <h4 class=\"panel-title\">
        <a class=\"collapsed\" role=\"button\" data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapse{$number}\" aria-expanded=\"false\" aria-controls=\"collapse{$number}\">". _("Izvannastavne aktivnosti") ." </a>    
      </h4>
    </div><div id=\"collapse{$number}\" class=\"panel-collapse collapse\" role=\"tabpanel\" aria-labelledby=\"heading{$number}\"> <div class=\"panel-body\">";

echo "<table id=\"tablei\"></table>";

echo "</div></div></div>";	
	


?>
</div>
</div>
</div>
<?php include('layout/footer.php') ?>

<script>
$( document ).ready(function() {
    console.log( "ready!" );
	$(".fixed-table-toolbar").empty();
	$('.table').bootstrapTable('hideColumn', 'action');
});
</script>

<script>
$('#tablei').bootstrapTable({
	
    columns: [{
        field: 'ime',
        title: '<?php echo _("Ime"); ?>'
    }, {
		field: 'bodovi',
        title: '<?php echo _("Bodovi");?>'
    }, {
        field: 'opis',
        title: '<?php echo _("Opis");?>'
    }],
    data: [{
        ime: '<?php echo _("Godina studija");?>',
        bodovi: '20',
        opis: '<?php echo _("Godina na studiju puta 20.");?>'
	}, {
		ime: '<?php echo _("Prosjek");?>',
        bodovi: '20',
        opis: '<?php echo _("Prosjek na 3 decimale, boduje se kao prosjek puta 20.");?>'
    }, {
		ime: '<?php echo _("Strani jezik");?>',
        bodovi: '20',
        opis: '<?php echo _("Svaki navedeni strani jezik; ne boduju se hrvatski, engleski, srpski, bosanski i mrtvi jezici.");?>'
    }, {
		ime: '<?php echo _("Standardizirana diploma");?>',
        bodovi: '25',
        opis: '<?php echo _("Svaka navedena diploma za strani jezik; ne boduje se dodatno s kategorijom Strani jezik.");?>'
    }, {
		ime: '<?php echo _("Dekanova");?>',
        bodovi: '20',
        opis: '<?php echo _("Navesti akademsku godinu.");?>'
    }, {
		ime: '<?php echo _("Rektorova");?>',
        bodovi: '30',
        opis: '<?php echo _("Navesti akademsku godinu.");?>'
    }, {
		ime: '<?php echo _("Znanstveni rad CC");?>',
        bodovi: '50',
        opis: '<?php echo _("Znanstveni radovi objavljeni u CC indeksiranim časopisima; navesti puni citat i/ili priložiti rad.");?>'
    }, {
		ime: '<?php echo _("Znanstveni rad non-CC");?>',
        bodovi: '25',
        opis: '<?php echo _("Znanstveni radovi objavljeni u non-CC indeksiranim časopisima; navesti puni citat i/ili priložiti rad.");?>'
    }, {
		ime: '<?php echo _("Znanstveni rad poster");?>',
        bodovi: '20',
        opis: '<?php echo _("Znanstveni rad poster, navesti puni citat i/ili priložiti rad.");?>'
    }, {
		ime: '<?php echo _("Demonstratura");?>',
        bodovi: '10',
        opis: '<?php echo _("Svaki navedeni kolegij za svaku godinu demonstrature od prošle razmjene.");?>'
    }, {
		ime: '<?php echo _("Članstvo u studentskoj udruzi/sekciji/organizaciji");?>',
        bodovi: '10',
        opis: '<?php echo _("Svaka sekcija neovisno o godinama članstva; sportske sekcije boduju se jednom.");?>'
    }, {
		ime: '<?php echo _("Predsjednik udruge/sekcije/organizacije");?>',
        bodovi: '30',
        opis: '<?php echo _("Svaka sekcija neovisno o godinama članstva; sportske sekcije boduju se jednom.");?>'
    }, {
		ime: '<?php echo _("Podpredsjednik udruge/sekcije/organizacije");?>',
        bodovi: '20',
        opis: '<?php echo _("Svaka sekcija neovisno o godinama članstva; sportske sekcije boduju se jednom.");?>'
	}, {
		ime: '<?php echo _("Prvo mjesto na sveučilišnom ili međunarodnom sportskom natjecanju");?>',
        bodovi: '50',
        opis: ''
	}, {
		ime: '<?php echo _("Drugo mjesto na sveučilišnom ili međunarodnom sportskom natjecanju");?>',
        bodovi: '30',
        opis: ''
		}, {
		ime: '<?php echo _("Treće mjesto na sveučilišnom ili međunarodnom sportskom natjecanju");?>',
        bodovi: '20',
        opis: ''
	}]
});
</script>