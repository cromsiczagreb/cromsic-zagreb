<?php require("includes/initialize.php");?>
<?php require ("core/" . ME_MPHP."_controller.php"); ?>
<?php if(!$session->is_logged_in()) { redirect_to("login.php"); } ?>
<?php include('layout/header.php') ?>

<div id="main">
<div id="navigation">
<?php require("layout/navigation.php");?>
</div>
<div id="page">
<?php echo output_message($message); ?>

<div id="float_container">
<div id="half_page_div_left">
<?php $instance_view = new EventView()?>
</div>
<div id="half_page_div_right">
<?php $participants_req = new SignupUsersRequiredList($_GET['id']);?>
</div>

<div id="half_page_div_right" style="padding-top:40px">
<?php $participants_subs = new SignupUsersSubsList($_GET['id']);?>
</div>
</div>


</div>
</div>
<?php include('layout/footer.php') ?>


<script>
$( document ).ready(function() {
    console.log( "ready!" );
    $(".fixed-table-toolbar").empty();
    $('.table').bootstrapTable('hideColumn', 'action');
});
</script>

