<?php


class Participants extends DatabaseObject {
	
	public static $table_name="participants";
	public static $db_fields = array('instance_id', 'user_id');
	
	public $instance_id;
	public $user_id;



	// Participants for instances
	
	public static function for_instance($instance_id) {		
		$sql = "SELECT * FROM participants WHERE instance_id = {$instance_id} "; 
		$IDs = Participants::find_by_sql($sql);
		$users = array();
		foreach ($IDs as $id) {
			$user = User::find_by_id($id->user_id);
			$users[] = $user;
		}
		return $users;
	}
	
	// Remove participants
	
	public static function remove_participants($instance_id, $user_ids) {
		global $db;
		foreach ($user_ids as $user_id) {
		$sql = "DELETE FROM participants WHERE instance_id='{$instance_id}' AND user_id='{$user_id}' ";
		$db->query($sql);
		}
	}
	
	// Add participants
	
	public static function add_participants($instance_id, $user_ids) {
		global $db;
		foreach ($user_ids as $user_id) {
		$sql = "INSERT INTO participants VALUES ( '{$instance_id}' , '{$user_id}' ); ";
		$db->query($sql);
		}
	}

}
?>