<?php


class Doc_Category extends DatabaseObject {
	
	public static $table_name="doc_categories";
	public static $db_fields = array('id', 'ime', 'bodovi');
	
	public $id;
	public $ime;
	public $bodovi;


	// Name from id
	
	public static function name_from_id($id) {
		if(empty($id)) return "";
		$category = Doc_Category::find_by_id($id);
		$category->ime = _("{$category->ime}");
		return $category->ime;
	}
	
	// value from id
	
	public static function value_from_id($id) {
		if(empty($id)) return "";
		$category = Doc_Category::find_by_id($id);
		return $category->bodovi;
	}
	
	
}


?>