<?php


class News extends DatabaseObject {
	
	public static $table_name="news";
	public static $db_fields = array('id', 'filename', 'naslov', 'podnaslov', 'tekst', 'creator_id');
	
	public $id;
	public $filename;
	public $naslov;
	public $podnaslov;
	public $tekst;
	public $creator_id;
	
	
	
	
	
	// Check if user is creator
	
	public static function is_creator($id, $user_id) {	
		$sql = "SELECT * FROM documents WHERE user_id = {$user_id} AND id = {$id}"; 
		if($documents = Document::find_by_sql($sql)) {
		return true;
		} else {
			return false;
		}
	}
	
	// Get filename form id
	
	public static function filename_from_id($id) {
		if(empty($id)) return "";
		$document = Document::find_by_id($id);
		return $document->filename;
	}

}


?>