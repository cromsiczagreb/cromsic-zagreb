<?php

class Controller {
	
	public $columns = array();
		
		
	private $pagination					= "true";
	private $show_pagination_switch		= "true";
	private $page_list					= "[10, 20, 50, 100]";
	private $show_export				= "true";
	private $search						= "true";
	private $show_columns				= "true";
	private $id_field 					= "id";
	private $show_refresh				= "true";
		
	// Create table
	
	public function create_table(){
		$this->set_options();
		$this->create_head();
		
		
	}
	
	// Set options
	
	public function set_options() {
		echo "<table data-toggle=\"table\" ";
		echo "data-id-filed=\"{$this->id_field}\" ";
		echo "data-show-export=\"{$this->show_export}\" ";
		echo "data-export-types=\"['json', 'xml', 'csv', 'sql', 'excel', 'pdf', 'doc', 'png']\" ";
		echo "data-show-pagination-switch=\"{$this->show_pagination_switch}\" ";
		echo "data-pagination?\"{$this->pagination}\" ";
		echo "data-page-list=\"{$this->page_list}\" ";
		echo "data-search=\"{$this->search}\" ";
		echo "data-show-columns=\"{$this->show_columns}\" ";
		echo "data-show-refresh=\"{$this->show_refresh}\" ";
		echo "data-maintain-selected=\"true\" ";
		echo "data-url=\"{$this->data_url}\" ";
		echo ">";
	}
	
	
	// Give head
	
	public function create_head() {
		echo "<thead>";
		if(isset($this->checkboxes)) { echo "<th data-field=\"state\" data-checkbox=\"true\"></th>" ; }
		echo "<th data-field=\"id\" data-visible=\"false\" data-switchable=\"false\">ID</th>";
		foreach ($this->columns as $column) {
		echo "<th data-field=\"$column\" ";
		if(($column == "datum_rod") or ($column == "registered") or ($column == "datum")) { echo "data-sorter=\"dateSorter\" "; }
				echo " data-sortable=\"true\">" . translate_column($column) . "</th>";
		}
		echo "<th data-field=\"action\" data-formatter=\"{$this->action}\" data-events=\"\">".$this->add_action_head()."</th>";
		echo "</thead></table>";
	}
	
	
	// Add action column head 
	
	public function add_action_head() {;
		if(isset($this->create_new_link)) {
		$output  =  "<a class=\"add\" "; 
		if($this->modal) { $output .= "data-toggle=\"modal\" data-target=\"#create_modal\" "; }
		$output .= " href=\"{$this->create_new_link}\" title=\""._("Novo") . "\">";
		$output .=  " <i class=\"glyphicon {$this->action_icon}\"></i></a>";
		return $output;
		}
	}
}


?>