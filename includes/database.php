<?php

class MySQLDatabase {
	
	private $conn;
	public $last_query;
	
	// Construct
	
	function __construct(){
		$this->open_connection();
	}
	
	// Open connection 
	
	public function open_connection() {
		$this->conn = mysqli_connect(DB_SERVER, DB_USER, DB_PASS, DB_NAME);
		$this->conn->set_charset("utf8");
		if (!$this->conn){
			die("Database connection failed: " . mysqli_connect_error($this->conn));
		}
	}
	
	// Close connection
	
	public function close_connection(){
		if(isset($this->conn)){
			mysqli_close($this->conn);
			unset($this->conn);
		}
	}
	
	// Query
	
	public function query($sql) {
		$this->last_query = $sql;
		$result = mysqli_query($this->conn, $sql);
		$this->confirm_query($result);
		return $result;
	}
	
	// Fetch array
	
	public function fetch_array($result_set) {
		return mysqli_fetch_array($result_set);
	}
	
	
	// Num rows
	
	public function num_rows($result_set) {
		return mysqli_num_rows($result_set);
	}
	
	// Last inserted ID
	
	public function insert_id() {
		return mysqli_insert_id($this->conn);
	}
	
	
	// Affected rows
	
	public function affected_rows() {
		return mysqli_affected_rows($this->conn);
	}
	
	
	// Escape values
	
	public function escape_value($value) {
			$this->conn;
			$escaped_string = mysqli_real_escape_string($this->conn, $value);
			return $escaped_string;
		}
		
	// Confirm query
	
	private function confirm_query($result) {
		if(!$result) {
			$output = "Database query failed: " . mysqli_error($this->conn) . "<br /><br />";
			$output .= "Last SQL query: " . $this->last_query;
			die($output);
		}
	}
}


$db = new MySQLDatabase();





?>