<?php


class Signup extends DatabaseObject {
	
	public static $table_name="signups";
	public static $db_fields = array('id', 'activity_id', 'ime', 'potrebno', 'zamjena',  'opis', 'creator_id', 'datum', 'prijaveod', 'odjavedo','special', 'export');
	
	public $id;
	public $activity_id;
	public $ime;
	public $opis;
	public $potrebno;
	public $zamjena;
	public $creator_id;
	public $datum;
	public $prijaveod;
	public $odjavedo;
	public $special;
	public $export;
	
	

	// Instances for participants
	
	public static function for_user($user_id) {		
		$sql = "SELECT * FROM signup_users WHERE user_id = '{$user_id}' "; 
		$IDs = SignupUsers::find_by_sql($sql);
		$instances = array();
		foreach ($IDs as $id) {
			$instance = Instance::find_by_id($id->instance_id);
			$instances[] = $instance;
		}
		return $instances;
	}
	
	// Remove signups
	
	public static function remove_signups($signup_id, $user_ids) {
		global $db;
		foreach ($user_ids as $user_id) {
		$sql = "DELETE FROM participants WHERE signup_id='{$signup_id}' AND user_id='{$user_id}' ";
		$db->query($sql);
		}
	}
	
	// Find future
	
	public static function find_future() {
		$datum = date("Y-m-d");    
		return static::find_by_sql("SELECT * FROM " . static::$table_name . " WHERE datum > '{$datum}' ORDER BY datum");
	}
	
	// Exported true
	
	public static function set_exported($id){
		global $db;
		$id = $db->escape_value($id);
		$exported = $db->escape_value($id);
		$sql = "UPDATE signups SET export=1 WHERE id={$id}";
		if ($db->query($sql)) { return true ;}	
	}
	

	
}


?>