<?php


class Application extends DatabaseObject {
	
	public static $table_name="applications";
	public static $db_fields = array('id', 'user_id', 'type', 'status', 'datum','nacionalnost', 'jezik', 'spol', 'adresa', 'mjesto', 'fiksni', 'razmjena', 'upisiv');
	
	public $id;
	public $user_id;
	public $type;
	public $status;
	public $datum;
	public $nacionalnost;
	public $jezik;
	public $spol;
	public $adresa;
	public $mjesto;
	public $fiksni;
	public $razmjena;
	public $upisiv;
	
	
	
	// Find by user ID
	
	public static function find_by_user_id($user_id){
		global $db;
		$result_array = static::find_by_sql("SELECT * FROM ". static::$table_name ." WHERE user_id={$user_id} AND status < 4 LIMIT 1");
		return !empty($result_array) ? array_shift($result_array) : false;
	}
	
	// Find by type
	
	public static function find_by_type($type) {
		global $db;
		$db->escape_value($type);
		$sql = "SELECT * FROM applications WHERE type = {$type} "; 
	    $applications = Application::find_by_sql($sql);	
		return $applications;
	}
	
	// Find by type
	
	public static function find_by_type_current($type) {
		global $db;
		$db->escape_value($type);
		$sql = "SELECT * FROM applications WHERE type = {$type} AND status < 3 "; 
	    $applications = Application::find_by_sql($sql);	
		return $applications;
	}
	
	// Find archived
	
	public static function find_archive_by_type($type) {
		global $db;
		$db->escape_value($type);
		$sql = "SELECT * FROM applications WHERE type = {$type} AND status > 2 "; 
	    $applications = Application::find_by_sql($sql);	
		return $applications;
	}
	
	// Get last exchange
	
	public static function get_last_exchange($user_id) {
		global $db;
		$result_array = static::find_by_sql("SELECT * FROM ". static::$table_name ." WHERE user_id={$user_id} AND status > 2 ORDER BY datum DESC LIMIT 1");
		return !empty($result_array) ? array_shift($result_array) : false;
	}
	
	


	
	
	
}


?>