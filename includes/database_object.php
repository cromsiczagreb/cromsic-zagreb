<?php

class DatabaseObject {


	public static $table_name;
	public static $db_fields;



// Common Database Methods
	
	// Find all 
	
	public static function find_all() {
		return static::find_by_sql("SELECT * FROM " . static::$table_name );
	}
	
	// Find by ID
	
	public static function find_by_id($id=0){
		global $db;
		$result_array = static::find_by_sql("SELECT * FROM ". static::$table_name ." WHERE id={$id} LIMIT 1");
		return !empty($result_array) ? array_shift($result_array) : false;
	}
	
	// Find  by SQL
	
	public static function find_by_sql($sql) {
		global $db;
		$result_set = $db->query($sql);
		$object_array = array();
		while ($row = $db->fetch_array($result_set)){
			
			$object_array[] = static::instantiate($row);
		}
		
		return $object_array;
		
	}
	
	
		// Count all
	
	public static function count_all() {
		global $db;
		
		$sql = "SELECT COUNT(*) FROM ".static::$table_name;
		$result_set = $db->query($sql);
		
		$row = $db->fetch_array($result_set);
		return array_shift($row);
	}

	// Instantiate an object
	
	private static function instantiate($record) {
		
		$object = new static ;
		
		foreach($record as $attribute=>$value){
			if($object->has_attribute($attribute)) {
				$object->$attribute = $value;
			}
		}
		return $object;
	}
	
	// Has attributes
	
	private function has_attribute($attribute) {
		// get_object_vars returns an associative array with all the attributes
		// (incl. private ones!) as the keys and their current values as the values
		$object_vars = $this->attributes();
		return array_key_exists($attribute, $object_vars);
	}
	
	
	// Attributes - return an array of attribute keys and their values
	
	protected function attributes() {
		$attributes = array();
		foreach(static::$db_fields as $field) {
			if(property_exists($this, $field)) { 
				$attributes[$field] = $this->$field;
			}
		}
		return $attributes;
	}
	
	// Cleaned attributes
	
	protected function cleaned_attributes(){
		global $db;
		$clean_attributes = array();
		
		foreach($this->attributes() as $key => $value){
			if(empty($value)) {
        		unset($this->attributes[$key]);		
			} else {
			$clean_attributes[$key] = $db->escape_value($value);
			}
		}
		return $clean_attributes;
	}
	
	
	// Save - check if create new or update old
	
	public function save() {
		
		return isset($this->id)? $this->update() : $this->create();
		
	}
	// Create new
	
	public function create() {
		global $db;
		$attributes = $this->cleaned_attributes();
		
		$sql  = "INSERT INTO " . static::$table_name . " (";
		$sql .= join(", ", array_keys($attributes));
		$sql .= ") VALUES ('";
		$sql .= join("', '", array_values($attributes));
		$sql .= "')";
		
		if($db->query($sql)) {
			$this->id = $db->insert_id();
			return true;
		} else {
			return false;
		}
		
	}
	
	
	// Update 
	
	public function update() {
		global $db;
		$attributes = $this->cleaned_attributes();
		foreach($attributes as $key => $value) {
			$attribute_pairs[] = "{$key}= '{$value}'";
		}
		
		$sql  = "UPDATE " . static::$table_name . " SET ";
		$sql .= join(", ", $attribute_pairs);
		$sql .= " WHERE id=". $db->escape_value($this->id);
		
		$db->query($sql);
		return ($db->affected_rows() == 1) ? true : false;
		
	}
	
	
	// Delete 
	
	public function delete() {
		global $db;
		
		$sql  = "DELETE FROM " . static::$table_name . " ";
		$sql .= "WHERE id=" .$db->escape_value($this->id);
		$sql .= " LIMIT 1";
		
		$db->query($sql);
		return ($db->affected_rows() == 1) ? true : false;
	}
	
	

}


?>