<?php


class OldEntry extends DatabaseObject {
	
	public static $table_name="old_entries";
	public static $db_fields = array('id', 'user_id', 'project_id', 'activity_id', 'status', 'officer','opis', );
	
	public $id;
	public $user_id;
	public $project_id;
	public $activity_id;
	public $status;
	public $officer;
	public $opis;
	
	


	
	// Get entries for user ID
	
	public static function get_old_entries_for_user($user_id) {
	$sql = "SELECT * FROM old_entries WHERE user_id = {$user_id} "; 
	$IDs = OldEntry::find_by_sql($sql);
	$old_entries = array();
		foreach ($IDs as $id) {
			$old_entry = OldEntry::find_by_id($id->id);
			$old_entries[] = $old_entry;
		}
		return $old_entries;
	}
		
	// Get verified for user ID
	
	public static function get_old_entries_verified_for_user($user_id) {
	$sql = "SELECT * FROM old_entries WHERE user_id = {$user_id} AND status=3 "; 
	$IDs = OldEntry::find_by_sql($sql);
	$old_entries = array();
		foreach ($IDs as $id) {
			$old_entry = OldEntry::find_by_id($id->id);
			$old_entries[] = $old_entry;
		}
		return $old_entries;
	}
	
	
	// Get verified for user ID
	
	public static function get_old_entries_verified_for_user_from_date($user_id, $date) {
	$sql = "SELECT * FROM old_entries WHERE user_id = {$user_id} AND status=3 AND timestamp < '{$date}' "; 
	$IDs = OldEntry::find_by_sql($sql);
	$old_entries = array();
		foreach ($IDs as $id) {
			$old_entry = OldEntry::find_by_id($id->id);
			$old_entries[] = $old_entry;
		}
		return $old_entries;
	}
	
	
	// Get waiting for user ID
	
	public static function get_old_entries_waiting_for_user($user_id) {
	$sql = "SELECT * FROM old_entries WHERE user_id = {$user_id} AND status=1 "; 
	$IDs = OldEntry::find_by_sql($sql);
	$old_entries = array();
		foreach ($IDs as $id) {
			$old_entry = OldEntry::find_by_id($id->id);
			$old_entries[] = $old_entry;
		}
		return $old_entries;
	}
	
	// Get rejected for user ID
	
	public static function get_old_entries_rejected_for_user($user_id) {
	$sql = "SELECT * FROM old_entries WHERE user_id = {$user_id} AND status=2 "; 
	$IDs = OldEntry::find_by_sql($sql);
	$old_entries = array();
		foreach ($IDs as $id) {
			$old_entry = OldEntry::find_by_id($id->id);
			$old_entries[] = $old_entry;
		}
		return $old_entries;
	}
	
	
	// Verififed enties for all users
	
	public static function verified_for_all() {	
		$sql = "SELECT * FROM old_entries WHERE status = 3"; 
		$entries = OldEntry::find_by_sql($sql);
		return $entries;
	}
	
	// Rejected enties for all users
	
	public static function rejected_for_all() {	
		$sql = "SELECT * FROM old_entries WHERE status = 2"; 
		$entries = OldEntry::find_by_sql($sql);
		return $entries;
	}
	
	// Waiting enties for project
	
	public static function waiting_for_project($id) {	
		$sql = "SELECT * FROM old_entries WHERE status = 1 AND project_id='{$id}'"; 
		$entries = OldEntry::find_by_sql($sql);
		return $entries;
	}
	
	
	// Approve
		
	public static function approve($id) {
		global $db;
		global $session;
		
		$sql  = "UPDATE old_entries ";
		$sql .= "SET status=3 , officer={$session->user_id} ";
		$sql .= "WHERE id=". $db->escape_value($id);
		
		$db->query($sql);
		return ($db->affected_rows() == 1) ? true : false;
	}
	
	
}


?>