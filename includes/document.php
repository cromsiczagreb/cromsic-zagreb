<?php


class Document extends DatabaseObject {
	
	public static $table_name="documents";
	public static $db_fields = array('id', 'filename', 'opis', 'status', 'kategorija', 'user_id', 'officer');
	
	public $id;
	public $filename;
	public $opis;
	public $status;
	public $kategorija;
	public $user_id;
	public $officer;
	
	
	// Approve
		
	public static function approve($id) {
		global $db;
		global $session;
		
		$sql  = "UPDATE documents ";
		$sql .= "SET status=1, officer={$session->user_id} ";
		$sql .= "WHERE id=". $db->escape_value($id);
		
		$db->query($sql);
		return ($db->affected_rows() == 1) ? true : false;
	}
	
	// Documents for user
	
	public static function for_user($user_id) {		
		$sql = "SELECT * FROM documents WHERE user_id = {$user_id} "; 
		$documents = Document::find_by_sql($sql);
		return $documents;
	}
	
	
	// Potvrda o prosijeku for user
	
	public static function average_for_user($user_id) {		 
		global $db;
		$result_array = static::find_by_sql("SELECT * FROM documents WHERE user_id = {$user_id} AND kategorija = 1 LIMIT 1");
		return !empty($result_array) ? array_shift($result_array) : false;
	}
	
	// Verififed documents for user
	
	public static function verified_for_user($user_id) {	
		$sql = "SELECT * FROM documents WHERE user_id = {$user_id} AND status = 1"; 
		$documents = Document::find_by_sql($sql);
		return $documents;
	}
	
	// Verififed documents for user from date
	
	public static function verified_for_user_from_date($user_id, $date) {	
		$sql = "SELECT * FROM documents WHERE user_id = {$user_id} AND status = 1 AND timestamp < '{$date}'"; 
		$documents = Document::find_by_sql($sql);
		return $documents;
	}
	
	// Unverififed documents for user
	
	public static function unverified_for_user($user_id) {	
		$sql = "SELECT * FROM documents WHERE user_id = {$user_id} AND status = 0"; 
		$documents = Document::find_by_sql($sql);
		return $documents;
	}
	
	// Verififed documents for all users
	
	public static function verified_for_all() {	
		$sql = "SELECT * FROM documents WHERE status = 1"; 
		$documents = Document::find_by_sql($sql);
		return $documents;
	}
	
	// Unverififed documents for all users
	
	public static function unverified_for_all() {	
		$sql = "SELECT * FROM documents WHERE status = 0"; 
		$documents = Document::find_by_sql($sql);
		return $documents;
	}
	
	
	// Unverififed documents for all users
	
	public static function all_gpa_doc() {	
		$sql = "SELECT * FROM documents WHERE kategorija = 1"; 
		$documents = Document::find_by_sql($sql);
		return $documents;
	}
	
	// Check if user is uploader
	
	public static function is_uploader($id, $user_id) {	
		$sql = "SELECT * FROM documents WHERE user_id = {$user_id} AND id = {$id}"; 
		if($documents = Document::find_by_sql($sql)) {
		return true;
		} else {
			return false;
		}
	}
	
	// Get filename form id
	
	public static function filename_from_id($id) {
		if(empty($id)) return "";
		$document = Document::find_by_id($id);
		$document->filename = _("$document->filename");
		return $document->filename;
	}
	
	// Get user from id
	
	public static function user_from_id($id) {
		if(empty($id)) return "";
		$document = Document::find_by_id($id);
		return $document->user_id;
	}

	
	// Delete old documents
	
	public static function delete_old() {
		$no_doc = 0;
		
		// Delete for 6th year inactive users (probably done)
		
		$users = User::find_all_unrefreshed();
		foreach ($users as $user) {
			// Delete only if 6th year
			if ($user->godina_sad == 6) {
				$documents = Document::for_user($user->id);
				foreach ($documents as $document) {
					$document->delete();
					$file = "../uploads/" . md5($document->user_id) . "/" . $document->filename;
					unlink($file);
					$no_doc = $no_doc + 1;
				}
				$dir = "../uploads/" . md5($user->id) ;
				rmdir($dir);
			}
			
		}
	
		// Delete GPA documents
		
		$gpa_documents = Document::all_gpa_doc();
		foreach ($gpa_documents as $document) {
			$document->delete();
			$file = "../uploads/" . md5($document->user_id) . "/" . $document->filename;
			unlink($file);
			$no_doc = $no_doc + 1;
		}
		
		return $no_doc;

	}
	
	
}


?>