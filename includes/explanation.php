<?php


class Explanation extends DatabaseObject {
	
	public static $table_name="explanations";
	public static $db_fields = array('id', 'entry_id', 'razlog');
	
	public $id;
	public $entry_id;
	public $razlog;
	
	
	
	// Find by entry ID
	
	public static function find_by_entry_id($entry_id){
		global $db;
		$result_array = static::find_by_sql("SELECT * FROM ". static::$table_name ." WHERE entry_id={$entry_id} LIMIT 1");
		return !empty($result_array) ? array_shift($result_array) : false;
	}
	
	// Get explanation
	
	public static function get_explanation($id) {
		if(empty($id)) return "";
		$explanation = Explanation::find_by_entry_id($id);
		return $explanation->razlog;
	}


	
	
	
}


?>