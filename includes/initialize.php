<?php error_reporting(E_ALL);
ini_set("display_errors", 1); ?>
<?php

// DIRECTORY_SEPERATOR
defined('DS') ? null : define('DS', DIRECTORY_SEPARATOR);


if (!isset($_SERVER['HTTPS']) || $_SERVER['HTTPS'] != 'on') {
    $protocol = "http://";
} else {
	$protocol = "https://";
}

defined('PATH') ? null : define('PATH', $protocol . $_SERVER['SERVER_NAME']);


defined('SITE_ROOT') ? null :
	define('SITE_ROOT', $_SERVER['DOCUMENT_ROOT']);
	
defined('LIB_PATH') ? null: define('LIB_PATH', SITE_ROOT.DS.'includes');
defined('LIB_PATH_EXT') ? null: define('LIB_PATH_EXT', SITE_ROOT.DS.'external');
defined('ME_MPHP') ? null : define('ME_MPHP', basename($_SERVER['PHP_SELF'], ".php"));


// Load config file
require(LIB_PATH.DS."config.php");

// Load basic functions
require(LIB_PATH.DS."functions.php");

// Load core objects
require(LIB_PATH.DS."session.php");
//require(LIB_PATH.DS."language.php"); Moved downside for DB change
require(LIB_PATH.DS."database.php");
require(LIB_PATH.DS."database_object.php");
require(LIB_PATH.DS."controller.php");


// Load helpers

require(LIB_PATH.DS."form.php");


// Load classes

require(LIB_PATH.DS."user.php");
require(LIB_PATH.DS."project.php");
require(LIB_PATH.DS."activity.php");
require(LIB_PATH.DS."doc_category.php");
require(LIB_PATH.DS."instance.php");
require(LIB_PATH.DS."participants.php");
require(LIB_PATH.DS."individual.php");
require(LIB_PATH.DS."document.php");
require(LIB_PATH.DS."news.php");
require(LIB_PATH.DS."signup.php");
require(LIB_PATH.DS."signup_users.php");
require(LIB_PATH.DS."old_entries.php");
require(LIB_PATH.DS."explanation.php");
require(LIB_PATH.DS."application.php");
require(LIB_PATH.DS."complaint.php");

// Load language
require(LIB_PATH.DS."language.php");

// Load external

require(LIB_PATH_EXT.DS."phpMailer".DS."PHPMailerAutoload.php");
ob_start();
?>