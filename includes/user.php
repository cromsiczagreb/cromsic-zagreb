<?php


class User extends DatabaseObject {
	
	public static $table_name="users";
	public static $db_fields = array('id', 'type', 'jmbag', 'prezime', 'ime', 'mail', 'mobitel', 'datum_rod', 'studij', 'godina_sad', 'godina_upis' 		, 'trajanje', 'jezik', 'obnovljen','registered','password');
	
	public $id;
	public $type;
	public $jmbag;
	public $prezime;
	public $ime;
	public $mail;
	public $mobitel;
	public $datum_rod;
	public $studij;
	public $godina_sad;
	public $godina_upis;
	public $trajanje;
	public $jezik;
	public $obnovljen;
	public $registered;
	public $password;
	
	
	
	// Make full name
	
	public function full_name(){
		if(isset($this->ime) && isset($this->prezime)) {
				return $this->ime . " " . $this->prezime;
		} else {
			return "";
		}
	}
	
	// Translate user type

	public static function translate_type($type) {
	
		switch($type) {
			case "1": return "Članovi";
			case "2": return "Asistenti";
			case "3": return "Vodstvo";
			case "4": return "Admin";
		}
	}
	
	
	// Authenticate
	
	public static function authenticate($username="", $password="") {
		global $db;
		
		$username = $db->escape_value($username);	
		$password = $db->escape_value($password);	
		

		$user = User::find_by_jmbag($username);
		
		if ($user) {
			if (password_verify($password, $user['password'])) {
				return $user;
			} else {
				return false;
			}
		} else {
			return false;
		} 
		
		
	}
	
	
	// Confirm password
	
	public static function confirm_password($id="", $password="") {
		global $db;
		
		$id = $db->escape_value($id);	
		$password = $db->escape_value($password);	
		

		$user = User::find_by_id($id);
		
		if ($user) {
			if (password_verify($password, $user->password)) {
				return $user;
			} else {
				return false;
			}
		} else {
			return false;
		} 
		
		
	}
	
	
	// Find by JMBAG
	
	public static function find_by_jmbag($jmbag=0){
		global $db;
		$jmbag = $db->escape_value($jmbag);	
		$result_array = $db->query("SELECT * FROM ". self::$table_name ." WHERE jmbag='{$jmbag}' LIMIT 1");
		$user_exists = $db->fetch_array($result_array);
		if($user_exists) {
			return $user_exists;
		} else {
		return null;
		}
	}
	
	// Find all refreshed
	
	
	public static function find_all_refreshed() {
		return static::find_by_sql("SELECT * FROM " . static::$table_name . " WHERE obnovljen=1");
	}
	
	
	// Find all unrefreshed
	
	
	public static function find_all_unrefreshed() {
		return static::find_by_sql("SELECT * FROM " . static::$table_name . " WHERE obnovljen=0");
	}
	
	
	// Reset refresh
	
	
	public static function reset_refresh() {
		global $db;
		$sql = "UPDATE users SET obnovljen=0";
		if ($db->query($sql)) { return true ;}
	}
	
	
	// Find all 
	
	
	public static function find_all() {
		return static::find_by_sql("SELECT * FROM " . static::$table_name);
	}
	
	// Count points for user
	
	public static function count_points($id) {
		
		$bodovi = 30;
		
		
		
		$bodovi = $bodovi + User::count_from_year($id);
		
		$bodovi = $bodovi + User::count_from_instances($id);
		
		$bodovi = $bodovi + User::count_from_individual($id);
		
		$bodovi = $bodovi + User::count_from_documents($id);
				
		$bodovi = $bodovi + User::count_from_entries($id);
		
		
		if( $application =  Application::get_last_exchange($id)) {
		$bodovi = $bodovi - User::deduct_points($id, $application->datum);
		}
		
		return $bodovi;

	}
	
	// Count from year
	
	public static function count_from_year($id) {
		$bodovi = User::points_from_year($id);
		return $bodovi;
	}
	
	// Count from instances
	
	public static function count_from_instances($id) {
		$instances = Instance::for_user($id);
		$bodovi = 0;
		
		foreach ($instances as $instance) {
			$bodovi = $bodovi + $instance->bodovi;
		}
		
		return $bodovi;
	}
	
	// Count from individual
	
	public static function count_from_individual($id) {
		$individuals = Individual::for_user($id);
		$bodovi = 0;
		
		foreach ($individuals as $individual) {
			$bodovi = $bodovi + $individual->bodovi;
		}
		
		return $bodovi;
	}
	
	// Count from verified documents
	
	public static function count_from_documents($id) {
		$documents = Document::verified_for_user($id);
		$bodovi = 0;
		
		foreach ($documents as $document) {
			if($document->kategorija == 1) {
				$bodovi = $bodovi + ($document->opis * Doc_Category::value_from_id($document->kategorija));
			} else {
				$bodovi = $bodovi + Doc_Category::value_from_id($document->kategorija);
			}
		}
		
		return $bodovi;
	}
	
	// Count for general points
	
	public static function count_from_general($id) {
		$bodovi = User::count_from_year($id) + User::count_from_documents($id);
		return $bodovi;
	}
	
	// Count for activities
	
	public static function count_from_activities($id) {
		$bodovi = User::count_from_instances($id) + User::count_from_individual($id) + User::count_from_entries($id) + 30;
		return $bodovi;
	}
	
	
	// Count from verified entries
	
	public static function count_from_entries($id) {
		$entries = OldEntry::get_old_entries_verified_for_user($id);
		$bodovi = 0;
		
		foreach ($entries as $entry) {
			$bodovi = $bodovi + Activity::points_from_id($entry->activity_id);
		}
		
		return $bodovi;
	}
	
	
	
	// Count dedactable points
	
	public static function deduct_points($id, $date) {

		$bodovi = 0;
		
		$bodovi = $bodovi + User::deduct_from_instances($id, $date);

		
		$bodovi = $bodovi + User::deduct_from_individual($id, $date);
		
		$bodovi = $bodovi + User::deduct_from_entries($id, $date);
		
		$bodovi = $bodovi + User::deduct_from_documents($id, $date);
				
		
		
		return $bodovi;

	}
	
	// Deduct from instances
	
	public static function deduct_from_instances($id, $date) {
		$instances = Instance::for_user_from_date($id, $date);
		$bodovi = 0;
		
		foreach ($instances as $instance) {
			$bodovi = $bodovi + $instance->bodovi;
		}
		
		return $bodovi;
	}
	
	
	// Deduct from individual
	
	public static function deduct_from_individual($id, $date) {
		$individuals = Individual::for_user_from_date($id, $date);
		
		$bodovi = 0;
		
		foreach ($individuals as $individual) {
			$bodovi = $bodovi + $individual->bodovi;
		}
		return $bodovi;
	}
	
	
	// Deduct from verified entries
	
	public static function deduct_from_entries($id, $date) {
		$entries = OldEntry::get_old_entries_verified_for_user_from_date($id, $date);
		$bodovi = 0;
		
		foreach ($entries as $entry) {
			$bodovi = $bodovi + Activity::points_from_id($entry->activity_id);
		}
		
		return $bodovi;
	}
	
	
	// Deduct from verified documents
	
	public static function deduct_from_documents($id, $date) {
		$documents = Document::verified_for_user_from_date($id, $date);
		$nerebodovano = array(12, 16, 17, 18);
		$bodovi = 0;
		
		foreach ($documents as $document) {
			if($document->kategorija == 1) {
				$bodovi = $bodovi + ($document->opis * Doc_Category::value_from_id($document->kategorija));
			} else {
				if (in_array($document->kategorija, $nerebodovano)){
					$bodovi = $bodovi + Doc_Category::value_from_id($document->kategorija);
				} else {
					continue;
				}
			}
		}
		return $bodovi;
	}
	
	
	//Change password
	
	public static function change_password($id, $password) {
		global $db;
		$id = $db->escape_value($id);
		$password = $db->escape_value($password);
		$password = password_hash($password, PASSWORD_DEFAULT);
		$sql = "UPDATE users SET password='{$password}' WHERE id={$id}";
		if ($db->query($sql)) { return true ;}
	}
	
	
	//Change language
	
	public static function change_language($id, $language) {
		global $db;
		$id = $db->escape_value($id);
		$language = $db->escape_value($language);
		$sql = "UPDATE users SET jezik='{$language}' WHERE id={$id}";
		if ($db->query($sql)) { return true ;}
	}
	
	
	// Get user name
	
	public static function name_from_id($user_id) {
		$user = User::find_by_id($user_id);
		return $user->full_name();
	}
	
	// Get points from year
	public static function points_from_year($user_id) {
		$user = User::find_by_id($user_id);
		$points = $user->godina_sad * 20;
		return $points;
	}
	
	// Get average
	
	public static function get_average($user_id) {
		$document = Document::average_for_user($user_id);
		if($document) { return $document->opis; } else { return _("Potvrda nije unesena") ;}
	}
	
	// Find from date
	
	public static function from_date($date) {
		return static::find_by_sql("SELECT * FROM " . static::$table_name . " WHERE registered > '{$date}'");
	}
	
	// Has ongoing complaint
	
	public static function has_complaint($user_id) {
		global $db;
		$db->escape_value($user_id);
		$result_array = static::find_by_sql("SELECT * FROM complaints WHERE user_id={$user_id} AND status=1 LIMIT 1");
		return !empty($result_array) ? true : false;	
	}
	
	// Is refreshed
	
	public static function is_refreshed($user_id) {
		global $db;
		$db->escape_value($user_id);
		$result_array = static::find_by_sql("SELECT * FROM users WHERE user_id={$user_id} AND obnovljen=1 LIMIT 1");
		return !empty($result_array) ? true : false;	
	}
	
	
	
}


?>