<?php


class Activity extends DatabaseObject {
	
	public static $table_name="activities";
	public static $db_fields = array('id', 'project_id', 'ime', 'bodovi', 'opis');
	
	public $id;
	public $project_id;
	public $ime;
	public $bodovi;
	public $opis;
	
	
	// Name from id
	
	public static function name_from_id($id) {
		if(empty($id)) return "";
		$activity = Activity::find_by_id($id);
		return $activity->ime;
	}
	
	// Points from id
	
	public static function points_from_id($id) {
		if(empty($id)) return "";
		$activity = Activity::find_by_id($id);
		return $activity->bodovi;
	}

	
	// Get activities for project ID
	
	public static function get_activities($id) {
	$sql = "SELECT * FROM activities WHERE project_id = {$id} "; 
	$IDs = Activity::find_by_sql($sql);
	$activities = array();
		foreach ($IDs as $id) {
			$activity = Activity::find_by_id($id->id);
			$activities[] = $activity;
		}
		return $activities;
	}
		

	
	
	
	
	
	
}


?>