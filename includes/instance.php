<?php


class Instance extends DatabaseObject {
	
	public static $table_name="instances";
	public static $db_fields = array('id', 'activity_id', 'ime', 'bodovi', 'opis', 'user_id', 'datum');
	
	public $id;
	public $activity_id;
	public $ime;
	public $opis;
	public $bodovi;
	public $user_id;
	public $datum;
	
	

	// Instances for participants
	
	public static function for_user($user_id) {		
		$sql = "SELECT * FROM participants WHERE user_id = {$user_id} "; 
		$IDs = Participants::find_by_sql($sql);
		$instances = array();
		foreach ($IDs as $id) {
			$instance = Instance::find_by_id($id->instance_id);
			$instances[] = $instance;
		}
		return $instances;
	}
	
	// Instances for participants from date
	
	public static function for_user_from_date($user_id, $date) {	
		$sql = "SELECT * FROM participants WHERE user_id = {$user_id}"; 
		$IDs = Participants::find_by_sql($sql);
		$instances = array();
		foreach ($IDs as $id) {
			$instance = Instance::find_by_id($id->instance_id);
			if ($instance->datum < $date) {
				$instances[] = $instance;
			}
		}
		return $instances;
	}
	
	
	
}


?>