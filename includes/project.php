<?php


class Project extends DatabaseObject {
	
	public static $table_name="projects";
	public static $db_fields = array('id', 'ime', 'opis');
	
	public $id;
	public $ime;
	public $opis;


	// Name from id
	
	public static function name_from_id($id) {
		if(empty($id)) return "";
		$project = Project::find_by_id($id);
		return $project->ime;
	}
	
	
}


?>