<?php


// If no language set, it's Croatian
if(!isset($_SESSION['language'])) {
		$lang = 'hr_HR';
} else {
	$lang = $_SESSION['language'];
}



// Check if language was changed
if (isset($_GET['language'])) {
	
	if(!language_exists($_GET['language'])) {
		$lang= "hr_HR";
		$_SESSION['language'] ='hr_HR';
	} else {
		$lang= $_GET['language'];
		$_SESSION['language'] =$lang;
		if($session->is_logged_in()){
		User::change_language($session->user_id, $lang);
		}
		redirect_to(ME_MPHP . ".php");
	}
}

putenv('LANG=' . $lang);
setlocale(LC_ALL, $lang);

// Set the text domain as 'messages'
$domain = 'messages';
$localedir = SITE_ROOT .DS.'locale';

bindtextdomain($domain, $localedir);
textdomain($domain);

?>