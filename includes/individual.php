
<?php


class Individual extends DatabaseObject {
	
	public static $table_name="individual";
	public static $db_fields = array('id', 'opis', 'bodovi', 'datum', 'user_id', 'creator_id');
	
	public $id;
	public $opis;
	public $bodovi;
	public $user_id;
	public $datum;
	public $creator_id;	
	

	// Instances for participants
	
	public static function for_user($user_id) {		
		$sql = "SELECT * FROM individual WHERE user_id = {$user_id} "; 
		$individuals = Individual::find_by_sql($sql);
		return $individuals;
	}	
	
	// Instances for participants from date
	
	public static function for_user_from_date($user_id, $date) {		
		$sql = "SELECT * FROM individual WHERE user_id = {$user_id} AND datum < '{$date}'"; 
		$individuals = Individual::find_by_sql($sql);
		return $individuals;
	}	
	
}


?>