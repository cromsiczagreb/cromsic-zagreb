<?php

class Form {
	
		public $errors;

		//	Construct
		
		function __construct() {
			if (isset($_POST['submit'])) {    
				$remove_submit = array_pop($_POST);
				$attributes = $_POST;
				$this->errors = array();
				foreach ($attributes as $key => $value) {
					$this->$key 		= $value;
				}
				
				$this->individual_process();
				
			}
		
		
			$this->create_form();	
			
		}

		// Start form
		
		public function start_form($target="", $id="", $enctype=""){
			echo "<form action=\"{$target}\" method=\"post\" {$enctype} id=\"{$id}\">";
		}
		
		// End form
		
		public function end_form($label="Submit"){
			echo "<input type=\"submit\" name=\"submit\" value=\"{$label}\" /></form>";
		}
		
		// End form save
	
		public function end_form_save($id, $class, $can_edit){
			if($can_edit) {
				 echo "<input type=\"submit\" name=\"submit\" value=\"Spremi\" />";
				 echo " &nbsp &nbsp &nbsp &nbsp &nbsp";
				 echo "<a href=\"model/delete.php?class={$class}&id={$id}\" onclick=\"return confirm('Sigurno želite obrisati ovaj unos?')\" >";
				 echo "<i class=\"glyphicon glyphicon-trash\"></i></a>";
			}
			echo "</form>";
		}

		
		// Add element
		
		public function add_element($name, $label, $type, $tooltip=false, $value="", $placeholder="", $id="", $disabled="" ) {
		echo $label . $this->add_tooltip($name, $tooltip) ."<input type=\"" .$type. "\" name=\"" . $name . "\" placeholder=\"{$placeholder}\" required {$disabled} id=\"".$id."\" value=\"".	        $value."\">";
		}
		
	
		//Add checkbox
		
		public function add_checkbox($name, $label, $checked, $tooltip=false, $value="", $placeholder="", $id="", $disabled="" ) 
		{
		echo "<input type=\"checkbox\" ".$checked." name=\"".$name."\" placeholder=\"{$placeholder}\"  {$disabled} id=\"".$id."\" value=\"".$value."\">" . "&emsp;" . $label;
		}
	
		// Add datepicker box
		
		public function add_datepicker($name, $label, $type, $tooltip=false, $value="", $placeholder="", $id="", $disabled="" ) {
		echo "<div style=\"position: relative\">";
		echo $label . $this->add_tooltip($name, $tooltip) ."<input type=\"" .$type. "\" name=\"" . $name . "\" placeholder=\"{$placeholder}\" required {$disabled} id=\"".$id."\" value=\"".	        $value."\">";
		echo "</div>";
		}
		
		// Add textbox
		
		public function add_element_textarea($name, $label="", $tooltip=false, $value="", $placeholder="", $id="", $disabled="" , $text="" ) {
		echo $label . "<textarea rows=\"5\" cols=\"80\" name=\"{$name}\" placeholder=\"{$placeholder}\" required {$disabled} id=\"{$id}\" value=\"{$value}\">{$text}</textarea>";
		}
		
		// Add element hidden
		
		public function add_element_hidden($name, $value) {
		echo "<input type=\"hidden\" name=\"{$name}\" value=\"{$value}\">" ;
		}
		
		// Add tooltip
		
		private function add_tooltip($element, $tooltip=false) {
			if($tooltip == false) { return null ; }
			$tooltip_text = $this->get_tooltip($element);
			$tooltip = "<i id=\"tooltip_icon\" style=\"margin-left:10px\" class=\"glyphicon glyphicon-question-sign\" data-toggle=\"tooltip\" data-placement=\"right\" title=\"" . $tooltip_text ."\" ></i> ";
			
			return $tooltip;
		}
		
		


		// Get text for tooltip - UPDATE HERE
		
		private function get_tooltip($element) {
			switch($element) {
				case "ime": return _("Koristite hrvatske znakove");
				case "prezime": return _("Koristite hrvatske znakove");
				case "jmbag": return _("Jedinstveni matični broj akademskog građana - možete ga pronaći na prvoj strani indeksa.") ;
				case "mobitel": return _("Format: 0991234567");
				case "godina_sad": return _("Navesti brojem 1-6 (u slučaju parcijalne godine, upisujete nižu godinu)");
				case "godina_upis": return _("Format: 2010/2011");
			}
		}
		
		// Show errors
		
		public function show_errors($errors=array()) {
		$output = "";
		if (!empty($errors)) {
			$output = "<div class=\"alert alert-danger alert-dismissible\" role=\"alert\">
  		<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>";
			foreach ($errors as $key => $error) {
			$output .= "      $error <br />";
			}
		$output .= "</div>";
		} 
		echo $output;
		}	
		
		
		// Create modal - start
		
		public function start_modal($modal_id, $modal_title) {
			echo "<div class=\"modal fade\" id=\"{$modal_id}\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
  				 <div class=\"modal-dialog\"><div class=\"modal-content\"><div class=\"modal-header\"><button type=\"button\" class=\"close\" 			                 data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button><h4 class=\"modal-title\"                		                 id=\"myModalLabel\">{$modal_title}</h4></div><div class=\"modal-body\">";
		}
		
		// Create modal - big - start
		
		
		public function start_modal_large($modal_id, $modal_title) {
			echo "<div class=\"modal fade\" id=\"{$modal_id}\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
  				 <div class=\"modal-dialog\"><div class=\"modal-content\"><div class=\"modal-header\"><button type=\"button\" class=\"close\" 			                 data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button><h4 class=\"modal-title\"                		                 id=\"myModalLabel\">{$modal_title}</h4></div><div class=\"modal-body\">";
		}

		
		
		
		
		
		// Create modal - end
		
		public function end_modal($button_id) {
			echo "</div><div class=\"modal-footer\">
                 <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">"._("Zatvori")."</button>
        		 <button type=\"button\" class=\"btn btn-primary\" id=\"{$button_id}\">"._("Spremi")."</button>
      			</div></div></div>></div></div></div>";	
		}
		
		// Create modal - end - buttonless
		
		public function end_modal_buttonless() {
			echo "</div></div></div></div></div>";	
		}
		
		
		// Add select
		
		public function add_element_select($name, $label="", $value="", $values="", $disabled="", $names_origin="", $class="") {
		echo $label . "<select {$disabled} name=\"{$name}\">";
		if (!empty($values)) {
			foreach ($values as $smth) {
				echo "<option value=\"{$smth}\"" ;
				if($smth == $value) { echo "selected " ; } 
				echo "\">" . $class::$names_origin($smth) ."</option>";
			}
		}
		echo "</select>";
		}

		// Add file holder
		
		public function add_element_upload($name, $label, $type, $tooltip=false, $value="", $id="", $disabled="" ) {
		echo $label . "<input type=\"file\" name=\"file_upload\" />";
		}
		
}