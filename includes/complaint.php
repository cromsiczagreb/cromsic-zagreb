<?php


class Complaint extends DatabaseObject {
	
	public static $table_name="complaints";
	public static $db_fields = array('id', 'user_id', 'content', 'status');
	
	public $id;
	public $user_id;
	public $content;
	public $status;
	public $officer;
	
	
	
	// Find by user ID
	
	public static function find_by_user_id($user_id){
		global $db;
		$result_array = static::find_by_sql("SELECT * FROM ". static::$table_name ." WHERE user_id={$user_id} LIMIT 1");
		return !empty($result_array) ? array_shift($result_array) : false;
	}
	
	// Approve
		
	public static function approve($id) {
		global $db;
		global $session;
		
		$sql  = "UPDATE complaints ";
		$sql .= "SET status=1 ";
		$sql .= "WHERE id=". $db->escape_value($id);
		
		$db->query($sql);
		return ($db->affected_rows() == 1) ? true : false;
	}


	// Get working

	
	public static function get_working() {	
		$sql = "SELECT * FROM complaints WHERE status = 1"; 
		$complaints = Complaint::find_by_sql($sql);
		return $complaints;
	}
	
	// Get waiting

	
	public static function get_waiting() {	
		$sql = "SELECT * FROM complaints WHERE status = 0"; 
		$complaints = Complaint::find_by_sql($sql);
		return $complaints;
	}
	
	
}


?>