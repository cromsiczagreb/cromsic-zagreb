<?php 



class LastMemberExport extends DatabaseObject {
	
	public static $table_name="last_member_check";
	public static $db_fields = array('datum');
	
	public $datum;
	
	
	// Get last export
	
	public static function get_last_export() {
	$sql = "SELECT datum FROM last_member_check LIMIT 1";
	$results = LastMemberExport::find_by_sql($sql);
	$last = array_shift($results);
	return $last->datum;
	}
	
	// Refresh export
	public static function refresh_export() {
		global $db;
		$datum = date("Y-m-d H:i:s");    
		$sql = "UPDATE last_member_check SET datum='{$datum}' ";
		if ($db->query($sql)) { return true ;}
	}
}

?>