<?php




// Include layout template

function include_layout_template($template="") {
 include(SITE_ROOT.DS.'layout'.DS.$template);
}




// Output message

function output_message($message="") {
	if (!empty($message)){
	/*	return "<div class=\"{$message['type']}\">{$message['message']}</div>";*/
		return "<div class=\"alert alert-{$message['type']} alert-dismissible\" role=\"alert\">
  		<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
  		{$message['message']}</div>";
	} else {
		return " ";
	}
}



// Redirect to

function redirect_to($location = NULL ) {
	if ($location != NULL) {
		header("Location: {$location}");
		exit;
	}
}


// Date hr to sql

function hr_to_sql($date) {
	$olderdate = str_replace(". ","-", $date);
	$olddate = str_replace(".","",$olderdate);
	$sqldate = date_create_from_format('d-m-Y', $olddate);
	if(is_object($sqldate)) {
		return $sqldate->format('Y-m-d');
	} else {
		return "2000-1-1";
	}
}

// Datetime hr to sql

function hr_to_sql_datetime($date) {
	$olderdate = str_replace(". ","-", $date);
	$olddate = str_replace(".","",$olderdate);
	$sqldate = date_create_from_format('d-m-Y-H:i', $olddate);
	return $sqldate->format('Y-m-d H:i:s');
}

// Datetime hr to export

function hr_to_export_datetime($date) {
	$olderdate = str_replace(". ","-", $date);
	$olddate = str_replace(".","",$olderdate);
	//$sqldate = date_create_from_format('d-m-Y H:i:s', $olddate);
	$sqldate = new DateTime($olddate);
	return $sqldate->format('d/m/Y H:i:s');
}

// Date hr to export

function hr_to_export($date) {
	$olderdate = str_replace(". ","-", $date);
	$olddate = str_replace(".","",$olderdate);
	$sqldate = date_create_from_format('d-m-Y', $olddate);
	$sqldate = new DateTime($olddate);
	return $sqldate->format('d/m/Y');
}

// Date sql to hr

function sql_to_hr($date= "01. 01. 2001.") {
	$hrdate = date_create_from_format('Y-m-d', $date);
	return $hrdate->format('d. m. Y.');
}

// Datetime sql to hr datetime

function sql_to_hr_datetime($date) {
	$hrdate = date_create_from_format('Y-m-d H:i:s', $date);
	return $hrdate->format('d. m. Y. H:i:s');
}

// Translate (the column name to something readable)
	
function translate_column($name) {
	switch($name) {
		case "ime": return _("Ime");
		case "prezime": return _("Prezime");
		case "jmbag": return "JMBAG" ;
		case "mobitel": return _("Mobitel");
		case "mail": return _("E-mail");
		case "datum_rod": return _("Datum rođenja");
		case "godina_sad": return _("Godina");
		case "godina_upis": return _("Godina upisa");
		case "registered": return _("Registriran");
		case "opis": return _("Opis");
		case "project_ime": return _("Projekt");
		case "bodovi": return _("Bodovi");
		case "user_ime": return _("Napravio");
		case "activity_ime": return _("Aktivnost");
		case "datum":	return _("Datum");
		case "target_ime": return _("Član");
		case "kategorija": return _("Kategorija");
		case "broj": return "#";
		case "naslov": return _("Naslov");
		case "podnaslov": return _("Podnaslov");
		case "napomena": return _("Napomena");
		case "odbor": return _("Odbor");
		case "razlog": return _("Razlog");
		case "bodovi_opci": return _("Opći dio");
		case "bodovi_cromsic": return _("CroMSIC");
		case "postotak": return "%";
		case "approver": return _("Odobrio");
		case "rejector": return _("Odbio");
		case "id": return "#";
		case "started": return _("Prihvatio");
		case "export": return _("Unešeno");
		case "potroseno": return _("Potrošeno");
		default: return $name;
	}
}
	


// Validate Date and Time

function validate_datetime($date, $format = 'Y-m-d H:i:s') {    
	$d = DateTime::createFromFormat($format, $date);    
	return $d && $d->format($format) == $date; 
}

function validate_date($date, $format = 'Y-m-d') {    
	$d = DateTime::createFromFormat($format, $date);    
	return $d && $d->format($format) == $date; 
}

// Sanitize - html special chars

function h($string) {
	return htmlspecialchars($string);
}

// Sanitize - strip tags

function s($string) {
	return strip_tags($string);
}


// Check if within time range

function check_date_is_within_range($start_date, $end_date, $todays_date)
{

  $start_timestamp = strtotime($start_date);
  $end_timestamp = strtotime($end_date);
  $today_timestamp = strtotime($todays_date);

  return (($today_timestamp >= $start_timestamp) && ($today_timestamp <= $end_timestamp));

}

// Check if language currently exists
function language_exists($language) {
	$localedir = SITE_ROOT .DS.'locale';
	$languages = scandir($localedir);
return in_array($language, $languages);
}
	
?>