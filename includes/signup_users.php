<?php


class SignupUsers extends DatabaseObject {
	
	public static $table_name="signup_users";
	public static $db_fields = array('signup_id', 'user_id', 'timestamp');
	
	public $signup_id;
	public $user_id;



	// Signups for instances
	
	public static function for_instance($signup_id) {		
		$sql = "SELECT * FROM signup_users WHERE signup_id = {$signup_id} ORDER BY timestamp "; 
		$IDs = SignupUsers::find_by_sql($sql);
		$users = array();
		foreach ($IDs as $id) {
			$user = User::find_by_id($id->user_id);
			$users[] = $user;
		}
		return $users;
	}
	
	// Signups for instances: required
	
	public static function for_instance_required($signup_id) {		
		$signup = Signup::find_by_id($signup_id);
		$required = $signup->potrebno;
		$sql = "SELECT * FROM signup_users WHERE signup_id = {$signup_id} ORDER BY timestamp LIMIT {$required} "; 
		$IDs = SignupUsers::find_by_sql($sql);
		$users = array();
		foreach ($IDs as $id) {
			$user = User::find_by_id($id->user_id);
			$users[] = $user;
		}
		return $users;
	}
	
	// Signups for instances: subs
	
	public static function for_instance_subs($signup_id) {	
		$signup = Signup::find_by_id($signup_id);
		$required = $signup->potrebno;
		$subs = $signup->zamjena;
		$sql = "SELECT * FROM signup_users WHERE signup_id = {$signup_id} ORDER BY timestamp LIMIT {$subs} OFFSET {$required} "; 
		$IDs = SignupUsers::find_by_sql($sql);
		$users = array();
		foreach ($IDs as $id) {
			$user = User::find_by_id($id->user_id);
			$users[] = $user;
		}
		return $users;
	}
	
	// Is user signed for event
	
	public static function is_signed($signup_id, $user_id) {
	global $db;
		$signup_id = $db->escape_value($signup_id);	
		$result_array = $db->query("SELECT * FROM ". self::$table_name ." WHERE signup_id={$signup_id} AND user_id={$user_id} LIMIT 1");
		$user_exists = $db->fetch_array($result_array);
		if($user_exists) {
			return true;
		} else {
		return false;
		}
	}
	
	// User sign-up
	
	public static function sign_up($signup_id, $user_id) {
	global $db;
	$db->escape_value($signup_id);
	$db->escape_value($user_id);	
	$sql = "INSERT INTO signup_users ( signup_id , user_id ) VALUES ( '{$signup_id}' , '{$user_id}' ); ";
	if($db->query($sql)) {
			return true;
		} else {
			return false;
		}
	}
	
	// User sign-off
	
	public static function sign_off($signup_id, $user_id) {
	global $db;
	$db->escape_value($signup_id);
	$db->escape_value($user_id);	
	$sql = "DELETE FROM signup_users WHERE signup_id='{$signup_id}' AND user_id='{$user_id}' ";
	$db->query($sql);
	return ($db->affected_rows() == 1) ? true : false;
	}
	
	// Remove participants
	
	public static function remove_participants($signup_id, $user_ids) {
		global $db;
		$db->escape_value($signup_id);
		foreach ($user_ids as $user_id) {
		$db->escape_value($user_id);
		$sql = "DELETE FROM signup_users WHERE signup_id='{$signup_id}' AND user_id='{$user_id}' ";
		$db->query($sql);
		}
	}
	
	// Add participants
	
	public static function add_participants($signup_id, $user_ids) {
		global $db;
		$db->escape_value($signup_id);
		foreach ($user_ids as $user_id) {
		$db->escape_value($user_id);
		$sql = "INSERT INTO signup_users ( signup_id , user_id ) VALUES ( '{$signup_id}' , '{$user_id}' ); ";
		$db->query($sql);
		}
	}

}
?>