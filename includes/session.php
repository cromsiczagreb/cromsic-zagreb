<?php

class session {
	
	private $logged_in;
	private $is_admin;
	private $is_refreshed;
	
	public $user_id;
	public $user_type;
	public $user_refreshed;
	public $user_name;
	public $user_surname;
	public $language;
	public $message = array();
	
	// Construct
	
	function __construct(){
		if ( !isset($_SESSION) ) session_start();
		unset($this->user_refreshed);
		$this->check_message();
		$this->check_login();
	}
	
	// Public check if logged
	
	public function is_logged_in(){
		return $this->logged_in;
	}
	
	// Public check if admin
	
	public function is_admin(){
		return $this->is_admin;
	}
	
	// Public checked if refreshed
	
	public function is_refreshed(){
		return $this->is_refreshed;
	}
	
	// Set refreshed
	
	public function set_refreshed(){
		$this->logout();
		$this->login($user);			
	}
	
	// Check log-in
	
	private function check_login(){
		if(isset($_SESSION['user_id'])) {
			$this->user_id   = $_SESSION['user_id'];
			$this->user_type = $_SESSION['user_type'];
			$this->user_name = $_SESSION['user_name'];
			$this->user_surname = $_SESSION['user_surname'];
			$this->user_refreshed = $_SESSION['user_refreshed'];
			$this->language = $_SESSION['language'];
			if ($this->user_type > 1) { $this->is_admin = true ; }
			if ($this->user_refreshed > 0) { $this->is_refreshed = true ; }
			$this->logged_in = true;
		} else { 
		unset($this->user_id);
		$this->logged_in = false;
		}
	}
	
	// Log-in
	
	public function login($user) {
		if($user){
			$this->user_id		= $_SESSION['user_id']		= $user['id'];
			$this->user_type	= $_SESSION['user_type']	= $user['type'];
			$this->user_name	= $_SESSION['user_name']	= $user['ime'];
			$this->user_surname	= $_SESSION['user_surname']	= $user['prezime'];
			$this->user_refreshed	= $_SESSION['user_refreshed']	= $user['obnovljen'];
			$this->language	= $_SESSION['language']	= $user['jezik'];
			$this->logged_in = true;
			if($this->user_type > 1) { $this->is_admin = true ; }
			if($this->user_refreshed > 0) { $this->is_refreshed = true; }
			$this->message(_("Uspješno ste se prijavili."), "success");
		}
	}
	
	// Log-out
	
	public function logout(){
		unset($_SESSION['user_id']);
		unset($_SESSION['user_type']);
		unset($_SESSION['user_name']);
		unset($_SESSION['user_surname']);
		unset($_SESSION['user_refreshed']);
		unset($_SESSION['language']);
		unset($_SESSION['message']);
		unset($_SESSION['msg_type']);
		unset($this->user_id);
		unset($this->user_type);
		unset($this->user_name);
		unset($this->user_surname);
		unset($this->message);
		unset($this->user_refreshed);
		unset($this->language);
		session_destroy();
		$this->logged_in = false;
		$this->is_admin = false;
	
	}
	
	// Check admin
	
	private function check_admin(){
		if(($_SESSION['user_type'] > 1 )) {
			$this->user_type = $_SESSION['user_type'];
			$this->is_admin = true;
		} else { 
		unset($this->user_type);
		$this->is_admin = false;
		}
	}
	
	// Message
	
	public function message($msg="", $type="") {
		if(!empty($msg)) {
			// Then this is "set message"
			$_SESSION['message']  = $msg;
			$_SESSION['msg_type'] = $type;
		} else {
			// This is the "get message"
			return $this->message;
		}
	}
	
	// Check message
	
	private function check_message() {
		if(isset($_SESSION['message'])) {
			$this->message['message'] = $_SESSION['message'];
			$this->message['type'] = $_SESSION['msg_type'];
			unset($_SESSION['message']);
			unset($_SESSION['msg_type']);
		} else {
			$this->message = "";
		}
	}
	
	// Full name
	
	public function full_name() {
		return $this->user_name . " " . $this->user_surname;
	}
	
}

$session = new session();
$message = $session->message();

?>