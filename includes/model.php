<?php

class Model {
	
	public $requested = array();
	
	function __construct() {
		if($this->check_permission()) {
			$this->individual_process();
		} else {
			echo "Invalid request";
		}
	}
	
	// Check permission (if the person doing request is admin)
	
	public function check_permission() {
		global $session;
		if($session->is_logged_in()) { return true ; }
	}

	
	// Create JSON 
	
	public function create_JSON($objects) {
		$this->json_array = array();

		foreach ($objects as $object) {
			foreach ($this->requested as $key) {
			if(validate_date($object->$key)) { 
				$this->inter_array[$key] = sql_to_hr($object->$key) ;
			 } elseif(validate_datetime($object->$key)) { 
			 	$this->inter_array[$key] = sql_to_hr_datetime($object->$key); 
			 } else {
				$this->inter_array[$key] = $object->$key;
			 }
		//	$this->inter_array[$key] = $object->$key;
			}
			array_push ($this->json_array,  $this->inter_array);
		}
		echo json_encode($this->json_array);
	}
	
	
}
	



?>