<?php require("includes/initialize.php");?>
<?php require("core/login_form.php"); ?>
<?php if($session->is_logged_in()) { redirect_to("index.php"); }?>
<?php include('layout/header.php') ?>


<div id="main">
<div id="navigation">
<?php require("layout/navigation.php");?>
</div>
<?php echo output_message($message); ?>
<div id="page">
	
<div id="login_form">
<?php $form = new LoginForm(); ?>
<div align="right"><a href="forgot_password.php"><?php echo _("Zaboravili ste lozinku?") ; ?></a></div>
</div>
	
</div>
</div>

<?php include("layout/footer.php");?>