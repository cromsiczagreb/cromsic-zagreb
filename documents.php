<?php require("includes/initialize.php");?>
<?php require ("core/" . ME_MPHP."_controller.php"); ?>


<?php include('layout/header.php') ?>

<div id="main">
<div id="navigation">
<?php  require('layout/navigation.php') ?>
</div>
<?php echo output_message($message); ?>
<div id="page">
<?php $create_modal = new NewDocumentForm("create", _("Nova potvrda")); ?>
<div id="float_container">
<?php $documents = new VerifiedPeruser(); ?>
<?php $documents = new UnVerifiedPeruser(); ?>
<br style="clear:both;" />
</div>


<div id="new_doc_button">
<?php
	echo "<input type=\"submit\" data-toggle=\"modal\" data-target=\"#create_modal\" value=\"". _("Unesi novu") ."\">";

?>
<a href="#help" data-toggle="modal" data-target="#help_modal"><?php echo _("Upute"); ?></a>
</div>
<?php $help_modal = new HelpModal("help", "Upute"); ?>

</div>
</div>
<?php include('layout/footer.php') ?>

<script>
$( document ).ready(function() {
    console.log( "ready!" );
	$(".fixed-table-toolbar").empty();
});
</script>