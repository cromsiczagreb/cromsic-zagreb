<?php require("includes/session.php");?>
<?php require("includes/functions.php");?>

<?php 
	$session->logout();
	redirect_to("index.php");
?>