<?php require("includes/initialize.php");?>


<?php include('layout/header.php') ?>

<div id="main">
<div id="navigation">
<?php  require('layout/navigation.php') ?>
</div>
<?php echo output_message($message); ?>
<div id="page">


<div id="float_container">
<!-- CAROUSEL -->
<div id="carousel_container">
<div id="myCarousel" class="carousel slide" data-ride="carousel">
  
 <!-- Indicators -->
  <ol class="carousel-indicators">
   
  <?php
 	 $news = News::find_all() ;
	 $count = count($news);
	echo  "<li data-target=\"#myCarousel\" data-slide-to=\"0\" class=\"active\"></li>";
	for ($x = 1; $x < $count; $x++) {
    	echo "<li data-target=\"#myCarousel\" data-slide-to=\"{$x}\"></li>";
	} 
    
	
   ?>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
  
   <?php ;
  
  		$new = array_shift($news) ;
		
		echo "<div class=\"item active\"><a href=\"news.php?id={$new->id}\"><img src=\"uploads/carousel/{$new->filename}\" alt=\"{$new->naslov}\" ></a></div>";
  		foreach ($news as $new) {
	  	echo "<div class=\"item\"><a href=\"news.php?id={$new->id}\"><img src=\"uploads/carousel/{$new->filename}\" alt=\"{$new->naslov}\" ></a></div>";
  		}
  
  ?>
  
  
   
  
  </div>

  <!-- Left and right controls -->
  <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
</div>

<!-- END CAROUSEL -->

<!-- FB Plugin -->
<div id="fb_container">
<div class="fb-page" data-href="https://www.facebook.com/CroMSIC.Zagreb" data-width="280" data-height="500" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="false" data-show-posts="false"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/CroMSIC.Zagreb"><a href="https://www.facebook.com/CroMSIC.Zagreb">CroMSIC Zagreb</a></blockquote></div></div></div>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.5";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<!-- END FB Plugin -->

<!-- Twitter plugin -->
<div id="tw_container">
<a class="twitter-timeline" href="https://twitter.com/CroMSIC_ZG" data-widget-id="588018698042155008" width="280" height="245">Tweets by @CroMSIC_ZG</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
</div>
<!-- END Twitter plugin -->

</div>
</div>
</div>

<?php include('layout/footer.php') ?>