<?php require("includes/initialize.php");?>
<?php
if(!($session->is_logged_in())) {
	$session->message(_("Morate se prijaviti da bi prijavili razmjenu."), "warning");	
	redirect_to("index.php");
}


$application = Application::find_by_user_id($session->user_id);


if(!empty($application)) { 
		if ($application->status == 1) {
		redirect_to("af_edit.php");	
	} else {
		redirect_to("af_view.php");
	}	
}

?>

<?php include('layout/header.php') ?>

<div id="main">
<div id="navigation">
<?php  require('layout/navigation.php') ?>
</div>

<div id="page">
<?php echo output_message($message); ?>

<?php
require("locale/{$lang}/HELP/af_info_help.php");

?>

<br /><br>
<div id="new_doc_button">
<form action="af_create.php">

<?php
if(check_date_is_within_range('2016-10-28 00:00:00', '2016-11-05 20:00:00', date("Y-m-d G:i:s"))){
    echo "<input type=\"submit\" value=\"" . _("Prijavi razmjenu") . "\" href=\"af_create.php\">";
} else {
    echo _('Natječaj trenutno nije u tijeku.');
}


?>

</form>
</div>


</div>
</div>

<?php include('layout/footer.php') ?>


