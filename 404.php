<?php require("includes/initialize.php");?>


<?php include('layout/header.php') ?>

<div id="main">
<div id="navigation">
<?php require("layout/navigation.php");?>
</div>
<div id="page">
<?php echo output_message($message); ?>
<div align="center">
<h2>Stranica koju tražite ne postoji ili je trenutačno nedostupna.</h2>
<h3>Provjerite poveznicu kojom ste došli ovdje ili pokušajte ponovno kasnije. </h3>
</div>
</div>
</div>
<?php include('layout/footer.php') ?>