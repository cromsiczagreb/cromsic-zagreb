<?php require("includes/initialize.php");?>


<?php include('layout/header.php') ?>

<div id="main">
<div id="navigation">
<?php  require('layout/navigation.php') ?>
</div>
<?php echo output_message($message); ?>
<div id="page">

<div id="float_container">
<div id="half_page_div_left">
<h3><?php echo _("Profesionalne"); ?> </h3>
<table id="tablep"></table>
</div>
<div id="half_page_div_right">
<h3> <?php echo _("Znanstvene"); ?></h3>
<table id="tabler"></table>
</div>
<br style="clear:both;" />

 
  <script>
$('#tablep').bootstrapTable({
	sortName: 'zemlja',
	sortable:'true',
    columns: [{
        field: 'zemlja',
        title: '<?php echo _("Zemlja");?>'
    }, {
		field: 'broj',
        title: '<?php echo _("Broj");?>'
    }, {
        field: 'uvjeti',
        title: '<?php echo _("Uvjeti");?>'
    }],
    data: [{
        broj: 1,
        zemlja: 'Kanada',
        uvjeti: '<a target="_blank" href="http://ifmsa.org/exchange/scope/explore/conditions/view/16">CFMS</a>'
    }, {
         broj: 1,
        zemlja: 'Njemačka (ne 7. ili 8.)',
        uvjeti: '<a target="_blank" href="http://ifmsa.org/exchange/scope/explore/conditions/view/33">BVMD</a>'
    }, {
		broj: 1,
        zemlja: 'Finska',
        uvjeti: '<a target="_blank" href="http://ifmsa.org/exchange/scope/explore/conditions/view/31">FiMSIC</a>'
	}, {
		broj: 2,
        zemlja: 'Japan',
        uvjeti: '<a target="_blank" href="http://ifmsa.org/exchange/scope/explore/conditions/view/46">IFMSA-Japan</a>'
	}, {
		broj: 1,
        zemlja: 'Španjolska',
        uvjeti: '<a target="_blank" href="http://ifmsa.org/exchange/scope/explore/conditions/view/83">IFMSA-Spain</a>'
	}, {
		broj: 1,
        zemlja: 'Švedska (10. 07.)',
        uvjeti: '<a target="_blank" href="http://ifmsa.org/exchange/scope/explore/conditions/view/85">IFMSA-Sweden</a>'
	}, {
		broj: 3,
        zemlja: 'Španjolska (8 mj.)',
        uvjeti: '<a target="_blank" href="http://ifmsa.org/exchange/scope/explore/conditions/view/83">IFMSA-Spain</a>'
	}, {
		broj: 2,
        zemlja: 'Grčka',
        uvjeti: '<a target="_blank" href="http://ifmsa.org/exchange/scope/explore/conditions/view/35">HelMSIC</a>'
	}, {
		broj: 1,
        zemlja: 'Turska',
        uvjeti: '<a target="_blank" href="http://ifmsa.org/exchange/scope/explore/conditions/view/93">TurkMSIC</a>'
	}, {
		broj: 2,
        zemlja: 'Katalonija (8. mj.)',
        uvjeti: '<a target="_blank" href="http://ifmsa.org/exchange/scope/explore/conditions/view/18">AECS</a>'
	}, {
		broj: 4,
        zemlja: 'Portugal',
        uvjeti: '<a target="_blank" href="http://ifmsa.org/exchange/scope/explore/conditions/view/74">PorMSIC</a>'
	}, {
		broj: 3,
        zemlja: 'Italija (8. mj.)',
        uvjeti: '<a target="_blank" href="http://ifmsa.org/exchange/scope/explore/conditions/view/44">SIMS</a>'
	}, {
		broj: 2,
        zemlja: 'Francuska',
        uvjeti: '<a target="_blank" href="http://ifmsa.org/exchange/scope/explore/conditions/view/32">ANEMF</a>'
	}, {
		broj: 2,
        zemlja: 'Maroko',
        uvjeti: '<a target="_blank" href="http://ifmsa.org/exchange/scope/explore/conditions/view/100">IFMSA-Morocco</a>'
	}, {
		broj: 3,
        zemlja: 'Poljska',
        uvjeti: '<a target="_blank" href="http://ifmsa.org/exchange/scope/explore/conditions/view/73">IFMSA-Poland</a>'
	}, {
		broj: 2,
        zemlja: 'Rusija (Moskva)',
        uvjeti: '<a target="_blank" href="http://ifmsa.org/exchange/scope/explore/conditions/view/76">HCCM</a>'
	}, {
		broj: 2,
        zemlja: 'Libanon',
        uvjeti: '<a target="_blank" href="http://ifmsa.org/exchange/scope/explore/conditions/view/53">LeMSIC</a>'
	}, {
		broj: 1,
        zemlja: 'Slovačka',
        uvjeti: '<a target="_blank" href="http://ifmsa.org/exchange/scope/explore/conditions/view/80">SloMSA</a>'
	}, {
		broj: 1,
        zemlja: 'Brazil (DENEM)',
        uvjeti: '<a target="_blank" href="http://ifmsa.org/exchange/scope/explore/conditions/view/11">DENEM</a>'
	}, {
		broj: 2,
        zemlja: 'Brazil (IFMSA)',
        uvjeti: '<a target="_blank" href="http://ifmsa.org/exchange/scope/explore/conditions/view/12">IFMSA-Brazil</a>'
	}, {
		broj: 1,
        zemlja: 'Bolivija',
        uvjeti: '<a target="_blank" href="http://ifmsa.org/exchange/scope/explore/conditions/view/8">IFMSA Bolivia</a>'
	}, {
		broj: 2,
        zemlja: 'Gana',
        uvjeti: '<a target="_blank" href="http://ifmsa.org/exchange/scope/explore/conditions/view/34">FGMSA</a>'
	}, {
		broj: 1,
        zemlja: 'Ekvador',
        uvjeti: '<a target="_blank" href="http://ifmsa.org/exchange/scope/explore/conditions/view/26">AEMPPI</a>'
	}, {
		broj: 1,
        zemlja: 'Indija',
        uvjeti: '<a target="_blank" href="http://ifmsa.org/exchange/scope/explore/conditions/view/26">AEMPPI</a>'
	}, {
		broj: 2,
        zemlja: 'Indonesia',
        uvjeti: '<a target="_blank" href="http://ifmsa.org/exchange/scope/explore/conditions/view/40">CIMSA-ISMKI</a>'
	}, {
		broj: 2,
        zemlja: 'Kenija',
        uvjeti: '<a target="_blank" href="http://ifmsa.org/exchange/scope/explore/conditions/view/48">MSAKE</a>'
		}, {
		broj: 1,
        zemlja: 'Latvija',
        uvjeti: '<a target="_blank" href="http://ifmsa.org/exchange/scope/explore/conditions/view/52">LAMSA Latvia</a>'
		}, {
		broj: 2,
        zemlja: 'Litva',
        uvjeti: '<a target="_blank" href="http://ifmsa.org/exchange/scope/explore/conditions/view/55">LMSA</a>'
		}, {
		broj: 1,
        zemlja: 'Panama',
        uvjeti: '<a target="_blank" href="http://ifmsa.org/exchange/scope/explore/conditions/view/68">IFMSA-Panama</a>'
		}, {
		broj: 1,
        zemlja: 'Rusija - Tatarstan',
        uvjeti: '<a target="_blank" href="http://ifmsa.org/exchange/scope/explore/conditions/view/88">TaMSA-Tatarstan</a>'
		}, {
		broj: 1,
        zemlja: 'Tunis',
        uvjeti: '<a target="_blank" href="http://ifmsa.org/exchange/scope/explore/conditions/view/55">ASSOCIA-MED</a>'
		}, {
		broj: 2,
        zemlja: 'Češka',
        uvjeti: '<a target="_blank" href="http://ifmsa.org/exchange/scope/explore/conditions/view/24">IFMSA CZ</a>'
	}]
});
</script>
<script>
$('#tabler').bootstrapTable({
	sortName: 'zemlja',
	sortable:'true',
    columns: [{
        field: 'zemlja',
        title: '<?php echo _("Zemlja");?>'
    }, {
		field: 'mjesec',
        title: '<?php echo _("Mjesec");?>'
    }, {
        field: 'uvjeti',
        title: '<?php echo _("Uvjeti");?>'
    }],
    data: [{
        mjesec: 9,
        zemlja: 'Njemačka',
        uvjeti: '<a target="_blank" href="http://ifmsa.org/exchange/score/explore/conditions/view/60">BVMD</a>'
    }, {
        mjesec: 8,
        zemlja: 'Malta ',
        uvjeti: '<a target="_blank" href="http://ifmsa.org/exchange/score/explore/conditions/view/85">MMSA</a>'
    }, {
		mjesec: 7,
        zemlja: 'Tajland',
        uvjeti: '<a target="_blank" href="http://ifmsa.org/exchange/score/explore/conditions/view/116">IFMSA-Thailand</a>'
		}, {
        mjesec: '',
        zemlja: 'Meksiko ',
        uvjeti: '<a target="_blank" href="http://ifmsa.org/exchange/score/explore/conditions/view/86">IFMSA-Mexico</a>'
		}, {
        mjesec: '4',
        zemlja: 'United Kingdom',
        uvjeti: '<a target="_blank" href="http://ifmsa.org/exchange/score/explore/conditions/view/123">Medsin-UK</a>'
		}, {
        mjesec: 8,
        zemlja: 'Poljska ',
        uvjeti: '<a target="_blank" href="http://ifmsa.org/exchange/score/explore/conditions/view/100">IFMSA Poland</a>'
		}, {
        mjesec: 9,
        zemlja: 'Portugal ',
        uvjeti: '<a target="_blank" href="http://ifmsa.org/exchange/score/explore/conditions/view/101">PorMSIC</a>'
		}, {
        mjesec: 9,
        zemlja: 'Katalonija',
        uvjeti: '<a target="_blank" href="http://ifmsa.org/exchange/score/explore/conditions/view/45">AECS</a>'
		}, {
        mjesec: 8,
        zemlja: 'Estonija ',
        uvjeti: '<a target="_blank" href="http://ifmsa.org/exchange/score/explore/conditions/view/56">EstMSA</a>'
				}, {
        mjesec: 7,
        zemlja: 'Italija ',
        uvjeti: '<a target="_blank" href="http://ifmsa.org/exchange/score/explore/conditions/view/71">SISM</a>'
		}, {
        mjesec: 8,
        zemlja: 'Maroko ',
        uvjeti: '<a target="_blank" href="http://ifmsa.org/exchange/score/explore/conditions/view/127">IFMSA-Morocco</a>'
		}, {
        mjesec: "8/9",
        zemlja: 'Libanon ',
        uvjeti: '<a target="_blank" href="http://ifmsa.org/exchange/score/explore/conditions/view/71">SISM</a>'
		}, {
        mjesec: 8,
        zemlja: 'Brazil (DENEM) ',
        uvjeti: '<a target="_blank" href="http://ifmsa.org/exchange/score/explore/conditions/view/38s">DENEM</a>'
		}, {
        mjesec: '',
        zemlja: 'Tunis ',
        uvjeti: '<a target="_blank" href="http://ifmsa.org/exchange/score/explore/conditions/view/119">ASSOCIA-MED</a>'
		
		
	}]
});
</script>
</div>   
</div>
</div>

<?php include('layout/footer.php') ?>

